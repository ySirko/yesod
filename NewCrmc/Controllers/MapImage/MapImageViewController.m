//
//  MapImageViewController.m
//  מכון ויצמן
//
//  Created by Lior Ronen on 2/2/15.
//  Copyright (c) 2015 webit. All rights reserved.
//

#import "MapImageViewController.h"

@interface MapImageViewController ()

@end

@implementation MapImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.logo.hidden=YES;
    self.buildings.hidden=YES;
    self.lblBackgroundTitle.frame=CGRectMake(0, 50, 320, 29);
    self.Title.frame=CGRectMake(0, 44, 304, 34);
    self.lblBackgroundTitle.backgroundColor=_color;
    self.Title.text=self.header;

    _mapImg=[[UIImageView alloc]initWithFrame:CGRectMake(0, 79, 320, self.view.frame.size.height-79)];
    _mapImg.image=[UIImage imageNamed:@"HaderaMap.jpg"];
    _mapImg.contentMode = UIViewContentModeScaleAspectFill;
    _mapImg.clipsToBounds = YES;
    [self.view addSubview:_mapImg];
    
    float cityLatitude=33.053874, cityLongtitude=35.594763;
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:cityLatitude
                                                            longitude:cityLongtitude
                                                                 zoom:13.5];
    _googleMapView = [GMSMapView mapWithFrame:CGRectMake(0, 79, 320, self.view.frame.size.height-79) camera:camera];
    _googleMapView.myLocationEnabled = YES;
    [self.view addSubview:_googleMapView];
    
    // Creates a marker in the center of the map.
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(cityLatitude, cityLongtitude);
    //marker.title = @"Hadera";
    //marker.snippet = @"Israel";
    marker.map = _googleMapView;
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
