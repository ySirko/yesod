//
//  CouncilOfficialCell.m
//  CRMC
//
//  Created by Racheli Kroiz on 05/05/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import "CouncilOfficialCell.h"
#import "AppDelegate.h"
@implementation CouncilOfficialCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (IBAction)btnEmailClick:(id)sender
{
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    [mc setToRecipients:[NSArray arrayWithObject:[NSString stringWithFormat:@"%@",_entity.emailAddress]]];
    
    [self.delegate CouncilOfficialCellPresentMail:mc];
}

- (IBAction)btnPhoneClick:(id)sender
{
    [self.delegate CouncilOfficialCellCall:_entity];
}
- (IBAction)btnInfoClick:(id)sender
{
    [self.delegate CouncilOfficialCellInfo:_entity];
}

-(void)setCouncilOfficialCell:(Entity*)entity
{
    _entity=entity;
    [self setDynamicObject];
    _lblNameCouncilOfficial.text=entity.entityName;
    if([entity.phoneNumber isEqualToString:@""])
    {
        //_imgPhone.hidden=YES;
    }
    if([entity.emailAddress isEqualToString:@""])
    {
        //_imgEmail.hidden=YES;
    }
    if(([entity.information isEqualToString:@""]||entity.information==nil)&&([entity.details isEqualToString:@""]||entity.details==nil))
    {
        // _imgInfo.hidden=YES;
    }
    _imgPhone.hidden = NO;
    _imgEmail.hidden = NO;
    _imgInfo.hidden = YES;
}
-(void)setDynamicObject
{
    //add back Label
    if(_entity.cityId==13)
    {
        _lblNameCouncilOfficial=[[UILabel alloc]initWithFrame:CGRectMake(78, 8, 240, 21)];
        [_lblNameCouncilOfficial setFont:[UIFont fontWithName:@"Helvetica-Bold" size:11]];
    }
    else
    {
        _lblNameCouncilOfficial=[[UILabel alloc]initWithFrame:CGRectMake(108, 2, 210, 33)];
        _lblNameCouncilOfficial.numberOfLines=2;
        [_lblNameCouncilOfficial setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13.0]];
    }
    [_lblNameCouncilOfficial setTextAlignment:NSTextAlignmentRight];
    _lblNameCouncilOfficial.layer.sublayerTransform = CATransform3DMakeTranslation(-6, 0, 0);
    [self addSubview:_lblNameCouncilOfficial];
    
    //add button
    _imgPhone=[[UIButton alloc]init];
    _imgPhone.layer.frame= CGRectMake(10 , 7, 28, 28);
    _imgPhone.layer.cornerRadius=4;
    [_imgPhone addTarget:self action:@selector(btnPhoneClick:) forControlEvents:UIControlEventTouchUpInside];
  
    [_imgPhone setImage:[UIImage imageNamed:[NSString stringWithFormat:@"call%@",_strColor]] forState:UIControlStateNormal];
    [self addSubview:_imgPhone];
    
    _imgEmail=[[UIButton alloc]init];
    _imgEmail.layer.frame= CGRectMake(41 , 7, 28, 28);
    _imgEmail.layer.cornerRadius=4;
    [_imgEmail addTarget:self action:@selector(btnEmailClick:) forControlEvents:UIControlEventTouchUpInside];
    [_imgEmail setImage:[UIImage imageNamed:[NSString stringWithFormat:@"mail%@",_strColor]] forState:UIControlStateNormal];
    [self addSubview:_imgEmail];
    
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.city.cityId==1 || appDelegate.city.cityId==18)
    {
        [_imgEmail setImage:[UIImage imageNamed:@"mailPurple.png"] forState:UIControlStateNormal];
        [_imgPhone setImage:[UIImage imageNamed:@"callPurple.png"] forState:UIControlStateNormal];
        
        _imgInfo=[[UIButton alloc]init];
        _imgInfo.layer.frame= CGRectMake(72 , 7, 28, 28);
        _imgInfo.layer.cornerRadius=4;
        [_imgInfo addTarget:self action:@selector(btnInfoClick:) forControlEvents:UIControlEventTouchUpInside];
        [_imgInfo setImage:[UIImage imageNamed:@"purpleInfo"] forState:UIControlStateNormal];
        [self addSubview:_imgInfo];
    }
    
    ///add line label
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, 40, 320, 4)];
    lblLine.backgroundColor=_color;
    [self addSubview:lblLine];
}
@end
