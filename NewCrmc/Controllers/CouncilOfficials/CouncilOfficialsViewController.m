
//  CouncilOfficialsViewController.m
//  CRMC
//
//  Created by Racheli Kroiz on 05/05/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import "CouncilOfficialsViewController.h"

@interface CouncilOfficialsViewController () <CouncilOfficialCellDelegate>
@end

@implementation CouncilOfficialsViewController
{
    NSMutableArray* filteredCouncilOfficalArray;
    AppDelegate*appdelegate;
    UIView*v;
    UILabel*lblBack;
    UIView*viewBack;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.Title.text=self.header;
    [self setDynamicObject];
    _searchBar.barStyle = UIBarStyleBlackTranslucent;
    self.lblBackgroundTitle.backgroundColor=_color;
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self.generic showNativeActivityIndicator:self];
    [self.connection connectionToService:@"GetCityEntities" jsonDictionary:[appdelegate.city parseCityToDict] controller:self withSelector:@selector(GetCityEntitiesResult:)];
}
-(void)GetCityEntitiesResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if(![result isEqualToString:@""]&&![result isEqualToString:@"[]"])
    {
        Entity * entity=[[Entity alloc]init];
        _listCouncilOfficials=[[NSArray alloc]initWithArray:[entity parseListEntityFromJson:result]];
        [_tableView reloadData];
    }
    else
        NSLog(@"Empty entites list");
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark- UITableView
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (![self.searchBar.text isEqualToString:@""])
        return [filteredCouncilOfficalArray count];
    else
        return _listCouncilOfficials.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    CouncilOfficialCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell = [[CouncilOfficialCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    Entity* entity;
    if (![self.searchBar.text isEqualToString:@""])
        entity = [filteredCouncilOfficalArray objectAtIndex:indexPath.row];
    else
        entity = [_listCouncilOfficials objectAtIndex:indexPath.row];
    cell.strColor=_strColor;
    cell.color=_color;
    [cell setCouncilOfficialCell:entity];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.delegate=self;
    return cell;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [filteredCouncilOfficalArray removeAllObjects];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.entityName contains[c] %@",self.searchBar.text];
    filteredCouncilOfficalArray = [NSMutableArray arrayWithArray:[_listCouncilOfficials filteredArrayUsingPredicate:predicate]];
    [_tableView reloadData];
    [searchBar resignFirstResponder];
}

#pragma mark- CellDelegate
-(void)CouncilOfficialCellCall:(Entity*) entity;
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",entity.phoneNumber]]];
}

-(void)CouncilOfficialCellInfo:(Entity*) entity;
{
    viewBack=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:viewBack];
    
    lblBack=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    lblBack.backgroundColor=[UIColor grayColor];
    lblBack.alpha=0.8;
    [viewBack addSubview:lblBack];
    
    v=[[UIView alloc]initWithFrame:CGRectMake(40,(self.view.frame.size.height-240)/2+80 , self.view.frame.size.width-80, 240)];
    v.backgroundColor=[UIColor whiteColor];
    [viewBack addSubview:v];
    
    UIButton*btn=[[UIButton alloc]initWithFrame:CGRectMake(0, 70, v.frame.size.width, 26)];
    btn.backgroundColor=self.appManager.purple;
    [btn setBackgroundImage:[UIImage imageNamed:@"רשת לפופאפ"] forState:normal];
    [v addSubview:btn];
    
    UILabel*lbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 96, v.frame.size.width, 144)];
    lbl.backgroundColor=[UIColor colorWithRed:224.0f/255.0f green:224.0f/255.0f blue:224.0f/255.0f alpha:1.0f];
    [v addSubview:lbl];
    
    UILabel*lblInfoName=[[UILabel alloc]initWithFrame:CGRectMake(30, 15, v.frame.size.width-40, 20)];
    lblInfoName.text=entity.entityName;
    lblInfoName.font=[UIFont fontWithName:@"Helvetica Bold" size:18];
    lblInfoName.textAlignment=NSTextAlignmentRight;
    lblInfoName.textColor=_color;
    
    UIFont *font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    CGRect labelRect = [lblInfoName.text boundingRectWithSize:CGSizeMake(lblInfoName.frame.size.width, 1000)
                                                         options:NSStringDrawingUsesLineFragmentOrigin
                                                      attributes:@{NSFontAttributeName:font}
                                                         context:nil];
    CGRect lRect=lblInfoName.frame;
    lRect.size.height=labelRect.size.height;
    lblInfoName.frame=lRect;
    if ((int)lRect.size.height>20)
    {
        lblInfoName.numberOfLines=2;
    }
    [v addSubview:lblInfoName];
    
    UILabel*lblInfoSubName=[[UILabel alloc]initWithFrame:CGRectMake(20, 35, v.frame.size.width-40, 20)];
    lblInfoSubName.text=entity.details;
    lblInfoSubName.font=[UIFont fontWithName:@"Helvetica Bold" size:15];
    lblInfoSubName.textAlignment=NSTextAlignmentRight;
    lblInfoSubName.textColor=_color;
    [v addSubview:lblInfoSubName];
    
    UILabel*lblInfo=[[UILabel alloc]initWithFrame:CGRectMake(15, 100, v.frame.size.width-30,130)];
    lblInfo.numberOfLines=0;
    lblInfo.textAlignment=NSTextAlignmentRight;
    lblInfo.text=entity.information;
    [v addSubview:lblInfo];
    
    UIButton *btnExit =[[UIButton alloc]initWithFrame:CGRectMake(15,15, 20, 20)];
    [btnExit setTitleColor:_color forState:UIControlStateNormal];
    [btnExit setTitle:@"X" forState:normal];
    [btnExit addTarget:self action:@selector(btnExitClick:) forControlEvents:UIControlEventTouchUpInside];
    [v addSubview:btnExit];
}

-(void)CouncilOfficialCellPresentMail:(MFMailComposeViewController*)mailCompose
{
    mailCompose.mailComposeDelegate=self;

//    [mailCompose setToRecipients:toRecipients];
//    [mailCompose setCcRecipients:ccRecipients];
//    [mailCompose setBccRecipients:bccRecipients];
    [self presentViewController:mailCompose animated:YES completion:NULL];
}

- (IBAction)btnExitClick:(id)sender
{
    [viewBack removeFromSuperview];
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [filteredCouncilOfficalArray removeAllObjects];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.entityName contains[c] %@",self.searchBar.text];
    filteredCouncilOfficalArray = [NSMutableArray arrayWithArray:[_listCouncilOfficials filteredArrayUsingPredicate:predicate]];
    [_tableView reloadData];
    [self.view endEditing:YES];
}
-(void)setDynamicObject
{
    //add searchBur
    _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 157, 320, 44)];
    _searchBar.delegate = self;
    _searchBar.barTintColor=_color;
    [self.view addSubview:_searchBar];
    //add table
    CGSize g=self.view.frame.size;
    _tableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 200, 320, g.height-200)];
    _tableView.delegate=self;
    _tableView.dataSource=self;
    [self.view addSubview:_tableView];
}
@end
