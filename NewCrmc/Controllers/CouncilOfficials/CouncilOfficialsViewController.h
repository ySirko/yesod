//
//  CouncilOfficialsViewController.h
//  CRMC
//
//  Created by Racheli Kroiz on 05/05/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CouncilOfficialCell/CouncilOfficialCell.h"
#import "SubViewController.h"
#import "Connection.h"
#import "Generic.h"
#import "AppDelegate.h"
#import "Entity.h"
@interface CouncilOfficialsViewController : SubViewController<UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate,MFMailComposeViewControllerDelegate,UISearchBarDelegate,UISearchDisplayDelegate>
@property(strong,nonatomic) UITableView* tableView;
@property(strong,nonatomic) UISearchBar* searchBar;
@property(retain,nonatomic)NSArray* listCouncilOfficials;
@property (strong, nonatomic)  UITextField *txtSearch;
@property(retain,nonatomic)UIColor *color;
@property(retain,nonatomic)NSString*strColor;
@end
