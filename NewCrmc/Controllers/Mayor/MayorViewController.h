//
//  MayorViewController.h
//  CRMC
//
//  Created by WebitMacMini Two on 6/18/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import "SubViewController.h"

#import "NSData+Base64.h"

@interface MayorViewController : SubViewController <UITextViewDelegate, UITextFieldDelegate>

@property (strong, nonatomic) UITextView *txtview;
@property (strong, nonatomic) UIImageView *imgMayor;
@property (retain, nonatomic) NSString* mayorSpeech;
@property (strong, nonatomic) UIColor* color;
@property int index;

@end