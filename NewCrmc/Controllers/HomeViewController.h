//
//  HomeViewController.h
//  NewCrmc
//
//  Created by WebitMacMini Two on 7/17/14.
//  Copyright (c) 2014 webit. All rights reserved.
//
#import "InheritorFromSubViewController.h"
@interface HomeViewController :InheritorFromSubViewController<MFMessageComposeViewControllerDelegate>
@property (strong, nonatomic)  UIButton *btn1;
@property (strong, nonatomic)  UIButton *btn2;
@property (strong, nonatomic)  UIButton *btn3;
@property (strong, nonatomic)  UIButton *btn4;
@property (strong, nonatomic)  UIButton *btn5;
@property (strong, nonatomic)  UIButton *btn6;
@property (strong, nonatomic)  UIButton *btn7;
@property (strong, nonatomic)  UIButton *btn8;
@property (strong, nonatomic)  UIButton *btn9;
//אפשר לצמצם את התמונה לתוך הכפתורים
@property (retain, nonatomic) UIImageView*btnIcon1;
@property (retain, nonatomic) UIImageView*btnIcon2;
@property (retain, nonatomic) UIImageView*btnIcon3;
@property (retain, nonatomic) UIImageView*btnIcon4;
@property (retain, nonatomic) UIImageView*btnIcon5;
@property (retain, nonatomic) UIImageView*btnIcon6;
@property (retain, nonatomic) UIImageView*btnIcon7;
@property (retain, nonatomic) UIImageView*btnIcon8;
@property (retain, nonatomic) UIImageView*btnIcon9;
@property (retain, nonatomic) NSMutableArray* listMenu;
@property (retain, nonatomic)  UIButton *btnEmergencyNum;
@property (retain, nonatomic)  UIButton *btnMoked;
@property (strong, nonatomic)  UIImageView  *images;
@property (strong, nonatomic)  UILabel *backAlpha;
@end
