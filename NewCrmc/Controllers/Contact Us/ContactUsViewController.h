//
//  ContactUsViewController.h
//  NewCrmc
//
//  Created by MyMac on 8/12/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "SubViewController.h"

@interface ContactUsViewController : SubViewController
@property (strong, nonatomic)  UITextField *txtFirstName;
@property (strong, nonatomic)  UITextField *txtLastName;
@property (strong, nonatomic)  UITextField *txtEmail;
@property (strong, nonatomic)  UITextView *txtViewComment;
@property (strong, nonatomic)  UIButton *btnSend;
@property (strong, nonatomic) UIButton *btnCancel;
@end