//
//  ContactUsViewController.m
//  NewCrmc
//
//  Created by MyMac on 8/12/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "ContactUsViewController.h"

@interface ContactUsViewController ()
{
    BOOL flag;
}
@end

@implementation ContactUsViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setDynamicObject];
    self.lblBackgroundTitle.backgroundColor=self.appManager.lightBlue;
    self.Title.text=self.header;
    _txtFirstName.layer.borderColor=self.appManager.lightBlue.CGColor;
    _txtLastName.layer.borderColor=self.appManager.lightBlue.CGColor;
    _txtEmail.layer.borderColor=self.appManager.lightBlue.CGColor;
    _txtViewComment.layer.borderColor=self.appManager.lightBlue.CGColor;
    _txtViewComment.text=@"תאור המפגע";
    _txtViewComment.textColor=[UIColor lightGrayColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)btnCancelClick:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btnOkClick:(id)sender;
{
    UIAlertView*alert=[[UIAlertView alloc]initWithTitle:@"" message:@"פנייתך נשלחה בהצלחה, ניצור עימך קשר בהקדם האפשרי" delegate:self cancelButtonTitle:@"אישור" otherButtonTitles: nil];
    if([self validationText:_txtViewComment.text]&&[self validationText:_txtLastName.text]&&[self validationText:_txtFirstName.text]&&[self validateEmailWithString:_txtEmail.text])
    {
         //שליחה לשרת
        [alert show];
    }
    else
    {
        alert.message=@"חלק משדות החובה חסרים או שתוכנם אינו חוקי. אנא מלאו ונסו שוב";
        [alert show];
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if([alertView.message isEqualToString:@"פנייתך נשלחה בהצלחה, ניצור עימך קשר בהקדם האפשרי"])
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(flag)
    {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+150, self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
        [self.view endEditing:YES];
        flag=NO;
    }
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if([textView.text isEqualToString:@"תאור המפגע"])
    {
        _txtViewComment.textColor=[UIColor blackColor];
        _txtViewComment.text=@"";
    }
    if(!flag)
    {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-150, self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
        flag=YES;
    }
    return YES;
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    if([_txtViewComment.text isEqualToString: @""])
    {
        _txtViewComment.text=@"תאור המפגע";
        _txtViewComment.textColor=[UIColor lightGrayColor];
    }
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(!flag)
    {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-150, self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
        flag=YES;
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+150, self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];
    [self.view endEditing:YES];
    flag=NO;
    return YES;
}
- (BOOL)textView:(UITextView *)txtView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"])
    {
        if([_txtViewComment.text isEqualToString: @""])
        {
            _txtViewComment.text=@"תאור המפגע";
            _txtViewComment.textColor=[UIColor lightGrayColor];
        }
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+150, self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
        [self.view endEditing:YES];
        flag=NO;
    }
    return YES;
}
-(BOOL)validationText:(NSString*)text
{
    NSString *myRegex = @"[A-Zא-תa-z ]*";
    NSPredicate *myTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", myRegex];
    NSString *string = text;
    BOOL valid = [myTest evaluateWithObject:string];
    return valid;
}
- (BOOL)validateEmailWithString:(NSString*)email
{
    if([email isEqualToString:@""])
        return NO;
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
-(void)setDynamicObject
{
    //add back Label
    UILabel *lblBack=[[UILabel alloc]initWithFrame:CGRectMake(0, 157, 320, 260)];
    lblBack.backgroundColor=self.appManager.lightBlue;
    [self.view addSubview:lblBack];
    UILabel* lblContactUs=[[UILabel alloc]initWithFrame:CGRectMake(0, 163, 300, 21)];
    [lblContactUs setText:@"על מנת ליצור קשר"];
    lblContactUs.font = [UIFont fontWithName:@"Helvetica" size:13];
    lblContactUs.textAlignment = NSTextAlignmentRight;
    
    [self.view addSubview:lblContactUs];
    UILabel* fillDetails=[[UILabel alloc]initWithFrame:CGRectMake(0, 178, 300, 21)];
    [fillDetails setText:@"נא מלאו את הפרטים הבאים:"];
    fillDetails.font = [UIFont fontWithName:@"Helvetica" size:13];
    fillDetails.textAlignment = NSTextAlignmentRight;
    
    [self.view addSubview:fillDetails];
    
    //add textField
    _txtFirstName = [[UITextField alloc] initWithFrame:CGRectMake(10 , 205, 300, 30)];
    _txtFirstName.delegate=self;
    [_txtFirstName.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
    [_txtFirstName.layer setBorderWidth:2.0];
    _txtFirstName.layer.cornerRadius = 5;
    _txtFirstName.backgroundColor=[UIColor whiteColor];
    _txtFirstName.clipsToBounds = YES;
    [_txtFirstName setPlaceholder:@"שם פרטי"];
    _txtFirstName.font = [UIFont fontWithName:@"Helvetica" size:13];
    [_txtFirstName setTextAlignment:NSTextAlignmentRight];
    _txtFirstName.layer.sublayerTransform = CATransform3DMakeTranslation(-6, 0, 0);
    [self.view addSubview:_txtFirstName];
    
    
    _txtLastName = [[UITextField alloc] initWithFrame:CGRectMake(10 , 240, 300, 30)];
    _txtLastName.delegate=self;
    [_txtLastName.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
    [_txtLastName.layer setBorderWidth:2.0];
    _txtLastName.layer.cornerRadius = 5;
    _txtLastName.font = [UIFont fontWithName:@"Helvetica" size:13];
    _txtLastName.backgroundColor=[UIColor whiteColor];
    _txtLastName.clipsToBounds = YES;
    
    [_txtLastName setPlaceholder:@"שם משפחה"];
    [_txtLastName setTextAlignment:NSTextAlignmentRight];
    _txtLastName.layer.sublayerTransform = CATransform3DMakeTranslation(-6, 0, 0);
    [self.view addSubview:_txtLastName];
    
    _txtEmail = [[UITextField alloc] initWithFrame:CGRectMake(10 , 276, 300, 30)];
    _txtEmail.delegate=self;
    [_txtEmail.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
    [_txtEmail.layer setBorderWidth:2.0];
    _txtEmail.layer.cornerRadius = 5;
    _txtEmail.font = [UIFont fontWithName:@"Helvetica" size:13];
    _txtEmail.backgroundColor=[UIColor whiteColor];
    _txtEmail.clipsToBounds = YES;
    [_txtEmail setPlaceholder:@"דואר אלקטרוני"];
    [_txtEmail setTextAlignment:NSTextAlignmentRight];
    _txtEmail.layer.sublayerTransform = CATransform3DMakeTranslation(-6, 0, 0);
    [self.view addSubview:_txtEmail];
    
    _txtViewComment=[[UITextView alloc]initWithFrame:CGRectMake(10, 314, 300, 94)];
    _txtViewComment.delegate=self;
    [_txtViewComment.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
    [_txtViewComment.layer setBorderWidth:2.0];
    _txtViewComment.layer.cornerRadius = 5;
    _txtViewComment.font = [UIFont fontWithName:@"Helvetica" size:13];
    _txtViewComment.backgroundColor=[UIColor whiteColor];
    _txtViewComment.clipsToBounds = YES;
    [_txtViewComment setTextAlignment:NSTextAlignmentRight];
    _txtViewComment.layer.sublayerTransform = CATransform3DMakeTranslation(-6, 0, 0);
    [self.view addSubview:_txtViewComment];
    
    //add button
    _btnSend =[[UIButton alloc]init];
    _btnSend.layer.frame= CGRectMake(162 , 425, 148, 30);
    _btnSend.layer.cornerRadius=4;
    [_btnSend setTitle:@"שלח" forState:UIControlStateNormal];
    [_btnSend addTarget:self action:@selector(btnOkClick:) forControlEvents:UIControlEventTouchUpInside];
    _btnSend.backgroundColor=self.appManager.lightBlue;
    [self.view addSubview:_btnSend];
    
    _btnCancel =[[UIButton alloc]init];
    _btnCancel.layer.frame= CGRectMake(10 , 425, 148, 30);
    _btnCancel.layer.cornerRadius=4;
    [_btnCancel setTitle:@"ביטול" forState:UIControlStateNormal];
    [_btnCancel addTarget:self action:@selector(btnCancelClick:) forControlEvents:UIControlEventTouchUpInside];
    _btnCancel.backgroundColor=self.appManager.lightBlue;
    [self.view addSubview:_btnCancel];
    
    ///add line label
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, 462, 320, 6)];
    lblLine.backgroundColor=self.appManager.lightBlue;
    [self.view addSubview:lblLine];
}
- (void)textViewDidChange:(UITextView *)textView
{
    if([textView.text length] == 0)
    {
        textView.text=@"תאור המפגע";
        textView.textColor=[UIColor lightGrayColor];
    }
    if([textView.text isEqualToString:@"תאור המפגע"])
    {
        textView.text=@"";
        textView.textColor=[UIColor blackColor];
    }
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if([textView.text isEqualToString:@"תאור המפגע"])
    {
        textView.text=@"";
        textView.textColor=[UIColor blackColor];
    }
}

@end