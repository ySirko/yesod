
//
//  InheritorFromSubViewController.m
//  NewCrmc
//
//  Created by WebitMacMini Two on 7/30/14.
//  Copyright (c) 2014 webit. All rights reserved.
//
#import "InheritorFromSubViewController.h"
@interface InheritorFromSubViewController ()//מי שצריך הודעות רצות
{
    int index;
}
@end

@implementation InheritorFromSubViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _lblRunningMessage=[[UILabel alloc]initWithFrame:CGRectMake(-320, 106, 320, 21)];
    [self.view addSubview:_lblRunningMessage];
    [self.generic showNativeActivityIndicator:self];
//    [self.connection connectionToService:@"GetFlashesTocity" jsonDictionary:[self.appDelegate.city parseCityToDict] controller:self withSelector:@selector(GetFlashesTocityResult:)];
    NSMutableDictionary*dict=[[NSMutableDictionary alloc]init];
    NSMutableDictionary*dic=[[NSMutableDictionary alloc]init];
    [dic setObject:@"flash" forKey:@"typeName"];// ארועים
    [dic setObject:[NSString stringWithFormat:@"%d",self.appDelegate.city.cityId] forKey:@"cityId"];
    [dict setObject:dic forKey:@"type"];
    [self.connection connectionToService:@"GetEventsByCityAndTypeId" jsonDictionary:dict controller:self withSelector:@selector(GetEventsByCityIdResult:)];
}

-(void)GetEventsByCityIdResult:(NSString*)result
{     NSString*s=@"<?xml";

    [self.generic hideNativeActivityIndicator:self];
//    if([s ([string rangeOfString:s].location == NSNotFound)])
    if(![result isEqualToString:@""]&&![result isEqualToString:@"[]"]&&([result rangeOfString:s].location == NSNotFound))
    {
        Event*event=[[Event alloc]init];
        _listMsg=[[NSArray alloc]initWithArray:[event parseListEventFromJson:result]];
        _lblRunningMessage.text=((Event*)_listMsg[0]).title;
    }
    else
        NSLog(@"Empty flashes list");
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    if (CGRectContainsPoint(CGRectMake(0, 103, 320, 21), [touch locationInView:self.view]))
    {
        _flag=YES;
        WebViewController* view=[[WebViewController alloc]init];
        ((WebViewController*)view).nsurl=[[NSString alloc]init];
        ((WebViewController*)view).nsurl=((Event*)_listMsg[index]).link;
        [self.navigationController pushViewController:view animated:YES];
    }
}
-(void)viewDidAppear:(BOOL)animated
{
    _flag=NO;
   [self anim];
}

-(void)anim
{
    if(!_flag)
    {
        [UIView animateWithDuration:8.0
                              delay:0.0
                            options: UIViewAnimationOptionAllowAnimatedContent
                         animations:^{_lblRunningMessage.frame=CGRectMake(320, _lblRunningMessage.frame.origin.y,_lblRunningMessage.frame.size.width, _lblRunningMessage.frame.size.height);}
                         completion:^(BOOL finished){
                             index++;
                             if(index==_listMsg.count)index=0;
                             _lblRunningMessage.text=((Event*)_listMsg[index]).title;
                             _lblRunningMessage.frame=CGRectMake(-320, _lblRunningMessage.frame.origin.y,_lblRunningMessage.frame.size.width, _lblRunningMessage.frame.size.height);
                             [self anim];
                         }];
    }
}
- (void)name
{
    _flag=YES;
}
@end
