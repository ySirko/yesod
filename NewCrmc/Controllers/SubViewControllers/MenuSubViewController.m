//
//  MenuSubViewController.m
//  NewCrmc
//
//  Created by MyMac on 8/12/14.
//  Copyright (c) 2014 webit. All rights reserved.
//
#import "MenuSubViewController.h"
#import "EventsTableViewController.h"
#import "ContactUsViewController.h"
#import "MayorViewController.h"
#import "MapImageViewController.h"
@interface MenuSubViewController ()
{
    UIViewController*view;
}
@end

@implementation MenuSubViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.multipleTouchEnabled=NO;
   // NSString*s=SubViewController.yesButWhichDeviceIsIt;
    self.bt.hidden=true;
    _btn1=[[UIButton alloc]initWithFrame:CGRectMake(208, 180, 86, 86)];
    _btn2=[[UIButton alloc]initWithFrame:CGRectMake(116, 180,  86, 86)];
    _btn3=[[UIButton alloc]initWithFrame:CGRectMake(24, 180,  86, 86)];
    _btn4=[[UIButton alloc]initWithFrame:CGRectMake(208, 271, 86, 86)];
    _btn5=[[UIButton alloc]initWithFrame:CGRectMake(116, 271, 86, 86)];
    _btn6=[[UIButton alloc]initWithFrame:CGRectMake(24, 271, 86, 86)];
    _btn7=[[UIButton alloc]initWithFrame:CGRectMake(208, 361, 86, 86)];
    _btn8=[[UIButton alloc]initWithFrame:CGRectMake(116, 361, 86, 86)];
    _btn9=[[UIButton alloc]initWithFrame:CGRectMake(24, 361, 86, 86)];
    _icon1=[[UIImageView alloc]initWithFrame:CGRectMake(237, 199, 30, 30)] ;
    _icon2=[[UIImageView alloc]initWithFrame:CGRectMake(145, 199, 30, 30)];
    _icon3=[[UIImageView alloc]initWithFrame:CGRectMake(53, 199, 30, 30) ];
    _icon4=[[UIImageView alloc]initWithFrame:CGRectMake(237, 291, 30, 30)] ;
    _icon5=[[UIImageView alloc]initWithFrame:CGRectMake(145, 291, 30, 30)] ;
    _icon6=[[UIImageView alloc]initWithFrame:CGRectMake(53, 291, 30, 30)] ;
    _icon7=[[UIImageView alloc]initWithFrame:CGRectMake(237, 380, 30, 30)] ;
    _icon8=[[UIImageView alloc]initWithFrame:CGRectMake(145, 380, 30, 30)] ;
    _icon9=[[UIImageView alloc]initWithFrame:CGRectMake(53, 380, 30, 30)] ;
    [self setFrame];
    _btnarray=[[NSArray alloc]initWithObjects:_btn1,_btn2,_btn3,_btn4,_btn5,_btn6,_btn7,_btn8,_btn9, nil];
    _btnIconArray=[[NSArray alloc]initWithObjects:_icon1,_icon2,_icon3,_icon4,_icon5,_icon6,_icon7,_icon8,_icon9, nil];
    
    for (int i=0; i<_listMenu.count; i++)
    {
        UIButton*btn=(UIButton*)_btnarray[i];
        Menu*menu=_listMenu[i];
        btn.backgroundColor=menu.color;
        btn.tag=menu.ID;
        btn.hidden=NO;
        [btn setTitle:menu.name forState:UIControlStateNormal];
        btn.titleLabel.numberOfLines=2;
        btn.titleLabel.textAlignment=NSTextAlignmentCenter;
        btn.layer.cornerRadius=6;
        [btn.titleLabel setFont:[UIFont systemFontOfSize:13.5]];
        btn.contentEdgeInsets = UIEdgeInsetsMake(44, 0, 0, 0);
        [btn addTarget:self action:@selector(TouchDown:) forControlEvents:UIControlEventTouchDown];
        [btn addTarget:self action:@selector(TouchUpOutside:) forControlEvents:UIControlEventTouchUpOutside];
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        ((UIImageView*) _btnIconArray[i]).image=menu.icon;
        [self.view addSubview:btn];
        [self.view addSubview:(UIImageView*)_btnIconArray[i]];
    }
    _btn1.exclusiveTouch = YES;
    _btn2.exclusiveTouch = YES;
    _btn3.exclusiveTouch = YES;
    _btn4.exclusiveTouch = YES;
    _btn5.exclusiveTouch = YES;
    _btn6.exclusiveTouch = YES;
    _btn7.exclusiveTouch = YES;
    _btn8.exclusiveTouch = YES;
    _btn9.exclusiveTouch = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)TouchDown:(UIButton*)sender
{
    NSArray* filterArray = [_listMenu filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"ID =[cd] %d",((UIButton*) sender).tag]];
    sender.backgroundColor=((Menu*)filterArray[0]).HighlightedColor;
}
-(void)TouchUpOutside:(UIButton*)sender
{
    NSArray* filterArray = [_listMenu filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"ID =[cd] %d",((UIButton*) sender).tag]];
    sender.backgroundColor=((Menu*)filterArray[0]).color;
}

-(void)btnClick:(UIButton*)sender
{
    self.flag=YES;
    NSArray* filterArray = [_listMenu filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"ID =[cd] %d",((UIButton*) sender).tag]];
    Menu*m=filterArray[0];
    ((UIButton*) sender).backgroundColor=m.color;
    switch (m.active)
    {
        case 1:
            switch (m.tag)
            {
                case 1://אירועים
                case 2://חדשות ועדכונים
                case 3://הודעות עבורך
                case 4://מסלולים
                case 5://אתרי מורשת,תיירות
                case 14://גופי סמך
                case 15://מבלים בעיר
                case 18:
                case 19:
                case 21:
                    view=[[EventsTableViewController alloc]init];
                    ((EventsTableViewController*)view).index=m.tag;
                    ((EventsTableViewController*)view).color=self.color;
                    ((EventsTableViewController*)view).highlightedColor=m.HighlightedColor;
                    ((EventsTableViewController*)view).chetz=m.strColor;
                    ((EventsTableViewController*)view).highlightedChetz=[NSString stringWithFormat:@"%@Highlighted",m.strColor];
                    ((EventsTableViewController*)view).header=m.name;
                          ((EventsTableViewController*)view).link=m.link;
                    break;
                case 6://טלפונים
                    view=[[CouncilOfficialsViewController alloc]init];
                    ((CouncilOfficialsViewController*)view).color=self.color;
                    ((CouncilOfficialsViewController*)view).strColor=m.strColor;
                    ((CouncilOfficialsViewController*)view).header=m.name;
                    break;
                case 7://מפה
                    view=[[iCodeMapViewController alloc]init];
                    ((iCodeMapViewController*)view).color=self.color;
                    ((iCodeMapViewController*)view).header=m.name;
                    ((iCodeMapViewController*)view).strColor=m.strColor;
                    break;
                case 8://פניה חדשה
                    view=[[NewAppealViewController alloc]init];
                    ((NewAppealViewController*)view).color=self.color;
                    ((NewAppealViewController*)view).header=m.name;
                    break;
                case 9://הפניות שלי
                    view=[[MyAppealTableViewController alloc]init];
                    ((MyAppealTableViewController*)view).color=self.color;
                    ((MyAppealTableViewController*)view).header=m.name;
                    break;
                case 10://דבר ראש העיר
                case 11://אודות העיר
                case 12://שעת חירום
                case 20://מידע  על העיר
                    view = [[MayorViewController alloc] init];
                    if(m.tag == 10)
                    {
                        [self.generic showNativeActivityIndicator:self];
                        [self.connection connectionToService:@"GetMayorImage" jsonDictionary:[self.appDelegate.city parseCityToDict] controller:self withSelector:@selector(GetMayorImageResult:)];
                    }
                    if (self.appDelegate.city.cityId == 5 && m.tag == 11) {
                        ((MayorViewController*)view).imgMayor=[[UIImageView alloc]initWithFrame:CGRectMake(200, 10, 110, 84)];
                        ((MayorViewController*)view).imgMayor.image=[UIImage imageNamed:@"tzvika.jgp"];
                        ((MayorViewController*)view).txtview.textContainer.exclusionPaths  = @[[UIBezierPath bezierPathWithRect:CGRectMake(((MayorViewController*)view).txtview.frame.size.width-((MayorViewController*)view).imgMayor.frame.size.width+10,0, ((MayorViewController*)view).imgMayor.frame.size.width+10, ((MayorViewController*)view).imgMayor.frame.size.height)]];
                        [((MayorViewController*)view).txtview addSubview:((MayorViewController *)view).imgMayor];
                    }
                    
                    ((MayorViewController*)view).index = m.tag;
                    ((MayorViewController*)view).color = self.color;
                    ((MayorViewController*)view).header = m.name;
                    [((MayorViewController*)view).txtview setSelectable:YES];

                    break;
                case 13://סקרים
                    view=[[SurveyViewController alloc]init];
                    ((SurveyViewController*)view).color=self.color;
                    ((SurveyViewController*)view).header=m.name;
                    break;
                case 16:
                    view=[[MapImageViewController alloc]init];
                    ((MapImageViewController*)view).color=m.color;
                    ((MapImageViewController*)view).header=m.name;
                    break;
                default:
                    break;
            }
            [self.navigationController pushViewController:view animated:YES];
            break;
        case 2:
        {
         if(m.tag==1)
              [[UIApplication sharedApplication] openURL:[NSURL URLWithString:m.link]];
           else
          {
            view=[[WebViewController alloc]init];
            ((WebViewController*)view).nsurl=[[NSString alloc]init];
            ((WebViewController*)view).nsurl=m.link;
              if (m.tag == 999) {
                  ((WebViewController*)view).nsurl = @"http://www.yesud-hamaala.muni.il/פרוטוקולים.html";
              }
            ((WebViewController*)view).header=m.name;
            ((WebViewController*)view).color=_color;
            [self.navigationController pushViewController:view animated:YES];
        }
        }
            break;
        case 3:
            view=[[MenuSubViewController alloc]init];
            ((MenuSubViewController*)view).listMenu=m.listMenu;
            ((MenuSubViewController*)view).color=m.color;
            ((MenuSubViewController*)view).header=m.name;
            [self.navigationController pushViewController:view animated:YES];
            break;
        case 4:
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:m.phone]];
            break;
        case 5:
        {
            MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
            [mc setToRecipients:[NSArray arrayWithObject:m.email]];
            [mc setSubject:m.emailSubject];
            mc.mailComposeDelegate=self;
            [self presentViewController:mc animated:YES completion:NULL];
            break;
        }
        default:
            break;
    }
}
-(void)GetMayorImageResult:(NSString*)result
{
    if(![result isEqualToString:@""] && ![result isEqualToString:@"\"\""])
    {
        ((MayorViewController *)view).imgMayor.image=[UIImage imageWithData:[NSData dataFromBase64String:[NSString stringWithFormat:@"%@", result]]];
    }
    [self.generic hideNativeActivityIndicator:self];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
        case MessageComposeResultFailed:
        {
            UIAlertView* msg=[[UIAlertView alloc]initWithTitle:@"error" message:@"failed" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles: nil];
            [msg show];
        }
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}
-(void)setFrame
{
    int constraintLeftSide=27;
    int constraintLeftSideIcon=constraintLeftSide+28;
    int constraintRightSide=-111;
    int constraintRightSideIcon=constraintRightSide+28;
    int widthButton=86;
    int widthIcon=30;
    int space=5;
    int wIcon=self.view.frame.size.width+constraintRightSideIcon;
    int xIcon=constraintLeftSideIcon+widthIcon;
    int spaceIcon=((wIcon-xIcon)-widthIcon)/2;
    int yButton=180;
    int spaceInButton=23;
    
    _btn1.layer.frame= CGRectMake(self.view.frame.size.width+constraintRightSide , yButton, widthButton, widthButton);
    _btn2.layer.frame= CGRectMake(constraintLeftSide+widthButton+space , yButton, 86, 86);
    _btn3.layer.frame= CGRectMake(constraintLeftSide , yButton, 86, 86);
    _btn4.layer.frame= CGRectMake(self.view.frame.size.width+constraintRightSide ,yButton+widthButton+space,widthButton, widthButton);
    _btn5.layer.frame= CGRectMake(constraintLeftSide+widthButton+space , yButton+widthButton+space,widthButton,widthButton);
    _btn6.layer.frame= CGRectMake(constraintLeftSide , yButton+widthButton+space,widthButton,widthButton);
    _btn7.layer.frame= CGRectMake(self.view.frame.size.width+constraintRightSide , yButton+((widthButton+space)*2),widthButton, widthButton);
    _btn8.layer.frame= CGRectMake(constraintLeftSide+widthButton+space , yButton+((widthButton+space)*2),widthButton,widthButton);
    _btn9.layer.frame= CGRectMake(constraintLeftSide ,  yButton+((widthButton+space)*2),widthButton,widthButton);
    
    
    _icon1.frame=CGRectMake(self.view.frame.size.width+constraintRightSideIcon,yButton+spaceInButton,widthIcon, widthIcon);
    _icon2.frame=CGRectMake(constraintLeftSideIcon+widthIcon+spaceIcon,yButton+spaceInButton, widthIcon, widthIcon);
    _icon3.frame=CGRectMake(constraintLeftSideIcon,yButton+spaceInButton, widthIcon, widthIcon);
    _icon4.frame=CGRectMake(self.view.frame.size.width+constraintRightSideIcon,yButton+widthButton+space+spaceInButton, widthIcon, widthIcon);
    _icon5.frame=CGRectMake(constraintLeftSideIcon+widthIcon+spaceIcon,yButton+widthButton+space+spaceInButton, widthIcon, widthIcon);
    _icon6.frame=CGRectMake(constraintLeftSideIcon,yButton+widthButton+space+spaceInButton, widthIcon, widthIcon);
    _icon7.frame=CGRectMake(self.view.frame.size.width+constraintRightSideIcon,yButton+((widthButton+space)*2)+spaceInButton, widthIcon, widthIcon);
    _icon8.frame=CGRectMake(constraintLeftSideIcon+widthIcon+spaceIcon,yButton+((widthButton+space)*2)+spaceInButton, widthIcon, widthIcon);
    _icon9.frame=CGRectMake(constraintLeftSideIcon,yButton+((widthButton+space)*2)+spaceInButton, widthIcon, widthIcon);
    
    
    
    
    
}


@end
