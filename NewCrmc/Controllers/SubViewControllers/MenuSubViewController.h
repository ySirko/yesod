//
//  MenuSubViewController.h
//  NewCrmc
//
//  Created by MyMac on 8/12/14.
//  Copyright (c) 2014 webit. All rights reserved.
//
#import "InheritorFromSubViewController.h"
@interface MenuSubViewController : InheritorFromSubViewController
@property(retain,nonatomic) UIButton*btn1;
@property(retain,nonatomic) UIButton*btn2;
@property(retain,nonatomic) UIButton*btn3;
@property(retain,nonatomic) UIButton*btn4;
@property(retain,nonatomic) UIButton*btn5;
@property(retain,nonatomic) UIButton*btn6;
@property(retain,nonatomic) UIButton*btn7;
@property(retain,nonatomic) UIButton*btn8;
@property(retain,nonatomic) UIButton*btn9;
@property(retain,nonatomic) UIImageView*icon1;
@property(retain,nonatomic) UIImageView*icon2;
@property(retain,nonatomic) UIImageView*icon3;
@property(retain,nonatomic) UIImageView*icon4;
@property(retain,nonatomic) UIImageView*icon5;
@property(retain,nonatomic) UIImageView*icon6;
@property(retain,nonatomic) UIImageView*icon7;
@property(retain,nonatomic) UIImageView*icon8;
@property(retain,nonatomic) UIImageView*icon9;
@property(retain,nonatomic) NSMutableArray* listMenu;
@property(retain,nonatomic) NSArray*btnarray;
@property(retain,nonatomic) NSArray*btnIconArray;
@property(retain,nonatomic) UIColor*color;
@end
