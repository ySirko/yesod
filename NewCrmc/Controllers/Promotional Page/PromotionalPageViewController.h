//
//  PromotionalPageViewController.h
//  NewCrmc
//
//  Created by MyMac on 8/31/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubViewController.h"
#import "StringHeader.h"

@interface PromotionalPageViewController : SubViewController

@property (retain, nonatomic) UIImageView *imageView;

@end
