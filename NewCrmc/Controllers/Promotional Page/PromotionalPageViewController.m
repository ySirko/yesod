//
//  PromotionalPageViewController.m
//  NewCrmc
//
//  Created by MyMac on 8/31/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "PromotionalPageViewController.h"
#import "HomeViewController.h"
#import "Menu.h"
#import "NSData+Base64.h"
#import "SubViewController.h"
@interface PromotionalPageViewController ()
{
    BOOL flag;
}
@end

@implementation PromotionalPageViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName=@"PromotionalPageViewController";
    for(UIView *subview in [self.view subviews])
        [subview removeFromSuperview];
    _imageView.layer.frame= CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:_imageView];
    
    UIButton*btn1=[[UIButton alloc]initWithFrame:CGRectMake(3, 13, 46, 30)];
    btn1.titleLabel.font=[UIFont fontWithName:@"" size:22];
    btn1.backgroundColor=[UIColor clearColor];
    btn1.titleLabel.textAlignment=NSTextAlignmentRight;
    [btn1 setTitle:@"X" forState:UIControlStateNormal];
    [btn1 addTarget:self action:@selector(btnXClick:) forControlEvents:UIControlEventTouchUpInside];
    btn1.tag=5;
    [btn1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:btn1];
    
    [self.generic showNativeActivityIndicator:self];
    [self.connection connectionToService:@"GetCityImage" jsonDictionary:[self.appDelegate.city parseCityToDict] controller:self withSelector:@selector(GetCityImageResult:)];
    switch (self.appDelegate.city.cityId)
    {
        case 1:
            [self setMenu1];
            break;
        case 10:
            [self setMenu10];
            break;
        case 9:
            [self setMenu9];
            break;
        case 5:
            [self setMenu5];
            break;
        case 13:
            [self setMenu13];
            break;
        case 18:
            [self setMenu18];
            break;
        default:
            break;
    }
}

-(void)GetCityImageResult:(NSString*)result
{
    if(![result isEqualToString:@"\"\""]&&![result isEqualToString:@"[]"]&&![result isEqualToString:@""])
    {
        UIImage *image=[UIImage imageWithData:[NSData dataFromBase64String:[NSString stringWithFormat:@"%@", result]]];
        self.appDelegate.back=image;
    }
    else
    {
        self.appDelegate.back=[UIImage imageNamed:@"Background.jpg"];
    }
    [self.generic showNativeActivityIndicator:self];
    [self.connection connectionToService:@"GetCityContents" jsonDictionary:[self.appDelegate.city parseCityToDict] controller:self withSelector:@selector(GetCityContentsResult:)];
}

-(void)GetCityContentsResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if(![result isEqualToString:@""]&&![result isEqualToString:@"[]"])
    {
        self.appDelegate.cityContent=[[CityContent alloc]init];
        self.appDelegate.cityContent=[self.appDelegate.cityContent parseCityContentFromJson:result];
//        [self.generic showNativeActivityIndicator:self];
//        [self.connection connectionToService:@"GetStructureByCityId" jsonDictionary:[self.appDelegate.city parseCityToDict] controller:self withSelector:@selector(GetStructureByCityIdResult:)];
    }
    else
        NSLog(@"Empty reports list");
    [self performSelector:@selector(onTick:) withObject:nil afterDelay:2.0];
}
-(void)GetStructureByCityIdResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if(![result isEqualToString:@""]&&![result isEqualToString:@"[]"])
    {
        Menu*menu=[[Menu alloc]init];
        self.appDelegate.menu=[menu parseListMenuFromJson:result];
    }
}

- (IBAction)btnXClick:(id)sender
{
    HomeViewController*viewController=[[HomeViewController alloc]init];
    [self.navigationController pushViewController:viewController animated:YES];
    flag=YES;
}

-(void)onTick:(NSTimer *)timer
{
    [timer invalidate];
    timer=nil;
    if(!flag)
    {
        HomeViewController*viewController=[[HomeViewController alloc]init];
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

-(void)setMenu13
{

    Menu*Menu2_1=[[Menu alloc]init];
    Menu2_1.ID=8;
    Menu2_1.menuID=3;
    Menu2_1.name=@"חיוג למוקד";
    Menu2_1.active=4;
    Menu2_1.tag=0;
    Menu2_1.color=self.appManager.green;
    Menu2_1.HighlightedColor=self.appManager.greenHighlighted;
    Menu2_1.strColor=@"green";
    Menu2_1.icon=[UIImage imageNamed:@"phonIcon"];
    Menu2_1.link=@"";
    Menu2_1.phone=@"tel:03-9723090";
    Menu2_1.listMenu=nil;
    
    Menu*Menu2_2=[[Menu alloc]init];
    Menu2_2.ID=9;
    Menu2_2.menuID=3;
    Menu2_2.name=@"פניה חדשה";
    Menu2_2.active=1;
    Menu2_2.tag=8;
    Menu2_2.color=self.appManager.green2;
    Menu2_2.HighlightedColor=self.appManager.greenHighlighted2;
    Menu2_2.strColor=@"green";
    Menu2_2.icon=[UIImage imageNamed:@"ApealsIcon"];
    Menu2_2.link=@"";
    Menu2_2.listMenu=nil;
    
    Menu*Menu2_3=[[Menu alloc]init];
    Menu2_3.ID=10;
    Menu2_3.menuID=3;
    Menu2_3.name=@"הפניות שלי";
    Menu2_3.active=1;
    Menu2_3.tag=9;
    Menu2_3.color=self.appManager.green;
    Menu2_3.HighlightedColor=self.appManager.greenHighlighted;
    Menu2_3.strColor=@"green";
    Menu2_3.icon=[UIImage imageNamed:@"MyApealsIcon"];
    Menu2_3.link=@"";
    Menu2_3.listMenu=nil;
    
    Menu*Menu2=[[Menu alloc]init];
    Menu2.ID=2;
    Menu2.menuID=0;
    Menu2.name=@"פניות למוקד";
    Menu2.active=3;
    Menu2.tag=0;
    Menu2.color=self.appManager.green;
    Menu2.HighlightedColor=self.appManager.greenHighlighted;
    Menu2.strColor=@"green";
    Menu2.icon=[UIImage imageNamed:@"ApealsIcon.png"];
    Menu2.link=@"";
    Menu2.listMenu=[NSMutableArray arrayWithObjects:Menu2_1,Menu2_2,Menu2_3, nil];
    
//    Menu*Menu1_1=[[Menu alloc]init];
//    Menu1_1.ID=7;
//    Menu1_1.menuID=1;
//    Menu1_1.name=@"שעת חירום";
//    Menu1_1.active=1;
//    Menu1_1.tag=12;
//    Menu1_1.color=self.lightBlue;
//    Menu1_1.HighlightedColor=self.lightBlueHighlighted;
//    Menu1_1.strColor=@"blue";
//    Menu1_1.icon=[UIImage imageNamed:@"EmergencyIcon"];
//    Menu1_1.link=@"";
//    Menu1_1.listMenu=nil;
    
    Menu*Menu1_2=[[Menu alloc]init];
    Menu1_2.ID=11;
    Menu1_2.menuID=1;
    Menu1_2.name=@"טלפונים";
    Menu1_2.active=1;
    Menu1_2.tag=6;
    Menu1_2.color=self.appManager.lightBlue;
    Menu1_2.HighlightedColor=self.appManager.lightBlueHighlighted;
    Menu1_2.strColor=@"blue";
    Menu1_2.icon=[UIImage imageNamed:@"phonIcon"];
    Menu1_2.link=@"";
    Menu1_2.listMenu=nil;
    
    Menu*Menu1_3=[[Menu alloc]init];
    Menu1_3.ID=12;
    Menu1_3.menuID=1;
    Menu1_3.name=@"מפה";
    Menu1_3.active=1;
    Menu1_3.tag=7;
    Menu1_3.color=self.appManager.lightBlue2;
    Menu1_3.HighlightedColor=self.appManager.lightBlueHighlighted2;
    Menu1_3.strColor=@"blue";
    Menu1_3.icon=[UIImage imageNamed:@"iconMap"];
    Menu1_3.link=@"";
    Menu1_3.listMenu=nil;
    
//    Menu*Menu1_4=[[Menu alloc]init];
//    Menu1_4.ID=14;
//    Menu1_4.menuID=2;
//    Menu1_4.name=@"תשלומים";
//    Menu1_4.active=2;
//    Menu1_4.tag=0;
//    Menu1_4.color=self.lightBlue2;
//    Menu1_4.HighlightedColor=self.lightBlueHighlighted;
//    Menu1_4.strColor=@"blue";
//    Menu1_4.icon=[UIImage imageNamed:@"iconPay"];
//    Menu1_4.link=@"https://www.citypay.co.il/Openning.asp?Lang=1&Bank=&CityName=";
//    Menu1_4.listMenu=nil;
    
    Menu*Menu1_5=[[Menu alloc]init];
    Menu1_5.ID=16;
    Menu1_5.menuID=1;
    Menu1_5.name=@"חדשות";
    Menu1_5.active=1;
    Menu1_5.tag=2;
    Menu1_5.color=self.appManager.lightBlue;
    Menu1_5.HighlightedColor=self.appManager.lightBlueHighlighted;
    Menu1_5.strColor=@"blue";
    Menu1_5.icon=[UIImage imageNamed:@"NewsIcon"];
    Menu1_5.link=@"";
    Menu1_5.listMenu=nil;
    
    Menu*Menu1_6=[[Menu alloc]init];
    Menu1_6.ID=13;
    Menu1_6.menuID=1;
    Menu1_6.name=@"מידע סביבתי";
    Menu1_6.active=1;
    Menu1_6.tag=11;
    Menu1_6.color=self.appManager.lightBlue2;
    Menu1_6.HighlightedColor=self.appManager.lightBlueHighlighted2;
    Menu1_6.strColor=@"blue";
    Menu1_6.icon=[UIImage imageNamed:@"AboutIcon"];
    Menu1_6.link=@"";
    Menu1_6.listMenu=nil;
    
    
    Menu*Menu1_7=[[Menu alloc]init];
    Menu1_7.ID=14;
    Menu1_7.menuID=1;
    Menu1_7.name=@"דיוור ישיר";
    Menu1_7.active=1;
    Menu1_7.tag=18;
    Menu1_7.color=self.appManager.lightBlue;
    Menu1_7.HighlightedColor=self.appManager.lightBlueHighlighted;
    Menu1_7.strColor=@"blue";
    Menu1_7.icon=[UIImage imageNamed:@"AboutIcon"];
    Menu1_7.link=@"http://www.shoham.muni.il/RSS/Articles.asp?CategoryID=210";
    Menu1_7.listMenu=nil;

    Menu*Menu1_8=[[Menu alloc]init];
    Menu1_8.ID=15;
    Menu1_8.menuID=1;
    Menu1_8.name=@"הודעות עבורך";
    Menu1_8.active=1;
    Menu1_8.tag=3;
    Menu1_8.color=self.appManager.lightBlue2;
    Menu1_8.HighlightedColor=self.appManager.lightBlueHighlighted2;
    Menu1_8.strColor=@"blue";
    Menu1_8.icon=[UIImage imageNamed:@"MassegsIcon"];
    Menu1_8.link=@"";
    Menu1_8.listMenu=nil;
    
    Menu*Menu1_9=[[Menu alloc]init];
    Menu1_9.ID=19;
    Menu1_9.menuID=1;
    Menu1_9.name=@"מידע לחירום";
    Menu1_9.active=1;
    Menu1_9.tag=18;
    Menu1_9.color=[UIColor redColor];
    Menu1_9.HighlightedColor=self.appManager.redHighlighted;
    Menu1_9.strColor=@"blue";
    Menu1_9.icon=[UIImage imageNamed:@"EmergencyIcon.png"];
    Menu1_9.link=@"http://www.shoham.muni.il/RSS/Articles.asp?CategoryID=167";
    Menu1_9.listMenu=nil;
    
    
    Menu*Menu1=[[Menu alloc]init];
    Menu1.ID=1;
    Menu1.menuID=0;
    Menu1.name=@"המועצה שלי";
    Menu1.active=3;
    Menu1.tag=0;
    Menu1.color=self.appManager.lightBlue;
    Menu1.HighlightedColor=self.appManager.lightBlueHighlighted;
    Menu1.strColor=@"orange";
    Menu1.icon=[UIImage imageNamed:@"MyCityHallIcon"];
    Menu1.link=@"";
    Menu1.listMenu=[NSMutableArray arrayWithObjects:Menu1_2,Menu1_3,Menu1_5,Menu1_6,Menu1_7,Menu1_8,Menu1_9, nil ];
    
    Menu*Menu4=[[Menu alloc]init];
    Menu4.ID=4;
    Menu4.menuID=0;
    Menu4.name=@"אתר חמש";
    Menu4.active=3;
    Menu4.tag=0;
    Menu4.color=self.appManager.purple;
    Menu4.HighlightedColor=self.appManager.purpleHighlighted;
    Menu4.strColor=@"purple";
    Menu4.icon=[UIImage imageNamed:@"MassegsIcon"];
    Menu4.link=@"http://www.hamesh.co.il";
    Menu4.listMenu=nil;
    
    Menu*Menu3=[[Menu alloc]init];
    Menu3.ID=3;
    Menu3.menuID=0;
    Menu3.name=@"פייסבוק";
    Menu3.active=2;
    Menu3.tag=0;
    Menu3.color=self.appManager.orange;
    Menu3.HighlightedColor=self.appManager.orangeHighlighted;
    Menu3.strColor=@"orange";
    Menu3.icon=[UIImage imageNamed:@"iconPay"];
    Menu3.link=@"https://www.facebook.com/shoham.isr?fref=ts";
    Menu3.listMenu=nil;
    
    Menu*Menu5=[[Menu alloc]init];
    Menu5.ID=5;
    Menu5.menuID=0;
    Menu5.name=@"לוח אירועים";
    Menu5.active=1;
    Menu5.tag=1;
    Menu5.color=self.appManager.pink;
    Menu5.HighlightedColor=self.appManager.pinkHighlighted;
    Menu5.strColor=@"pink";
    Menu5.icon=[UIImage imageNamed:@"EventIcon"];
    Menu5.link=@"";
    Menu5.listMenu=nil;
    
    Menu*Menu6=[[Menu alloc]init];
    Menu6.ID=6;
    Menu6.menuID=0;
    Menu6.name=@"אתר המועצה";
    Menu6.active=2;
    Menu6.tag=0;
    Menu6.color=[UIColor redColor];
    Menu6.HighlightedColor=self.appManager.redHighlighted;
    Menu6.strColor=@"red";
    Menu6.icon=[UIImage imageNamed:@"CitySideIcon"];
    Menu6.link=@"http://www.shoham.muni.il/";
    Menu6.listMenu=nil;
    self.appDelegate.menu=[[NSArray alloc]initWithObjects:Menu1,Menu2,Menu3,Menu4,Menu5,Menu6 ,nil];
}

-(void)setMenu5
{
    Menu*Menu1_1=[[Menu alloc]init];
    Menu1_1.ID=7;
    Menu1_1.menuID=1;
    Menu1_1.name=@"אירועים";
    Menu1_1.active=1;
    Menu1_1.tag=1;
    Menu1_1.color=self.appManager.lightBlue;
    Menu1_1.HighlightedColor=self.appManager.lightBlueHighlighted;
    Menu1_1.strColor=@"blue";
    Menu1_1.icon=[UIImage imageNamed:@"EventIcon.png"];
    Menu1_1.link=@"";
    Menu1_1.listMenu=nil;
    
    Menu*Menu1_2=[[Menu alloc]init];
    Menu1_2.ID=11;
    Menu1_2.menuID=1;
    Menu1_2.name=@"טלפונים";
    Menu1_2.active=1;
    Menu1_2.tag=6;
    Menu1_2.color=self.appManager.lightBlue2;
    Menu1_2.HighlightedColor=self.appManager.lightBlueHighlighted2;
    Menu1_2.strColor=@"blue";
    Menu1_2.icon=[UIImage imageNamed:@"phonIcon"];
    Menu1_2.link=@"";
    Menu1_2.listMenu=nil;
    
    Menu*Menu1_3=[[Menu alloc]init];
    Menu1_3.ID=12;
    Menu1_3.menuID=1;
    Menu1_3.name=@"תיק תושב";
    Menu1_3.active=2;
    Menu1_3.tag=0;
    Menu1_3.color=self.appManager.lightBlue;
    Menu1_3.HighlightedColor=self.appManager.lightBlueHighlighted;
    Menu1_3.strColor=@"blue";
    Menu1_3.icon=[UIImage imageNamed:@"iconRationalizationproposals.png"];
    Menu1_3.link=@"https://www.city4u.co.il/v2/default.aspx?SiteID=8&CustomerID=66&theme";
    Menu1_3.listMenu=nil;
    
    Menu*Menu1_4=[[Menu alloc]init];
    Menu1_4.ID=14;
    Menu1_4.menuID=2;
    Menu1_4.name=@"אודות העיר";
    Menu1_4.active=1;
    Menu1_4.tag=11;
    Menu1_4.color=self.appManager.lightBlue2;
    Menu1_4.HighlightedColor=self.appManager.lightBlueHighlighted;
    Menu1_4.strColor=@"blue";
    Menu1_4.icon=[UIImage imageNamed:@"AboutIcon"];
    Menu1_4.link=@"";
    Menu1_4.listMenu=nil;
    
    Menu*Menu1_5=[[Menu alloc]init];
    Menu1_5.ID=16;
    Menu1_5.menuID=1;
    Menu1_5.name=@"אתר העיריה";
    Menu1_5.active=2;
    Menu1_5.tag=0;
    Menu1_5.color=self.appManager.lightBlue;
    Menu1_5.HighlightedColor=self.appManager.lightBlueHighlighted;
    Menu1_5.strColor=@"blue";
    Menu1_5.icon=[UIImage imageNamed:@"CitySideIcon"];
    Menu1_5.link=@"http://www.migdal-haemeq.muni.il/Pages/default.aspx";
    Menu1_5.listMenu=nil;
    
    Menu*Menu1_6=[[Menu alloc]init];
    Menu1_6.ID=13;
    Menu1_6.menuID=1;
    Menu1_6.name=@"דבר ראש העיר";
    Menu1_6.active=1;
    Menu1_6.tag=10;
    Menu1_6.color=self.appManager.lightBlue2;
    Menu1_6.HighlightedColor=self.appManager.lightBlueHighlighted2;
    Menu1_6.strColor=@"blue";
    Menu1_6.icon=[UIImage imageNamed:@"MayorIcon.png"];
    Menu1_6.link=@"";
    Menu1_6.listMenu=nil;
    
    Menu*Menu1_7=[[Menu alloc]init];
    Menu1_7.ID=17;
    Menu1_7.menuID=1;
    Menu1_7.name=@"צור קשר";
    Menu1_7.active=5;
    Menu1_7.tag=0;
    Menu1_7.color=self.appManager.lightBlue;
    Menu1_7.HighlightedColor=self.appManager.lightBlueHighlighted;
    Menu1_7.strColor=@"blue";
    Menu1_7.icon=[UIImage imageNamed:@"TelephonIcon.png"];
    Menu1_7.link=@"";
    Menu1_7.email=@"shalomzu@migdal-haemeq.muni.il";
    Menu1_7.emailSubject=@"יצירת קשר אפליקציה";
    Menu1_7.listMenu=nil;
    
    Menu*Menu1_8=[[Menu alloc]init];
    Menu1_8.ID=18;
    Menu1_8.menuID=1;
    Menu1_8.name=@"חדשות";
    Menu1_8.active=1;
    Menu1_8.tag=2;
    Menu1_8.color=self.appManager.lightBlue2;
    Menu1_8.HighlightedColor=self.appManager.lightBlueHighlighted2;
    Menu1_8.strColor=@"blue";
    Menu1_8.icon=[UIImage imageNamed:@"NewsIcon.png"];
    Menu1_8.link=@"";
    Menu1_8.listMenu=nil;
    
    Menu*Menu1_9=[[Menu alloc]init];
    Menu1_9.ID=19;
    Menu1_9.menuID=1;
    Menu1_9.name=@"שעת חירום";
    Menu1_9.active=1;
    Menu1_9.tag=12;
    Menu1_9.color=[UIColor redColor];
    Menu1_9.HighlightedColor=self.appManager.redHighlighted;
    Menu1_9.strColor=@"blue";
    Menu1_9.icon=[UIImage imageNamed:@"EmergencyIcon.png"];
    Menu1_9.link=@"";
    Menu1_9.listMenu=nil;
    
    Menu*Menu1=[[Menu alloc]init];
    Menu1.ID=1;
    Menu1.menuID=0;
    Menu1.name=@"העיריה שלי";
    Menu1.active=3;
    Menu1.tag=0;
    Menu1.color=self.appManager.lightBlue;
    Menu1.HighlightedColor=self.appManager.lightBlueHighlighted;
    Menu1.strColor=@"orange";
    Menu1.icon=[UIImage imageNamed:@"MyCityHallIcon"];
    Menu1.link=@"";
    Menu1.listMenu=[NSMutableArray arrayWithObjects:Menu1_1,Menu1_2,Menu1_3,Menu1_4,Menu1_5,Menu1_6,Menu1_7,Menu1_8,Menu1_9, nil ];
    
    Menu*Menu2_1=[[Menu alloc]init];
    Menu2_1.ID=8;
    Menu2_1.menuID=3;
    Menu2_1.name=@"חיוג למוקד";
    Menu2_1.active=4;
    Menu2_1.tag=0;
    Menu2_1.color=self.appManager.green;
    Menu2_1.HighlightedColor=self.appManager.greenHighlighted;
    Menu2_1.strColor=@"green";
    Menu2_1.icon=[UIImage imageNamed:@"phonIcon"];
    Menu2_1.link=@"";
    Menu2_1.phone=@"tel:106";
    Menu2_1.listMenu=nil;
    
    Menu*Menu2_2=[[Menu alloc]init];
    Menu2_2.ID=9;
    Menu2_2.menuID=3;
    Menu2_2.name=@"פניה חדשה";
    Menu2_2.active=1;
    Menu2_2.tag=8;
    Menu2_2.color=self.appManager.green2;
    Menu2_2.HighlightedColor=self.appManager.greenHighlighted2;
    Menu2_2.strColor=@"green";
    Menu2_2.icon=[UIImage imageNamed:@"ApealsIcon"];
    Menu2_2.link=@"";
    Menu2_2.listMenu=nil;
    
    Menu*Menu2_3=[[Menu alloc]init];
    Menu2_3.ID=10;
    Menu2_3.menuID=3;
    Menu2_3.name=@"הפניות שלי";
    Menu2_3.active=1;
    Menu2_3.tag=9;
    Menu2_3.color=self.appManager.green;
    Menu2_3.HighlightedColor=self.appManager.greenHighlighted;
    Menu2_3.strColor=@"green";
    Menu2_3.icon=[UIImage imageNamed:@"MyApealsIcon"];
    Menu2_3.link=@"";
    Menu2_3.listMenu=nil;
    
    Menu*Menu2=[[Menu alloc]init];
    Menu2.ID=2;
    Menu2.menuID=0;
    Menu2.name=@"פניות למוקד";
    Menu2.active=3;
    Menu2.tag=0;
    Menu2.color=self.appManager.green;
    Menu2.HighlightedColor=self.appManager.greenHighlighted;
    Menu2.strColor=@"blue";
    Menu2.icon=[UIImage imageNamed:@"ApealsIcon.png"];
    Menu2.link=@"";
    Menu2.listMenu=[NSMutableArray arrayWithObjects:Menu2_1,Menu2_2,Menu2_3, nil ];
    
    Menu*Menu3=[[Menu alloc]init];
    Menu3.ID=3;
    Menu3.menuID=0;
    Menu3.name=@"תשלומים";
    Menu3.active=2;
    Menu3.tag=0;
    Menu3.color=self.appManager.orange;
    Menu3.HighlightedColor=self.appManager.orangeHighlighted;
    Menu3.strColor=@"green";
    Menu3.icon=[UIImage imageNamed:@"iconPay"];
    Menu3.link=@"https://www.citypay.co.il/Openning.asp?Lang=1&Bank=&CityName=";
    Menu3.listMenu=nil;
    
    Menu*Menu4=[[Menu alloc]init];
    Menu4.ID=4;
    Menu4.menuID=0;
    Menu4.name=@"הודעות עבורך";
    Menu4.active=1;
    Menu4.tag=3;
    Menu4.color=self.appManager.purple;
    Menu4.HighlightedColor=self.appManager.purpleHighlighted;
    Menu4.strColor=@"purple";
    Menu4.icon=[UIImage imageNamed:@"MassegsIcon"];
    Menu4.link=@"";
    Menu4.listMenu=nil;
    
    Menu*Menu5=[[Menu alloc]init];
    Menu5.ID=5;
    Menu5.menuID=0;
    Menu5.name=@"מפת העיר";
    Menu5.active=1;
    Menu5.tag=7;
    Menu5.color=self.appManager.pink;
    Menu5.HighlightedColor=self.appManager.pinkHighlighted;
    Menu5.strColor=@"pink";
    Menu5.icon=[UIImage imageNamed:@"iconMap"];
    Menu5.link=@"";
    Menu5.listMenu=nil;
    
    Menu*Menu6=[[Menu alloc]init];
    Menu6.ID=6;
    Menu6.menuID=0;
    Menu6.name=@"סקרים";
    Menu6.active=1;
    Menu6.tag=13;
    Menu6.color=[UIColor redColor];
    Menu6.HighlightedColor=self.appManager.redHighlighted;
    Menu6.strColor=@"red";
    Menu6.icon=[UIImage imageNamed:@"iconSurvey.png"];
    Menu6.link=@"";
    Menu6.listMenu=nil;
    self.appDelegate.menu=[[NSArray alloc]initWithObjects:Menu1,Menu2,Menu3,Menu4,Menu5,Menu6 ,nil];
}

-(void)setMenu9
{
    Menu*Menu1_1=[[Menu alloc]init];
    Menu1_1.ID=7;
    Menu1_1.menuID=1;
    Menu1_1.name=@"אירועי תרבות";
    Menu1_1.active=2;
    Menu1_1.tag=0;
    Menu1_1.color=self.appManager.lightBlue;
    Menu1_1.HighlightedColor=self.appManager.lightBlueHighlighted;
    Menu1_1.strColor=@"blue";
    Menu1_1.icon=[UIImage imageNamed:@"EventIcon.png"];
    Menu1_1.link=@"http://wws.weizmann.ac.il/ulamot/board";
    Menu1_1.listMenu=nil;
    
    Menu*Menu1_2=[[Menu alloc]init];
    Menu1_2.ID=11;
    Menu1_2.menuID=1;
    Menu1_2.name=@"טלפונים חיוניים";
    Menu1_2.active=1;
    Menu1_2.tag=6;
    Menu1_2.color=self.appManager.lightBlue2;
    Menu1_2.HighlightedColor=self.appManager.lightBlueHighlighted2;
    Menu1_2.strColor=@"blue";
    Menu1_2.icon=[UIImage imageNamed:@"phonIcon"];
    Menu1_2.link=@"";
    Menu1_2.listMenu=nil;
    
    Menu*Menu1_3=[[Menu alloc]init];
    Menu1_3.ID=12;
    Menu1_3.menuID=1;
    Menu1_3.name=@"פייסבוק מרכז שירות 9106";
    Menu1_3.active=2;
    Menu1_3.tag=0;
    Menu1_3.color=self.appManager.lightBlue;
    Menu1_3.HighlightedColor=self.appManager.lightBlueHighlighted;
    Menu1_3.strColor=@"blue";
    Menu1_3.icon=[UIImage imageNamed:@"FacebookIcon.png"];
    Menu1_3.link=@"https://www.facebook.com/pages/%D7%9E%D7%9B%D7%95%D7%9F-%D7%95%D7%99%D7%A6%D7%9E%D7%9F-%D7%9C%D7%9E%D7%93%D7%A2/203388273008016";
    Menu1_3.listMenu=nil;
    
    Menu*Menu1_4=[[Menu alloc]init];
    Menu1_4.ID=14;
    Menu1_4.menuID=2;
    Menu1_4.name=@"אודות המכון";
    Menu1_4.active=1;
    Menu1_4.tag=11;
    Menu1_4.color=self.appManager.lightBlue2;
    Menu1_4.HighlightedColor=self.appManager.lightBlueHighlighted;
    Menu1_4.strColor=@"blue";
    Menu1_4.icon=[UIImage imageNamed:@"AboutIcon"];
    Menu1_4.link=@"";
    Menu1_4.listMenu=nil;
    
    Menu*Menu1_5=[[Menu alloc]init];
    Menu1_5.ID=16;
    Menu1_5.menuID=1;
    Menu1_5.name=@"אתר אגף התיפעול";
    Menu1_5.active=2;
    Menu1_5.tag=0;
    Menu1_5.color=self.appManager.lightBlue;
    Menu1_5.HighlightedColor=self.appManager.lightBlueHighlighted;
    Menu1_5.strColor=@"blue";
    Menu1_5.icon=[UIImage imageNamed:@"CitySideIcon"];
    Menu1_5.link=@"http://wws.weizmann.ac.il/operations/";
    Menu1_5.listMenu=nil;
    
    Menu*Menu1_6=[[Menu alloc]init];
    Menu1_6.ID=13;
    Menu1_6.menuID=1;
    Menu1_6.name=@"חלוציות מתמשכת";
    Menu1_6.active=2;
    Menu1_6.tag=0;
    Menu1_6.color=self.appManager.lightBlue2;
    Menu1_6.HighlightedColor=self.appManager.lightBlueHighlighted2;
    Menu1_6.strColor=@"blue";
    Menu1_6.icon=[UIImage imageNamed:@"MayorIcon.png"];
    Menu1_6.link=@"http://www.weizmann.ac.il/pages/he/pioneering-progress";
    Menu1_6.listMenu=nil;
    
    Menu*Menu1_7=[[Menu alloc]init];
    Menu1_7.ID=17;
    Menu1_7.menuID=1;
    Menu1_7.name=@"צור קשר שירות 9106";
    Menu1_7.active=5;
    Menu1_7.tag=0;
    Menu1_7.color=self.appManager.lightBlue;
    Menu1_7.HighlightedColor=self.appManager.lightBlueHighlighted;
    Menu1_7.strColor=@"blue";
    Menu1_7.icon=[UIImage imageNamed:@"TelephonIcon.png"];
    Menu1_7.link=@"";
    Menu1_7.email=@"service.center@weizmann.ac.il";
    Menu1_7.emailSubject=@"יצירת קשר";
    Menu1_7.listMenu=nil;
    
    Menu*Menu1_8=[[Menu alloc]init];
    Menu1_8.ID=18;
    Menu1_8.menuID=1;
    Menu1_8.name=@"מסעדות המכון";
    Menu1_8.active=2;
    Menu1_8.tag=0;
    Menu1_8.color=self.appManager.lightBlue2;
    Menu1_8.HighlightedColor=self.appManager.lightBlueHighlighted2;
    Menu1_8.strColor=@"blue";
    Menu1_8.icon=[UIImage imageNamed:@"restorens"];
    Menu1_8.link=@"http://wws.weizmann.ac.il/food/he";
    Menu1_8.listMenu=nil;
    
    Menu*Menu1_9=[[Menu alloc]init];
    Menu1_9.ID=19;
    Menu1_9.menuID=1;
    Menu1_9.name=@"שעת חירום";
    Menu1_9.active=1;
    Menu1_9.tag=12;
    Menu1_9.color=self.appManager.lightBlue;
    Menu1_9.HighlightedColor=self.appManager.lightBlueHighlighted2;
    Menu1_9.strColor=@"blue";
    Menu1_9.icon=[UIImage imageNamed:@"EmergencyIcon.png"];
    Menu1_9.link=@"";
    Menu1_9.listMenu=nil;

    Menu*Menu1=[[Menu alloc]init];
    Menu1.ID=1;
    Menu1.menuID=0;
    Menu1.name=@"המכון שלי";
    Menu1.active=3;
    Menu1.tag=0;
    Menu1.color=self.appManager.lightBlue;
    Menu1.HighlightedColor=self.appManager.lightBlueHighlighted;
    Menu1.strColor=@"orange";
    Menu1.icon=[UIImage imageNamed:@"MyCityHallIcon"];
    Menu1.link=@"";
    Menu1.listMenu=[NSMutableArray arrayWithObjects:Menu1_1,Menu1_2,Menu1_3,Menu1_4,Menu1_5,Menu1_6,Menu1_7,Menu1_8,Menu1_9, nil ];
    
    Menu*Menu2_1=[[Menu alloc]init];
    Menu2_1.ID=8;
    Menu2_1.menuID=3;
    Menu2_1.name=@"חיוג למוקד";
    Menu2_1.active=4;
    Menu2_1.tag=0;
    Menu2_1.color=self.appManager.green;
    Menu2_1.HighlightedColor=self.appManager.greenHighlighted;
    Menu2_1.strColor=@"green";
    Menu2_1.icon=[UIImage imageNamed:@"phonIcon"];
    Menu2_1.link=@"";
    Menu2_1.phone=@"tel:106";
    Menu2_1.listMenu=nil;
    
    Menu*Menu2_2=[[Menu alloc]init];
    Menu2_2.ID=9;
    Menu2_2.menuID=3;
    Menu2_2.name=@"פניה חדשה";
    Menu2_2.active=1;
    Menu2_2.tag=8;
    Menu2_2.color=self.appManager.green2;
    Menu2_2.HighlightedColor=self.appManager.greenHighlighted2;
    Menu2_2.strColor=@"green";
    Menu2_2.icon=[UIImage imageNamed:@"ApealsIcon"];
    Menu2_2.link=@"";
    Menu2_2.listMenu=nil;
    
    Menu*Menu2_3=[[Menu alloc]init];
    Menu2_3.ID=10;
    Menu2_3.menuID=3;
    Menu2_3.name=@"הפניות למוקד שירות 9106";
    Menu2_3.active=1;
    Menu2_3.tag=9;
    Menu2_3.color=self.appManager.green;
    Menu2_3.HighlightedColor=self.appManager.greenHighlighted;
    Menu2_3.strColor=@"green";
    Menu2_3.icon=[UIImage imageNamed:@"MyApealsIcon"];
    Menu2_3.link=@"";
    Menu2_3.listMenu=nil;
    
    Menu*Menu2=[[Menu alloc]init];
    Menu2.ID=2;
    Menu2.menuID=0;
    Menu2.name=@"פניות למוקד";
    Menu2.active=3;
    Menu2.tag=0;
    Menu2.color=self.appManager.green;
    Menu2.HighlightedColor=self.appManager.lightBlueHighlighted;
    Menu2.strColor=@"blue";
    Menu2.icon=[UIImage imageNamed:@"ApealsIcon.png"];
    Menu2.link=@"";
    Menu2.listMenu=[NSMutableArray arrayWithObjects:Menu2_1,Menu2_2,Menu2_3, nil ];
    
    Menu*Menu3=[[Menu alloc]init];
    Menu3.ID=3;
    Menu3.menuID=0;
    Menu3.name=@"הצעות יעול";
    Menu3.active=5;
    Menu3.tag=0;
    Menu3.color=self.appManager.orange;
    Menu3.HighlightedColor=self.appManager.orangeHighlighted;
    Menu3.strColor=@"green";
    Menu3.icon=[UIImage imageNamed:@"iconRationalizationproposals.png"];
    Menu3.link=@"";
    Menu3.email=@"service.center@weizmann.ac.il";
    Menu3.emailSubject=@"הצעות יעול";
    Menu3.listMenu=nil;
    
    Menu*Menu4=[[Menu alloc]init];
    Menu4.ID=4;
    Menu4.menuID=0;
    Menu4.name=@"הודעות עבורך";
    Menu4.active=1;
    Menu4.tag=3;
    Menu4.color=self.appManager.purple;
    Menu4.HighlightedColor=self.appManager.purpleHighlighted;
    Menu4.strColor=@"purple";
    Menu4.icon=[UIImage imageNamed:@"MassegsIcon"];
    Menu4.link=@"";
    Menu4.listMenu=nil;
    
    Menu*Menu5=[[Menu alloc]init];
    Menu5.ID=5;
    Menu5.menuID=0;
    Menu5.name=@"מפה";
    Menu5.active=1;
    Menu5.tag=7;
    Menu5.color=self.appManager.pink;
    Menu5.HighlightedColor=self.appManager.pinkHighlighted;
    Menu5.strColor=@"pink";
    Menu5.icon=[UIImage imageNamed:@"iconMap"];
    Menu5.link=@"";
    Menu5.listMenu=nil;
    
    Menu*Menu6=[[Menu alloc]init];
    Menu6.ID=6;
    Menu6.menuID=0;
    Menu6.name=@"סקרים";
    Menu6.active=1;
    Menu6.tag=13;
    Menu6.color=[UIColor redColor];
    Menu6.HighlightedColor=self.appManager.redHighlighted;
    Menu6.strColor=@"red";
    Menu6.icon=[UIImage imageNamed:@"iconSurvey.png"];
    Menu6.link=@"";
    Menu6.listMenu=nil;
    self.appDelegate.menu=[[NSArray alloc]initWithObjects:Menu1,Menu2,Menu3,Menu4,Menu5,Menu6 ,nil];
}

-(void)setMenu10
{
    Menu*Menu1=[[Menu alloc]init];
    Menu1.ID=1;
    Menu1.menuID=0;
    Menu1.name=@"פייסבוק";
    Menu1.active=2;
    Menu1.tag=0;
    Menu1.color=self.appManager.orange;
    Menu1.HighlightedColor=self.appManager.orangeHighlighted;
    Menu1.strColor=@"orange";
    Menu1.icon=[UIImage imageNamed:@"FacebookIcon"];
    Menu1.link=@"https://www.facebook.com/pages/%D7%9E%D7%95%D7%A2%D7%A6%D7%94-%D7%9E%D7%A7%D7%95%D7%9E%D7%99%D7%AA-%D7%91%D7%A0%D7%99%D7%9E%D7%99%D7%A0%D7%94-%D7%92%D7%91%D7%A2%D7%AA-%D7%A2%D7%93%D7%94/288539514561594";
    Menu1.listMenu=nil;
    
    Menu*Menu2_1=[[Menu alloc]init];
    Menu2_1.ID=7;
    Menu2_1.menuID=2;
    Menu2_1.name=@"תשלומים";
    Menu2_1.active=2;
    Menu2_1.tag=0;
    Menu2_1.color=self.appManager.lightBlue;
    Menu2_1.HighlightedColor=self.appManager.lightBlueHighlighted;
    Menu2_1.strColor=@"blue";
    Menu2_1.icon=[UIImage imageNamed:@"iconPay"];
    Menu2_1.link=@"https://www.citypay.co.il/Openning.asp?Lang=1&Bank=&CityName=";
    Menu2_1.listMenu=nil;
    
    Menu*Menu2_2=[[Menu alloc]init];
    Menu2_2.ID=11;
    Menu2_2.menuID=2;
    Menu2_2.name=@"טלפונים";
    Menu2_2.active=1;
    Menu2_2.tag=6;
    Menu2_2.color=self.appManager.lightBlue2;
    Menu2_2.HighlightedColor=self.appManager.lightBlueHighlighted2;
    Menu2_2.strColor=@"blue";
    Menu2_2.icon=[UIImage imageNamed:@"phonIcon"];
    Menu2_2.link=@"";
    Menu2_2.listMenu=nil;
    
    Menu*Menu2_3=[[Menu alloc]init];
    Menu2_3.ID=12;
    Menu2_3.menuID=2;
    Menu2_3.name=@"תיירות עסקים ואמנים";
    Menu2_3.active=1;
    Menu2_3.tag=5;
    Menu2_3.color=self.appManager.lightBlue;
    Menu2_3.HighlightedColor=self.appManager.lightBlueHighlighted;
    Menu2_3.strColor=@"blue";
    Menu2_3.icon=[UIImage imageNamed:@"iconMap"];
    Menu2_3.link=@"";
    Menu2_3.listMenu=nil;
    
    Menu*Menu2_4=[[Menu alloc]init];
    Menu2_4.ID=13;
    Menu2_4.menuID=2;
    Menu2_4.name=@"דבר ראש המועצה";
    Menu2_4.active=1;
    Menu2_4.tag=10;
    Menu2_4.color=self.appManager.lightBlue2;
    Menu2_4.HighlightedColor=self.appManager.lightBlueHighlighted2;
    Menu2_4.strColor=@"blue";
    Menu2_4.icon=[UIImage imageNamed:@"MayorIcon"];
    Menu2_4.link=@"";
    Menu2_4.listMenu=nil;
    
    Menu*Menu2_5=[[Menu alloc]init];
    Menu2_5.ID=14;
    Menu2_5.menuID=2;
    Menu2_5.name=@"אודות הישוב";
    Menu2_5.active=1;
    Menu2_5.tag=11;
    Menu2_5.color=self.appManager.lightBlue;
    Menu2_5.HighlightedColor=self.appManager.lightBlueHighlighted;
    Menu2_5.strColor=@"blue";
    Menu2_5.icon=[UIImage imageNamed:@"AboutIcon"];
    Menu2_5.link=@"";
    Menu2_5.listMenu=nil;
    
    Menu*Menu2_6=[[Menu alloc]init];
    Menu2_6.ID=15;
    Menu2_6.menuID=2;
    Menu2_6.name=@"חירום";
    Menu2_6.active=1;
    Menu2_6.tag=12;
    Menu2_6.color=self.appManager.lightBlue2;
    Menu2_6.HighlightedColor=self.appManager.lightBlueHighlighted2;
    Menu2_6.strColor=@"blue";
    Menu2_6.icon=[UIImage imageNamed:@"EmergencyIcon"];
    Menu2_6.link=@"";
    Menu2_6.listMenu=nil;
    
    Menu*Menu2_7=[[Menu alloc]init];
    Menu2_7.ID=16;
    Menu2_7.menuID=2;
    Menu2_7.name=@"אתר המתנ״ס";
    Menu2_7.active=2;
    Menu2_7.tag=0;
    Menu2_7.color=self.appManager.lightBlue;
    Menu2_7.HighlightedColor=self.appManager.lightBlueHighlighted;
    Menu2_7.strColor=@"blue";
    Menu2_7.icon=[UIImage imageNamed:@"CitySideIcon"];
    Menu2_7.link=@"http://matnasbg.org.il/";
    Menu2_7.listMenu=nil;
    
    Menu*Menu2_8=[[Menu alloc]init];
    Menu2_8.ID=17;
    Menu2_8.menuID=2;
    Menu2_8.name=@"סקרים";
    Menu2_8.active=1;
    Menu2_8.tag=13;
    Menu2_8.color=self.appManager.lightBlue2;
    Menu2_8.HighlightedColor=self.appManager.lightBlueHighlighted2;
    Menu2_8.strColor=@"blue";
    Menu2_8.icon=[UIImage imageNamed:@"iconSurvey"];
    Menu2_8.link=@"";
    Menu2_8.listMenu=nil;
    
    Menu*Menu2=[[Menu alloc]init];
    Menu2.ID=2;
    Menu2.menuID=0;
    Menu2.name=@"המועצה שלי";
    Menu2.active=3;
    Menu2.tag=0;
    Menu2.color=self.appManager.lightBlue;
    Menu2.HighlightedColor=self.appManager.lightBlueHighlighted;
    Menu2.strColor=@"blue";
    Menu2.icon=[UIImage imageNamed:@"MyCityHallIcon"];
    Menu2.link=@"";
    Menu2.listMenu=[NSMutableArray arrayWithObjects:Menu2_1,Menu2_2,Menu2_3,Menu2_4,Menu2_5,Menu2_6,Menu2_7,Menu2_8, nil ];
    
    Menu*Menu3_1=[[Menu alloc]init];
    Menu3_1.ID=8;
    Menu3_1.menuID=3;
    Menu3_1.name=@"חיוג למוקד";
    Menu3_1.active=4;
    Menu3_1.tag=0;
    Menu3_1.color=self.appManager.green;
    Menu3_1.HighlightedColor=self.appManager.greenHighlighted;
    Menu3_1.strColor=@"green";
    Menu3_1.icon=[UIImage imageNamed:@"phonIcon"];
    Menu3_1.link=@"";
    Menu3_1.phone=@"tel:106";
    Menu3_1.listMenu=nil;
    
    Menu*Menu3_2=[[Menu alloc]init];
    Menu3_2.ID=9;
    Menu3_2.menuID=3;
    Menu3_2.name=@"פניה חדשה";
    Menu3_2.active=1;
    Menu3_2.tag=8;
    Menu3_2.color=self.appManager.green2;
    Menu3_2.HighlightedColor=self.appManager.greenHighlighted2;
    Menu3_2.strColor=@"green";
    Menu3_2.icon=[UIImage imageNamed:@"ApealsIcon"];
    Menu3_2.link=@"";
    Menu3_2.listMenu=nil;
    
    Menu*Menu3_3=[[Menu alloc]init];
    Menu3_3.ID=10;
    Menu3_3.menuID=3;
    Menu3_3.name=@"הפניות שלי";
    Menu3_3.active=1;
    Menu3_3.tag=9;
    Menu3_3.color=self.appManager.green;
    Menu3_3.HighlightedColor=self.appManager.greenHighlighted;
    Menu3_3.strColor=@"green";
    Menu3_3.icon=[UIImage imageNamed:@"MyApealsIcon"];
    Menu3_3.link=@"";
    Menu3_3.listMenu=nil;
    
    Menu*Menu3=[[Menu alloc]init];
    Menu3.ID=3;
    Menu3.menuID=0;
    Menu3.name=@"פניות למוקד";
    Menu3.active=3;
    Menu3.tag=0;
    Menu3.color=self.appManager.green;
    Menu3.HighlightedColor=self.appManager.greenHighlighted;
    Menu3.strColor=@"green";
    Menu3.icon=[UIImage imageNamed:@"ApealsIcon"];
    Menu3.link=@"";
    Menu3.listMenu=[NSMutableArray arrayWithObjects:Menu3_1,Menu3_2,Menu3_3,nil];
    
    Menu*Menu4=[[Menu alloc]init];
    Menu4.ID=4;
    Menu4.menuID=0;
    Menu4.name=@"הודעות עבורך";
    Menu4.active=1;
    Menu4.tag=3;
    Menu4.color=self.appManager.purple;
    Menu4.HighlightedColor=self.appManager.purpleHighlighted;
    Menu4.strColor=@"purple";
    Menu4.icon=[UIImage imageNamed:@"MassegsIcon"];
    Menu4.link=@"";
    Menu4.listMenu=nil;
    
    Menu*Menu5=[[Menu alloc]init];
    Menu5.ID=5;
    Menu5.menuID=0;
    Menu5.name=@"מפה";
    Menu5.active=1;
    Menu5.tag=7;
    Menu5.color=self.appManager.pink;
    Menu5.HighlightedColor=self.appManager.pinkHighlighted;
    Menu5.strColor=@"pink";
    Menu5.icon=[UIImage imageNamed:@"iconMap"];
    Menu5.link=@"";
    Menu5.listMenu=nil;
    
    Menu*Menu6=[[Menu alloc]init];
    Menu6.ID=6;
    Menu6.menuID=0;
    Menu6.name=@"אירועים";
    Menu6.active=1;
    Menu6.tag=1;
    Menu6.color=[UIColor redColor];
    Menu6.HighlightedColor=self.appManager.redHighlighted;
    Menu6.strColor=@"red";
    Menu6.icon=[UIImage imageNamed:@"EventIcon"];
    Menu6.link=@"";
    Menu6.listMenu=nil;
    self.appDelegate.menu=[[NSArray alloc]initWithObjects:Menu1,Menu2,Menu3,Menu4,Menu5,Menu6 ,nil];
}
-(void)setMenu1
{
    Menu*Menu1_1=[[Menu alloc]init];
    Menu1_1.ID=7;
    Menu1_1.menuID=1;
    Menu1_1.name=@"דבר ראש העיר";
    Menu1_1.active=1;
    Menu1_1.tag=10;
    Menu1_1.color=self.appManager.lightBlue;
    Menu1_1.HighlightedColor=self.appManager.lightBlueHighlighted;
    Menu1_1.strColor=@"blue";
    Menu1_1.icon=[UIImage imageNamed:@"MayorIcon.png"];
    Menu1_1.link=@"";
    Menu1_1.listMenu=nil;
    
    Menu*Menu1_2=[[Menu alloc]init];
    Menu1_2.ID=11;
    Menu1_2.menuID=1;
    Menu1_2.name=@"מידע על העיר";
    Menu1_2.active=1;
    Menu1_2.tag=11;
    Menu1_2.color=self.appManager.lightBlue2;
    Menu1_2.HighlightedColor=self.appManager.lightBlueHighlighted2;
    Menu1_2.strColor=@"blue";
    Menu1_2.icon=[UIImage imageNamed:@"AboutIcon.png"];
    Menu1_2.link=@"";
    Menu1_2.listMenu=nil;
    
    Menu*Menu1_3=[[Menu alloc]init];
    Menu1_3.ID=12;
    Menu1_3.menuID=1;
    Menu1_3.name=@"מסלולי טיולים";
    Menu1_3.active=1;
    Menu1_3.tag=4;
    Menu1_3.color=self.appManager.lightBlue;
    Menu1_3.HighlightedColor=self.appManager.lightBlueHighlighted;
    Menu1_3.strColor=@"blue";
    Menu1_3.icon=[UIImage imageNamed:@"מסלולי טיולים.png"];
    Menu1_3.link=@"";
    Menu1_3.listMenu=nil;
    
    Menu*Menu1_4=[[Menu alloc]init];
    Menu1_4.ID=14;
    Menu1_4.menuID=2;
    Menu1_4.name=@"אתרי מורשת";
    Menu1_4.active=1;
    Menu1_4.tag=5;
    Menu1_4.color=self.appManager.lightBlue2;
    Menu1_4.HighlightedColor=self.appManager.lightBlueHighlighted;
    Menu1_4.strColor=@"blue";
    Menu1_4.icon=[UIImage imageNamed:@"אתרי מורשת"];
    Menu1_4.link=@"";
    Menu1_4.listMenu=nil;
    
    Menu*Menu1_5=[[Menu alloc]init];
    Menu1_5.ID=16;
    Menu1_5.menuID=1;
    Menu1_5.name=@"מפה";
    Menu1_5.active=1;
    Menu1_5.tag=16;
    Menu1_5.color=self.appManager.lightBlue;
    Menu1_5.HighlightedColor=self.appManager.lightBlueHighlighted;
    Menu1_5.strColor=@"blue";
    Menu1_5.icon=[UIImage imageNamed:@"iconMap"];
    Menu1_5.link=@"";
    Menu1_5.listMenu=nil;
    
    Menu*Menu1=[[Menu alloc]init];
    Menu1.ID=1;
    Menu1.menuID=0;
    Menu1.name=@"אודות חדרה";
    Menu1.active=3;
    Menu1.tag=0;
    Menu1.color=self.appManager.lightBlue;
    Menu1.HighlightedColor=self.appManager.lightBlueHighlighted;
    Menu1.strColor=@"orange";
    Menu1.icon=[UIImage imageNamed:@"MyCityHallIcon"];
    Menu1.link=@"";
    Menu1.listMenu=[NSMutableArray arrayWithObjects:Menu1_1,Menu1_2,Menu1_3,Menu1_4,Menu1_5, nil ];
    
    Menu*Menu2_1=[[Menu alloc]init];
    Menu2_1.ID=8;
    Menu2_1.menuID=3;
    Menu2_1.name=@"חיוג למוקד";
    Menu2_1.active=4;
    Menu2_1.tag=0;
    Menu2_1.color=self.appManager.green;
    Menu2_1.HighlightedColor=self.appManager.greenHighlighted;
    Menu2_1.strColor=@"green";
    Menu2_1.icon=[UIImage imageNamed:@"phonIcon"];
    Menu2_1.link=@"";
    Menu2_1.phone=@"tel:04-6303325";
    Menu2_1.listMenu=nil;
    
    Menu*Menu2_2=[[Menu alloc]init];
    Menu2_2.ID=9;
    Menu2_2.menuID=3;
    Menu2_2.name=@"פניה חדשה";
    Menu2_2.active=1;
    Menu2_2.tag=8;
    Menu2_2.color=self.appManager.green2;
    Menu2_2.HighlightedColor=self.appManager.greenHighlighted2;
    Menu2_2.strColor=@"green";
    Menu2_2.icon=[UIImage imageNamed:@"ApealsIcon"];
    Menu2_2.link=@"";
    Menu2_2.listMenu=nil;
    
    Menu*Menu2_3=[[Menu alloc]init];
    Menu2_3.ID=10;
    Menu2_3.menuID=3;
    Menu2_3.name=@"הפניות שלי";
    Menu2_3.active=1;
    Menu2_3.tag=9;
    Menu2_3.color=self.appManager.green;
    Menu2_3.HighlightedColor=self.appManager.greenHighlighted;
    Menu2_3.strColor=@"green";
    Menu2_3.icon=[UIImage imageNamed:@"MyApealsIcon"];
    Menu2_3.link=@"";
    Menu2_3.listMenu=nil;
    
    Menu*Menu2=[[Menu alloc]init];
    Menu2.ID=2;
    Menu2.menuID=0;
    Menu2.name=@"מוקד 106";
    Menu2.active=3;
    Menu2.tag=0;
    Menu2.color=self.appManager.green;
    Menu2.HighlightedColor=self.appManager.greenHighlighted;
    Menu2.strColor=@"blue";
    Menu2.icon=[UIImage imageNamed:@"ApealsIcon.png"];
    Menu2.link=@"";
    Menu2.listMenu=[NSMutableArray arrayWithObjects:Menu2_1,Menu2_2,Menu2_3, nil ];
    
    Menu*Menu3_1=[[Menu alloc]init];
    Menu3_1.ID=16;
    Menu3_1.menuID=1;
    Menu3_1.name=@"תיק תושב";
    Menu3_1.active=2;
    Menu3_1.tag=0;
    Menu3_1.color=self.appManager.orange;
    Menu3_1.HighlightedColor=self.appManager.orangeHighlighted;
    Menu3_1.strColor=@"orange";
    Menu3_1.icon=[UIImage imageNamed:@"iconTikToshav.png"];
    Menu3_1.link=@"https://toshavnet.malam.com/tiktoshav/MuniApps.aspx?MuniCode=6500";
    Menu3_1.listMenu=nil;
    
    Menu*Menu3_2=[[Menu alloc]init];
    Menu3_2.ID=17;
    Menu3_2.menuID=1;
    Menu3_2.name=@"תשלום ארנונה";
    Menu3_2.active=2;
    Menu3_2.tag=0;
    Menu3_2.color=self.appManager.orange2;
    Menu3_2.HighlightedColor=self.appManager.orangeHighlighted2;
    Menu3_2.strColor=@"orange";
    Menu3_2.icon=[UIImage imageNamed:@"תשלום ארנונה.png"];
         //Menu3_2.link=@"https://www.mybills.co.il";

//   Menu3_2.link=@"https://www.mybills.co.il/Payments/475/חדרה_ארנונה/שמירה.html";
     Menu3_2.link=@"https://www.mybills.co.il/Payments/475/%D7%97%D7%93%D7%A8%D7%94_%D7%90%D7%A8%D7%A0%D7%95%D7%A0%D7%94/";

    Menu3_2.listMenu=nil;
    
    Menu*Menu3_3=[[Menu alloc]init];
    Menu3_3.ID=18;
    Menu3_3.menuID=1;
    Menu3_3.name=@"תשלום דוחות חניה";
    Menu3_3.active=2;
    Menu3_3.tag=1;
    Menu3_3.color=self.appManager.orange;
    Menu3_3.HighlightedColor=self.appManager.orangeHighlighted;
    Menu3_3.strColor=@"orange";
    Menu3_3.icon=[UIImage imageNamed:@"תשלום דוחות חניה.png"];
        // Menu3_3.link=@"https://www.mybills.co.il";
    Menu3_3.link=@"https://www.doh.co.il/Default.aspx?ReportType=1&Rashut=265000";
    Menu3_3.listMenu=nil;
    
    Menu*Menu3_4=[[Menu alloc]init];
    Menu3_4.ID=19;
    Menu3_4.menuID=1;
    Menu3_4.name=@"תשלום דוחות פיקוח כללי";
    Menu3_4.active=2;
    Menu3_4.tag=1;
    Menu3_4.color=self.appManager.orange2;
    Menu3_4.HighlightedColor=self.appManager.orangeHighlighted2;
    Menu3_4.strColor=@"orange";
    Menu3_4.icon=[UIImage imageNamed:@"תשלום דוחות פיקוח כללי.png"];
    Menu3_4.link=@"https://www.doh.co.il/Default.aspx?ReportType=2&Rashut=265000";
    Menu3_4.listMenu=nil;
    
    Menu*Menu3_5=[[Menu alloc]init];
    Menu3_5.ID=20;
    Menu3_5.menuID=1;
    Menu3_5.name=@"מי חדרה";
    Menu3_5.active=2;
    Menu3_5.tag=0;
    Menu3_5.color=self.appManager.orange;
    Menu3_5.HighlightedColor=self.appManager.orangeHighlighted;
    Menu3_5.strColor=@"orange";
    Menu3_5.icon=[UIImage imageNamed:@"מי חדרה.png"];
   Menu3_5.link=@"https://www.pay24.co.il/pay24/Page_SearchPay.aspx";
    Menu3_5.listMenu=nil;
    
    Menu*Menu3_6=[[Menu alloc]init];
    Menu3_6.ID=21;
    Menu3_6.menuID=1;
    Menu3_6.name=@"רישום למערכת החינוך";
    Menu3_6.active=2;
    Menu3_6.tag=0;
    Menu3_6.color=self.appManager.orange2;
    Menu3_6.HighlightedColor=self.appManager.orangeHighlighted2;
    Menu3_6.strColor=@"orange";
    Menu3_6.icon=[UIImage imageNamed: @"רישום למערכת החינוך.png"];
    Menu3_6.link=@"https://toshavnet.malam.com/tiktoshav/";
    Menu3_6.listMenu=nil;
    
    Menu*Menu3_7=[[Menu alloc]init];
    Menu3_7.ID=22;
    Menu3_7.menuID=1;
    Menu3_7.name=@"תשלום אגרת שילוט";
    Menu3_7.active=2;
    Menu3_7.tag=0;
    Menu3_7.color=self.appManager.orange;
    Menu3_7.HighlightedColor=self.appManager.orangeHighlighted;
    Menu3_7.strColor=@"orange";
    Menu3_7.icon=[UIImage imageNamed:@"תשלום אגרת שילוט.png"];
    Menu3_7.link=@"https://www.mybills.co.il/Payments/477/";
    Menu3_7.listMenu=nil;
    
    Menu*Menu3=[[Menu alloc]init];
    Menu3.ID=3;
    Menu3.menuID=0;
    Menu3.name=@"תשלומים";
    Menu3.active=3;
    Menu3.tag=0;
    Menu3.color=self.appManager.orange2;
    Menu3.HighlightedColor=self.appManager.orangeHighlighted2;
    Menu3.strColor=@"orange";
    Menu3.icon=[UIImage imageNamed:@"iconPay.png"];
    Menu3.link=@"";
    Menu3.listMenu=[NSMutableArray arrayWithObjects:Menu3_1,Menu3_2,Menu3_3,Menu3_4,Menu3_5,Menu3_6,Menu3_7,nil];
    
    Menu*Menu4_1=[[Menu alloc]init];
    Menu4_1.ID=23;
    Menu4_1.menuID=4;
    Menu4_1.name=@"חדשות ועדכונים";
    Menu4_1.active=1;
    Menu4_1.tag=2;
    Menu4_1.color=self.appManager.purple;
    Menu4_1.HighlightedColor=self.appManager.purpleHighlighted;
    Menu4_1.strColor=@"purple";
    Menu4_1.icon=[UIImage imageNamed:@"NewsIcon"];
    Menu4_1.link=@"";
    Menu4_1.listMenu=nil;
    
    Menu*Menu4_2=[[Menu alloc]init];
    Menu4_2.ID=24;
    Menu4_2.menuID=4;
    Menu4_2.name=@"חופש המידע";
    Menu4_2.active=1;
    Menu4_2.tag=20;
    Menu4_2.color=self.appManager.purple2;
    Menu4_2.HighlightedColor=self.appManager.purpleHighlighted2;
    Menu4_2.strColor=@"purple";
    Menu4_2.icon=[UIImage imageNamed:@"חפש המידע"];
    Menu4_2.link=@"";
    Menu4_2.listMenu=nil;
    
    Menu*Menu4_3=[[Menu alloc]init];
    Menu4_3.ID=25;
    Menu4_3.menuID=4;
    Menu4_3.name=@"שעת חירום";
    Menu4_3.active=1;
    Menu4_3.tag=21;
    Menu4_3.color=self.appManager.purple;
    Menu4_3.HighlightedColor=self.appManager.purpleHighlighted;
    Menu4_3.strColor=@"purple";
    Menu4_3.icon=[UIImage imageNamed:@"EmergencyIcon"];
    Menu4_3.link=@"";
    Menu4_3.listMenu=nil;
    
    Menu*Menu4_4=[[Menu alloc]init];
    Menu4_4.ID=26;
    Menu4_4.menuID=4;
    Menu4_4.name=@"טלפונים בעירייה";
    Menu4_4.active=1;
    Menu4_4.tag=6;
    Menu4_4.color=self.appManager.purple2;
    Menu4_4.HighlightedColor=self.appManager.purpleHighlighted2;
    Menu4_4.strColor=@"Purple";
    Menu4_4.icon=[UIImage imageNamed:@"phonIcon"];
    Menu4_4.link=@"";
    Menu4_4.listMenu=nil;
    
    Menu*Menu4_5=[[Menu alloc]init];
    Menu4_5.ID=27;
    Menu4_5.menuID=4;
    Menu4_5.name=@"גופי סמך";
    Menu4_5.active=1;
    Menu4_5.tag=14;
    Menu4_5.color=self.appManager.purple;
    Menu4_5.HighlightedColor=self.appManager.purpleHighlighted;
    Menu4_5.strColor=@"purple";
    Menu4_5.icon=[UIImage imageNamed:@"גופי סמך.png"];
    Menu4_5.link=@"";
    Menu4_5.listMenu=nil;
    
    Menu*Menu4_6=[[Menu alloc]init];
    Menu4_6.ID=28;
    Menu4_6.menuID=4;
    Menu4_6.name=@"אתר העירייה";
    Menu4_6.active=2;
    Menu4_6.tag=0;
    Menu4_6.color=self.appManager.purple2;
    Menu4_6.HighlightedColor=self.appManager.purpleHighlighted2;
    Menu4_6.strColor=@"purple";
    Menu4_6.icon=[UIImage imageNamed:@"CitySideIcon"];
    Menu4_6.link=@"http://www.hadera.muni.il/";
    Menu4_6.listMenu=nil;
    
    Menu*Menu4_7=[[Menu alloc]init];
    Menu4_7.ID=29;
    Menu4_7.menuID=4;
    Menu4_7.name=@"פייסבוק";
    Menu4_7.active=2;
    Menu4_7.tag=0;
    Menu4_7.color=self.appManager.purple;
    Menu4_7.HighlightedColor=self.appManager.purpleHighlighted;
    Menu4_7.strColor=@"purple";
    Menu4_7.icon=[UIImage imageNamed:@"FacebookIcon"];
    Menu4_7.link=@"https://www.facebook.com/hadera.muni";
    Menu4_7.listMenu=nil;
    
    Menu*Menu4_8=[[Menu alloc]init];
    Menu4_8.ID=30;
    Menu4_8.menuID=4;
    Menu4_8.name=@"חוגים ופעילויות פנאי";
    Menu4_8.active=2;
    Menu4_8.tag=0;
    Menu4_8.color=self.appManager.purple2;
    Menu4_8.HighlightedColor=self.appManager.purpleHighlighted2;
    Menu4_8.strColor=@"purple";
    //Menu4_8.icon=[UIImage imageNamed:@"חוגים ופעילויות פנאי"];
    Menu4_8.icon=[UIImage imageNamed:@"0-עלה"];
    Menu4_8.link=@"http://www.pnay.org.il/ale%20final.pdf";
    Menu4_8.listMenu=nil;
   
    Menu*Menu4=[[Menu alloc]init];
    Menu4.ID=4;
    Menu4.menuID=0;
    Menu4.name=@"שירות ומידע";
    Menu4.active=3;
    Menu4.tag=0;
    Menu4.color=self.appManager.purple;
    Menu4.HighlightedColor=self.appManager.purpleHighlighted;
    Menu4.strColor=@"purple";
    Menu4.icon=[UIImage imageNamed:@"שירות ומידע"];
    Menu4.link=@"";
    Menu4.listMenu=[NSMutableArray arrayWithObjects:Menu4_1,Menu4_2,Menu4_3,Menu4_4,Menu4_5,Menu4_8,Menu4_6,Menu4_7 ,nil];
    
    Menu*Menu5=[[Menu alloc]init];
    Menu5.ID=5;
    Menu5.menuID=0;
    Menu5.name=@"מבלים בעיר";
    Menu5.active=1;
    Menu5.tag=15;
    Menu5.color=self.appManager.pink;
    Menu5.HighlightedColor=self.appManager.pinkHighlighted;
    Menu5.strColor=@"pink";
    Menu5.icon=[UIImage imageNamed:@"EventIcon"];
    Menu5.link=@"";
    Menu5.listMenu=nil;
    
    Menu*Menu6=[[Menu alloc]init];
    Menu6.ID=6;
    Menu6.menuID=0;
    Menu6.name=@"הודעות עבורך";
    Menu6.active=1;
    Menu6.tag=3;
    Menu6.color=[UIColor redColor];
    Menu6.HighlightedColor=self.appManager.redHighlighted2;
    Menu6.strColor=@"red";
    Menu6.icon=[UIImage imageNamed:@"MassegsIcon"];
    Menu6.link=@"";
    Menu6.listMenu=nil;
    
    //    Menu*Menu4_9=[[Menu alloc]init];
    //    Menu4_9.ID=31;
    //    Menu4_9.menuID=4;
    //    Menu4_9.name=@"הודעות עבורך";
    //    Menu4_9.active=1;
    //    Menu4_9.tag=3;
    //    Menu4_9.color=self.appManager.purple;
    //    Menu4_9.HighlightedColor=self.appManager.purpleHighlighted2;
    //    Menu4_9.strColor=@"purple";
    //    Menu4_9.icon=[UIImage imageNamed:@"MassegsIcon"];
    //    Menu4_9.link=@"";
    //    Menu4_9.listMenu=nil;

    self.appDelegate.menu=[[NSArray alloc]initWithObjects:Menu1,Menu2,Menu3,Menu4,Menu5,Menu6 ,nil];
 
}

-(void)setMenu18
{
    Menu*Menu1_1=[[Menu alloc]init];
    Menu1_1.ID=7;
    Menu1_1.menuID=1;
    Menu1_1.name=@"דבר ראש המועצה";
    Menu1_1.active=1;
    Menu1_1.tag=10;
    Menu1_1.color=self.appManager.lightBlue;
    Menu1_1.HighlightedColor=self.appManager.lightBlueHighlighted;
    Menu1_1.strColor=@"blue";
    Menu1_1.icon=[UIImage imageNamed:@"MayorIcon.png"];
    Menu1_1.link=@"";
    Menu1_1.listMenu=nil;
    
    Menu*Menu1_2=[[Menu alloc]init];
    Menu1_2.ID=11;
    Menu1_2.menuID=1;
    Menu1_2.name=@"מידע על המועצה";
    Menu1_2.active=1;
    Menu1_2.tag=11;
    Menu1_2.color=self.appManager.lightBlue2;
    Menu1_2.HighlightedColor=self.appManager.lightBlueHighlighted2;
    Menu1_2.strColor=@"blue";
    Menu1_2.icon=[UIImage imageNamed:@"AboutIcon.png"];
    Menu1_2.link=@"";
    Menu1_2.listMenu=nil;
    
    Menu*Menu1_3=[[Menu alloc]init];
    Menu1_3.ID=16;
    Menu1_3.menuID=1;
    Menu1_3.name=@"מפה";
    Menu1_3.active=1;
    Menu1_3.tag=16;
    Menu1_3.color=self.appManager.lightBlue;
    Menu1_3.HighlightedColor=self.appManager.lightBlueHighlighted;
    Menu1_3.strColor=@"blue";
    Menu1_3.icon=[UIImage imageNamed:@"iconMap"];
    Menu1_3.link=@"";
    Menu1_3.listMenu=nil;
    
    Menu*Menu1=[[Menu alloc]init];
    Menu1.ID=1;
    Menu1.menuID=0;
    Menu1.name=@"אודות יסוד המעלה";
    Menu1.active=3;
    Menu1.tag=0;
    Menu1.color=self.appManager.lightBlue;
    Menu1.HighlightedColor=self.appManager.lightBlueHighlighted;
    Menu1.strColor=@"orange";
    Menu1.icon=[UIImage imageNamed:@"MyCityHallIcon"];
    Menu1.link=@"";
    Menu1.listMenu = [NSMutableArray arrayWithObjects:Menu1_1,Menu1_2,Menu1_3, nil];
    
    Menu*Menu2_1=[[Menu alloc]init];
    Menu2_1.ID=8;
    Menu2_1.menuID=3;
    Menu2_1.name=@"חיוג למוקד";
    Menu2_1.active=4;
    Menu2_1.tag=0;
    Menu2_1.color=self.appManager.green;
    Menu2_1.HighlightedColor = self.appManager.greenHighlighted;
    Menu2_1.strColor=@"green";
    Menu2_1.icon=[UIImage imageNamed:@"phonIcon"];
    Menu2_1.link=@"";
    Menu2_1.phone=@"tel:04-6937511";
    Menu2_1.listMenu=nil;
    
    Menu*Menu2_2=[[Menu alloc]init];
    Menu2_2.ID=9;
    Menu2_2.menuID=3;
    Menu2_2.name=@"פניה חדשה";
    Menu2_2.active=1;
    Menu2_2.tag=8;
    Menu2_2.color=self.appManager.green2;
    Menu2_2.HighlightedColor=self.appManager.greenHighlighted2;
    Menu2_2.strColor=@"green";
    Menu2_2.icon=[UIImage imageNamed:@"ApealsIcon"];
    Menu2_2.link=@"";
    Menu2_2.listMenu=nil;
    
    Menu*Menu2_3=[[Menu alloc]init];
    Menu2_3.ID=10;
    Menu2_3.menuID=3;
    Menu2_3.name=@"הפניות שלי";
    Menu2_3.active=1;
    Menu2_3.tag=9;
    Menu2_3.color=self.appManager.green;
    Menu2_3.HighlightedColor=self.appManager.greenHighlighted;
    Menu2_3.strColor=@"green";
    Menu2_3.icon=[UIImage imageNamed:@"MyApealsIcon"];
    Menu2_3.link=@"";
    Menu2_3.listMenu=nil;
    
    Menu*Menu2=[[Menu alloc]init];
    Menu2.ID=2;
    Menu2.menuID=0;
    Menu2.name=@"מוקד 106";
    Menu2.active=3;
    Menu2.tag=0;
    Menu2.color=self.appManager.green;
    Menu2.HighlightedColor=self.appManager.greenHighlighted;
    Menu2.strColor=@"blue";
    Menu2.icon=[UIImage imageNamed:@"ApealsIcon.png"];
    Menu2.link=@"";
    Menu2.listMenu=[NSMutableArray arrayWithObjects:Menu2_1,Menu2_2,Menu2_3, nil ];
    
    Menu*Menu3_1=[[Menu alloc]init];
    Menu3_1.ID=23;
    Menu3_1.menuID=4;
    Menu3_1.name=@"מידע";
    Menu3_1.active=1;
    Menu3_1.tag=1;
    Menu3_1.color=self.appManager.orange;
    Menu3_1.HighlightedColor=self.appManager.orangeHighlighted;
    Menu3_1.strColor=@"orange";
    Menu3_1.icon=[UIImage imageNamed:@"חפש המידע.png"];
    Menu3_1.link=@"";
    Menu3_1.listMenu=nil;
    
    Menu*Menu3_2=[[Menu alloc]init];
    Menu3_2.ID=5;
    Menu3_2.menuID=0;
    Menu3_2.name=@"חוגים";
    Menu3_2.active=1;
    Menu3_2.tag=18;
    Menu3_2.color=self.appManager.orange2;
    Menu3_2.HighlightedColor=self.appManager.orangeHighlighted2;
    Menu3_2.strColor=@"orange";
    Menu3_2.icon=[UIImage imageNamed:@"EventIcon"];
    Menu3_2.link=@"";
    Menu3_2.listMenu=nil;
    
    Menu*Menu3=[[Menu alloc]init];
    Menu3.ID=3;
    Menu3.menuID=0;
    Menu3.name=@"חינוך ותרבות במושבה";
    Menu3.active=3;
    Menu3.tag=3;
    Menu3.color=self.appManager.orange;
    Menu3.HighlightedColor=self.appManager.orangeHighlighted;
    Menu3.strColor=@"red";
    Menu3.icon=[UIImage imageNamed:@"iconRationalizationproposals.png"];
    Menu3.link=@"";
    Menu3.listMenu=[NSMutableArray arrayWithObjects:Menu3_1,Menu3_2, nil ];
    
    Menu*Menu4_1=[[Menu alloc]init];
    Menu4_1.ID=23;
    Menu4_1.menuID=4;
    Menu4_1.name=@"חדשות ועדכונים";
    Menu4_1.active=1;
    Menu4_1.tag=2;
    Menu4_1.color=self.appManager.purple;
    Menu4_1.HighlightedColor=self.appManager.purpleHighlighted;
    Menu4_1.strColor=@"purple";
    Menu4_1.icon=[UIImage imageNamed:@"NewsIcon"];
    Menu4_1.link=@"";
    Menu4_1.listMenu=nil;
    
    Menu*Menu4_2=[[Menu alloc]init];
    Menu4_2.ID=24;
    Menu4_2.menuID=4;
    Menu4_2.name=@"חופש המידע";
    Menu4_2.active=2;
    Menu4_2.tag=999;
    Menu4_2.color=self.appManager.purple2;
    Menu4_2.HighlightedColor=self.appManager.purpleHighlighted2;
    Menu4_2.strColor=@"purple";
    Menu4_2.icon=[UIImage imageNamed:@"חפש המידע"];
    Menu4_2.link=@"";
    Menu4_2.listMenu=nil;
    
    Menu*Menu4_3=[[Menu alloc]init];
    Menu4_3.ID=25;
    Menu4_3.menuID=4;
    Menu4_3.name=@"שעת חירום";
    Menu4_3.active=1;
    Menu4_3.tag=21;
    Menu4_3.color=self.appManager.purple;
    Menu4_3.HighlightedColor=self.appManager.purpleHighlighted;
    Menu4_3.strColor=@"purple";
    Menu4_3.icon=[UIImage imageNamed:@"EmergencyIcon"];
    Menu4_3.link=@"";
    Menu4_3.listMenu=nil;
    
    Menu*Menu4_4=[[Menu alloc]init];
    Menu4_4.ID=26;
    Menu4_4.menuID=4;
    Menu4_4.name=@"טלפונים במועצה";
    Menu4_4.active=1;
    Menu4_4.tag=6;
    Menu4_4.color=self.appManager.purple2;
    Menu4_4.HighlightedColor=self.appManager.purpleHighlighted2;
    Menu4_4.strColor=@"Purple";
    Menu4_4.icon=[UIImage imageNamed:@"phonIcon"];
    Menu4_4.link=@"";
    Menu4_4.listMenu=nil;
    
    Menu*Menu4_5=[[Menu alloc]init];
    Menu4_5.ID=27;
    Menu4_5.menuID=4;
    Menu4_5.name=@"גופי סמך";
    Menu4_5.active=1;
    Menu4_5.tag=14;
    Menu4_5.color=self.appManager.purple;
    Menu4_5.HighlightedColor=self.appManager.purpleHighlighted;
    Menu4_5.strColor=@"purple";
    Menu4_5.icon=[UIImage imageNamed:@"גופי סמך.png"];
    Menu4_5.link=@"";
    Menu4_5.listMenu=nil;
    
    Menu*Menu4_6=[[Menu alloc]init];
    Menu4_6.ID=28;
    Menu4_6.menuID=4;
    Menu4_6.name=@"אתר המועצה";
    Menu4_6.active=2;
    Menu4_6.tag=0;
    Menu4_6.color=self.appManager.purple2;
    Menu4_6.HighlightedColor=self.appManager.purpleHighlighted2;
    Menu4_6.strColor=@"purple";
    Menu4_6.icon=[UIImage imageNamed:@"CitySideIcon"];
    Menu4_6.link=@"http://www.yesud-hamaala.muni.il";
    Menu4_6.listMenu=nil;
    
    Menu*Menu4_8=[[Menu alloc]init];
    Menu4_8.ID=29;
    Menu4_8.menuID=4;
    Menu4_8.name=@"פייסבוק";
    Menu4_8.active=2;
    Menu4_8.tag=0;
    Menu4_8.color=self.appManager.purple2;
    Menu4_8.HighlightedColor=self.appManager.purpleHighlighted2;
    Menu4_8.strColor=@"purple";
    Menu4_8.icon=[UIImage imageNamed:@"FacebookIcon"];
    Menu4_8.link=@"https://www.facebook.com/pages/%D7%99%D7%A1%D7%95%D7%93-%D7%94%D7%9E%D7%A2%D7%9C%D7%94-%D7%9E%D7%95%D7%A2%D7%A6%D7%94-%D7%9E%D7%A7%D7%95%D7%9E%D7%99%D7%AA/168397453318636?fref=ts";
    Menu4_8.listMenu=nil;
    
    Menu*Menu4_9=[[Menu alloc]init];
    Menu4_9.ID=30;
    Menu4_9.menuID=4;
    Menu4_9.name=@"דף לתושב";
    Menu4_9.active=2;
    Menu4_9.tag=0;
    Menu4_9.color=self.appManager.purple;
    Menu4_9.HighlightedColor=self.appManager.purpleHighlighted;
    Menu4_9.strColor=@"purple";
    Menu4_9.icon=[UIImage imageNamed:@"חוגים ופעילויות פנאי.png"];
    Menu4_9.link=@"";
    Menu4_9.listMenu=nil;
    
    
    
    Menu*Menu4_7=[[Menu alloc]init];
    Menu4_7.ID=31;
    Menu4_7.menuID=4;
    Menu4_7.name=@"תשלומים";
    Menu4_7.active=2;
    Menu4_7.tag=0;
    Menu4_7.color=self.appManager.purple;
    Menu4_7.HighlightedColor=self.appManager.purpleHighlighted;
    Menu4_7.strColor=@"purple";
    Menu4_7.icon=[UIImage imageNamed:@"iconPay.png"];
    Menu4_7.link=@"https://www.citypay.co.il/htmls_Heb/tofes_tashlum.asp?SId=9&LAMASID=800290";
    Menu4_7.listMenu=nil;

    
    Menu*Menu4=[[Menu alloc]init];
    Menu4.ID=4;
    Menu4.menuID=0;
    Menu4.name=@"שירות, מידע ותשלומים";
    Menu4.active=3;
    Menu4.tag=0;
    Menu4.color=self.appManager.purple;
    Menu4.HighlightedColor=self.appManager.purpleHighlighted;
    Menu4.strColor=@"purple";
    Menu4.icon=[UIImage imageNamed:@"שירות ומידע"];
    Menu4.link=@"";
    Menu4.listMenu=[NSMutableArray arrayWithObjects:Menu4_1,Menu4_2,Menu4_3,Menu4_4,Menu4_5,Menu4_6,Menu4_7,  Menu4_8,Menu4_9, nil ];
    
    Menu*Menu5_1=[[Menu alloc]init];
    Menu5_1.ID=12;
    Menu5_1.menuID=1;
    Menu5_1.name=@"מסלולי טיולים";
    Menu5_1.active=1;
    Menu5_1.tag=4;
    Menu5_1.color=self.appManager.pink;
    Menu5_1.HighlightedColor=self.appManager.pinkHighlighted;
    Menu5_1.strColor=@"pink";
    Menu5_1.icon=[UIImage imageNamed:@"מסלולי טיולים.png"];
    Menu5_1.link=@"";
    Menu5_1.listMenu=nil;
    
    Menu*Menu5_2=[[Menu alloc]init];
    Menu5_2.ID=33;
    Menu5_2.menuID=5;
    Menu5_2.name=@"אתרי מורשת";
    Menu5_2.active=1;
    Menu5_2.tag=5;
    Menu5_2.color=self.appManager.pink2;
    Menu5_2.HighlightedColor=self.appManager.pinkHighlighted2;
    Menu5_2.strColor=@"pink";
    Menu5_2.icon=[UIImage imageNamed:@"אתרי מורשת"];
    Menu5_2.link=@"";
    Menu5_2.listMenu=nil;
    
    Menu*Menu5_3=[[Menu alloc]init];
    Menu5_3.ID=5;
    Menu5_3.menuID=0;
    Menu5_3.name=@"בואו לבקר";
    Menu5_3.active=1;
    Menu5_3.tag=15;
    Menu5_3.color=self.appManager.pink;
    Menu5_3.HighlightedColor=self.appManager.pinkHighlighted;
    Menu5_3.strColor=@"pink";
    Menu5_3.icon=[UIImage imageNamed:@"EventIcon"];
    Menu5_3.link=@"";
    Menu5_3.listMenu=nil;
    
    
    Menu*Menu5=[[Menu alloc]init];
    Menu5.ID=5;
    Menu5.menuID=0;
    Menu5.name=@"תיירות ופנאי";
    Menu5.active=3;
    Menu5.tag=0;
    Menu5.color=self.appManager.pink;
    Menu5.HighlightedColor=self.appManager.pinkHighlighted;
    Menu5.strColor=@"pink";
    Menu5.icon=[UIImage imageNamed:@"EventIcon"];
    Menu5.link=@"";
    Menu5.listMenu=[NSMutableArray arrayWithObjects:Menu5_3,Menu5_2,Menu5_1, nil ];

    
    Menu*Menu6=[[Menu alloc]init];
    Menu6.ID=6;
    Menu6.menuID=0;
    Menu6.name=@"הודעות עבורך";
    Menu6.active=1;
    Menu6.tag=3;
    Menu6.color=[UIColor redColor];
    Menu6.HighlightedColor=self.appManager.redHighlighted2;
    Menu6.strColor=@"red";
    Menu6.icon=[UIImage imageNamed:@"MassegsIcon"];
    Menu6.link=@"";
    Menu6.listMenu=nil;
    
    self.appDelegate.menu=[[NSArray alloc]initWithObjects:Menu1,Menu2,Menu3,Menu4,Menu5,Menu6 ,nil];
    
}

@end
