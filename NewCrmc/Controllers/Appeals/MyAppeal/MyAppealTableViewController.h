//
//  MyAppealTableViewController.h
//  CRMC
//
//  Created by Lior Ronen on 5/12/14.
//  Copyright (c) 2014 neomi&batya&shuli. All rights reserved.
//

#import "MyAppealCell.h"
#import "SubViewController.h"

@interface MyAppealTableViewController : SubViewController <UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) UIImageView *imgBack;
@property (strong, nonatomic) UITableView *tbvAppeals;
@property (retain,nonatomic) UILabel *header;
@property (retain,nonatomic) NSArray *listMyApeals;
@property (retain,nonatomic) UILabel *noapeals;
@property (retain, nonatomic) UIColor *color;

@end
