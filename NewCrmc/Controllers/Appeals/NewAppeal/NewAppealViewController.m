//
//  NewAppealViewController.m
//  Crmc
//
//  Created by MyMac on 5/5/14.
//  Copyright (c) 2014 nbsh. All rights reserved.
//

#import "NewAppealViewController.h"
#import "AppDelegate.h"
#import "City.h"
#import "Street.h"
#import "Report.h"
#import "NSData+Base64.h"
#import "UserDetailsViewController.h"
#import "newTicket.h"
#import "focusStreet.h"
#import "StreetListWrapper.h"
#import "WSResult.h"
#import "WSStreet.h"
#import "NewTicketWrapper.h"

@interface NewAppealViewController ()
{
    NSMutableArray*filterStreet;
    UITableView* autoCompleteTableView;
    BOOL flag;
    int streetId;
    NSString* streetName;
    UIAlertView* alertReport;
    focusStreet *street;
    int num;
    BOOL manualLocationMode;
}
@end

@implementation NewAppealViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    int blockWidth=132,block2Width =blockWidth+35, block2X=10+blockWidth+5;
    num=145;
    _lblbackgroung=[[UILabel alloc]initWithFrame:CGRectMake(0, 156, 320, 285)];
    _lblbackgroung.backgroundColor=_color;
    _lblbackgroung.alpha=0.6;
    [self.view addSubview:_lblbackgroung];
    
    _txtFildCity=[[UITextField alloc]initWithFrame:CGRectMake(7, 163, 307, 39)];
    _txtFildCity.text=self.appDelegate.city.cityName;
    _txtFildCity.textAlignment=NSTextAlignmentRight;
    _txtFildCity.delegate=self;
    _txtFildCity.tag=1;
    _txtFildCity.layer.borderWidth=1;
    _txtFildCity.backgroundColor=[UIColor whiteColor];
    _txtFildCity.font = [UIFont fontWithName:@"Helvetica Neue" size:14.0];
    _txtFildCity.layer.borderColor=[_color CGColor];
    _txtFildCity.layer.sublayerTransform = CATransform3DMakeTranslation(-6, 0, 0);
    [self.view addSubview:_txtFildCity];
    
    //house num
    _txtNumHouse=[[UITextField alloc]initWithFrame:CGRectMake(10, 208, 49, 39)];
    _txtNumHouse.textAlignment=NSTextAlignmentRight;
    _txtNumHouse.delegate=self;
    _txtNumHouse.layer.borderWidth=1;
    [_txtNumHouse setKeyboardType:UIKeyboardTypeNumberPad];
    _txtNumHouse.backgroundColor=[UIColor whiteColor];
    _txtNumHouse.placeholder=@"מספר בית";
    _txtNumHouse.font = [UIFont fontWithName:@"Helvetica Neue" size:13.0];
    _txtNumHouse.layer.borderColor=[_color CGColor];
    _txtNumHouse.layer.sublayerTransform = CATransform3DMakeTranslation(-6, 0, 0);
    [self.view addSubview:_txtNumHouse];
    
    //street
    _txtFildAddress=[[UITextField alloc]initWithFrame:CGRectMake(_txtNumHouse.frame.origin.x+_txtNumHouse.frame.size.width+6, 208, self.view.frame.size.width-71, 39)];
    _txtFildAddress.textAlignment=NSTextAlignmentRight;
    _txtFildAddress.delegate=self;
    _txtFildAddress.layer.borderWidth=1;
    _txtFildAddress.backgroundColor=[UIColor whiteColor];
    _txtFildAddress.placeholder=@"רחוב";
    _txtFildAddress.tag=2;
    _txtFildAddress.font = [UIFont fontWithName:@"Helvetica Neue" size:14.0];
    _txtFildAddress.layer.borderColor=[_color CGColor];
    _txtFildAddress.layer.sublayerTransform = CATransform3DMakeTranslation(-6, 0, 0);
    [self.view addSubview:_txtFildAddress];
    
    _txtViewDescription = [[UITextView alloc]initWithFrame:CGRectMake(block2X, 255, block2Width, 180)];//CGRectMake(94, 255, 220, 180)];
    _txtViewDescription.textAlignment = NSTextAlignmentRight;
    _txtViewDescription.delegate = self;
    _txtViewDescription.layer.borderWidth = 1;
    _txtViewDescription.backgroundColor = [UIColor whiteColor];
    _txtViewDescription.font = [UIFont fontWithName:@"Helvetica Neue" size:14.0];
    _txtViewDescription.textColor = [UIColor lightGrayColor];
    _txtViewDescription.layer.borderColor = [_color CGColor];
    [self.view addSubview:_txtViewDescription];
    
    _btnLocationMethod=[[UIButton alloc]initWithFrame:CGRectMake(10, 208+47, blockWidth, 85)];
    _btnLocationMethod.layer.borderWidth=1.5;
    _btnLocationMethod.backgroundColor=_color;
    [_btnLocationMethod setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _btnLocationMethod.titleLabel.textAlignment=NSTextAlignmentCenter;
    _btnLocationMethod.titleLabel.numberOfLines=3;
    [_btnLocationMethod.titleLabel setFont:[UIFont fontWithName:@"Helvetica Neue" size:13.0]];
    [_btnLocationMethod setTitle:@"set manual mode" forState:UIControlStateNormal];
    _btnLocationMethod.layer.borderColor=_color.CGColor;
    [_btnLocationMethod addTarget:self action:@selector(changeLocationMethod:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btnLocationMethod];
    
    _btnImag_click=[[UIButton alloc]initWithFrame:CGRectMake(10, 208+85+5+47, blockWidth, 137-47)];//CGRectMake(10, 255, 75, 180)];
    _btnImag_click.layer.borderWidth=1.5;
    _btnImag_click.layer.borderColor=_color.CGColor;
    _btnImag_click.backgroundColor=[UIColor whiteColor];
    [_btnImag_click addTarget:self action:@selector(img_click:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btnImag_click];
    
    _imgCamera=[[UIImageView alloc]initWithFrame:CGRectMake(23, 46, 48, 35)];
    _imgCamera.center=CGPointMake(_btnImag_click.frame.size.width/2, _btnImag_click.frame.size.height/2);
    _imgCamera.image=[UIImage imageNamed:@"Camera.png"];
    [_btnImag_click addSubview:_imgCamera];
    
    _imgView=[[UIImageView alloc]initWithFrame:_btnImag_click.frame];
    _imgView.layer.borderWidth=1.5;
    _imgView.layer.borderColor=_color.CGColor;
    [self.view addSubview:_imgView];
    
    _btnSend=[[UIButton alloc]initWithFrame:CGRectMake(7, 444, 307, 28)];
    _btnSend.backgroundColor=_color;
    _btnSend.titleLabel.font= [UIFont boldSystemFontOfSize:24.0f];
    [_btnSend setTitle:@"שלח" forState:UIControlStateNormal];
    [_btnSend setTitleColor:[UIColor whiteColor] forState:normal];
    [_btnSend addTarget:self action:@selector(send_click:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btnSend];
    
    street=[[focusStreet alloc]init];
    street.userName=@"Webit";
    street.password=self.appDelegate.cityContent.passwordToCrmcDB;
    street.clientId=@"1234";
    self.lblBackgroundTitle.backgroundColor=_color;
    self.Title.text=self.header;
    _txtViewDescription.text=@"תאור המפגע";
 
    if(self.appDelegate.city.cityId==9)
    {
        _txtFildAddress.placeholder=@"שם בניין";
        _txtNumHouse.placeholder=@"מספר חדר/משרד";
        _txtNumHouse.font= [UIFont fontWithName:@"Helvetica" size:11];
    }
    
    streetName=[[NSString alloc]init];
    
    [self.generic showNativeActivityIndicator:self];
    StreetListWrapper*streetListWrapper=[[StreetListWrapper alloc]init];
    streetListWrapper.cityPassword=self.appDelegate.cityContent.passwordToCrmcDB;
    streetListWrapper.clientId=self.appDelegate.city.cityId;
    [self.connection connectionToService:@"GetCRMCStreetList" jsonDictionary:[streetListWrapper parseStreetListWrapperToDict] controller:self withSelector:@selector(GetStreetListWrapperResult:)];
    
    autoCompleteTableView=[[UITableView alloc]init];
    autoCompleteTableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    [[autoCompleteTableView layer]setBorderWidth:0.7];
    autoCompleteTableView.delegate= self;
    autoCompleteTableView.dataSource= self;
    autoCompleteTableView.scrollEnabled= YES;
    autoCompleteTableView.hidden= YES;
    autoCompleteTableView.rowHeight= 20;
    [self.view addSubview:autoCompleteTableView];
    if(self.appDelegate.city.cityId==1 || self.appDelegate.city.cityId==18)
    {
        if([[NSUserDefaults standardUserDefaults]objectForKey:@"residentId"]==nil)
        {
            alertReport=[[UIAlertView alloc]initWithTitle:@"" message:@"למשתמש אנונימי אין אפשרות לשלוח פניה" delegate:self cancelButtonTitle:@"להרשמה" otherButtonTitles:@"חזרה לעמוד קודם",nil];
            [alertReport show];
            NSLog(@"Anonymic user");
        }
    }
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = kCLDistanceFilterNone;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)])
    {
        [_locationManager requestAlwaysAuthorization];
    }
    [_locationManager startUpdatingLocation];
    [self changeLocationMethod:nil];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    NSLog(@"NewLocation %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
    [self getInfoAboutLocationWithLatitude:newLocation.coordinate.latitude longtitude:newLocation.coordinate.longitude];
}

-(void)getInfoAboutLocationWithLatitude:(float)latitude longtitude:(float)longtitude
{
    CLGeocoder *ceo = [[CLGeocoder alloc]init];
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:latitude longitude:longtitude];
    
    [ceo reverseGeocodeLocation:loc
              completionHandler:^(NSArray *placemarks, NSError *error) {
                  if (error)
                  {
                      NSLog(@"Could not locate:%@",error.localizedDescription);
                  }
                  else
                  {
                      CLPlacemark *placemark = [placemarks objectAtIndex:0];
                      NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
                      NSLog(@"addressDictionary %@", placemark.addressDictionary);
                      
                      NSString *strName=[placemark.addressDictionary valueForKey:@"Thoroughfare"];
                      NSString *strNum=[placemark.addressDictionary valueForKey:@"SubThoroughfare"];
                      
                      _txtNumHouse.text=strNum;
                      _txtFildAddress.text=strName;
                      
                      NSLog(@"placemark %@",placemark.region);
                      NSLog(@"placemark %@",placemark.country);  // Give Country Name
                      NSLog(@"placemark %@",placemark.locality); // Extract the city name
                      NSLog(@"location %@",placemark.name);//street and number
                      NSLog(@"location %@",placemark.ocean);
                      NSLog(@"location %@",placemark.postalCode);
                      NSLog(@"location %@",placemark.subLocality);
                      
                      NSLog(@"location %@",placemark.location);
                      //Print the location to console
                      NSLog(@"I am currently at %@",locatedAt);
                  }
              }];
}

-(void)GetStreetListWrapperResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    WSResult* wSResult=[[WSResult alloc]init];
     wSResult=[wSResult parseWSResultFromJson:result];
    if(![result isEqualToString:@""]&&![result isEqualToString:@"[]"]&&wSResult.ResultObjects.count!=0)
    {
        filterStreet=[[NSMutableArray alloc]initWithArray:wSResult.ResultObjects];
        _listStreets=[[NSMutableArray alloc]initWithArray:wSResult.ResultObjects];
    }
    else
    {
        alertReport=[[UIAlertView alloc]initWithTitle:@"" message:@" בעיה במערכת רשימת הרחובות אינה חוזרת" delegate:self cancelButtonTitle:@"אישור" otherButtonTitles: nil];
                     [alertReport show];
        NSLog(@"Empty streets list");
    }
}

-(IBAction)changeLocationMethod:(id)sender
{
    NSLog(@"changeLocationMethod!");
    if(flag)
    {
        [self OpenCloseKeyBord:num Flag:NO];
        flag=NO;
    }
    if (manualLocationMode||!sender)
    {
        manualLocationMode=NO;
        NSString* automaticModeStr=@"לא הכתובת הנכונה? להזנה ידנית  של הפרטים לחצו כאן";
        NSMutableAttributedString *as = [[NSMutableAttributedString alloc]initWithString:automaticModeStr attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13]}];
        [as addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:13] range:[automaticModeStr rangeOfString:@"לא הכתובת הנכונה"]];

        [_btnLocationMethod setAttributedTitle:as forState:UIControlStateNormal];
        [_locationManager startUpdatingLocation];
    }
    else
    {
        manualLocationMode=YES;
        NSString *str1=@" GPS הפעלת \n";
        NSString *str2=@" להזנת הפרטים אוטומטית ";
        NSString *str3=@" לחצו כאן ";
        
        NSString *manualModeStr=[[str1 stringByAppendingString:str2]stringByAppendingString:str3];
        NSMutableAttributedString *as = [[NSMutableAttributedString alloc]initWithString:manualModeStr attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13]}];
        [as addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:13] range:[manualModeStr rangeOfString:@"GPS הפעלת"]];
        
        [_btnLocationMethod setAttributedTitle:as forState:UIControlStateNormal];
        [_locationManager stopUpdatingLocation];
        _txtFildAddress.enabled=YES;
        _txtNumHouse.enabled=YES;
    }
}

- (IBAction)send_click:(id)sender
{
    alertReport=[[UIAlertView alloc]initWithTitle:@"" message:@"פניתך נשלחה בהצלחה" delegate:self cancelButtonTitle:@"אישור" otherButtonTitles: nil];
    if([_txtViewDescription.text isEqualToString: @""]||[_txtNumHouse.text isEqualToString: @""]||[_txtFildCity.text isEqualToString: @""]||[_txtFildAddress.text isEqualToString: @""]||[_txtViewDescription.text isEqualToString: @"תאור המפגע"]) {
        [alertReport setMessage:@"חלק משדות החובה חסרים או שתוכנם אינו חוקי. אנא מלאו ונסו שוב"];
        [alertReport show];
    } else {
        if([[NSUserDefaults standardUserDefaults]objectForKey:@"residentId"]==nil)
        {
            alertReport=[[UIAlertView alloc]initWithTitle:@"" message:@"למשתמש אנונימי אין אפשרות לשלוח פניה" delegate:self cancelButtonTitle:@"להרשמה" otherButtonTitles:@"חזרה לעמוד קודם",nil];
            [alertReport show];
        } else {
            NewTicketWrapper*new=[[NewTicketWrapper alloc]initWith:[NSString stringWithFormat:@"%d", self.appDelegate.city.cityId] Description:_txtViewDescription.text AttachedFiles:[self setImageGarbage:_imgView.image] StreetName:streetName HouseNumber:_txtNumHouse.text Password:self.appDelegate.cityContent.passwordToCrmcDB];
            [self.generic showNativeActivityIndicator:self];
            [self.connection connectionToService:@"CreateNewTicket" jsonDictionary:[new parseNewTicketWrapperToDict] controller:self withSelector:@selector(CreateNewTicketResult:)];
        }
    }
}

-(void)CreateNewTicketResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if([result isEqualToString:@""])
        NSLog(@"Exepction");
    else
        [alertReport show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if([alertView.message isEqualToString:@"פניתך נשלחה בהצלחה"])
        [self.navigationController popViewControllerAnimated:YES];
     if([alertView.message isEqualToString:@" בעיה במערכת רשימת הרחובות אינה חוזרת"])
         [self.navigationController popViewControllerAnimated:YES];
    if([alertView.message isEqualToString:@"למשתמש אנונימי אין אפשרות לשלוח פניה"])
    {
        if (buttonIndex == 1)
        {
            [self.navigationController popViewControllerAnimated:NO];
        }
        
        if (buttonIndex == 0)
        {
                EmergencyViewController* emergencyview;
        
         emergencyview = [[UserDetailsViewController alloc]init];
            ((UserDetailsViewController*)emergencyview).first=YES;
            [self.view addSubview:emergencyview.view];
   

    }
    }
    
}

-(NSString*)setImageGarbage:(UIImage*)image
{
    NSData *data = nil;
    NSString *imageData = nil;
    NSData *dataForJPEGFile;
    if(image==Nil)
        return @"";
    else
        dataForJPEGFile = UIImageJPEGRepresentation(image, 1.0);
    image=[[UIImage alloc]initWithData:dataForJPEGFile];
    CGSize newSize = CGSizeMake(320.0f, 480.0f);
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    data=UIImagePNGRepresentation(newImage);
    dataForJPEGFile = UIImageJPEGRepresentation(newImage, 0.2);//picks down
    imageData = [dataForJPEGFile base64EncodedString];
    NSLog(@"image lenght: %lu",(unsigned long)[imageData length]);
    return imageData;
}

#pragma mark AddPictur

- (IBAction)img_click:(id)sender
{

        [_txtViewDescription resignFirstResponder];
        [_txtNumHouse resignFirstResponder];
        [_txtFildAddress resignFirstResponder];
        _txtFildAddress.text=streetName;
        filterStreet=[[NSMutableArray alloc] initWithArray:_listStreets];
        autoCompleteTableView.hidden=YES;
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"בחר באפשרות הרצויה:" delegate:self cancelButtonTitle:@"סגירה" destructiveButtonTitle:nil otherButtonTitles:@"מצלמה",@"גלרית תמונות",nil];
    [popup showInView:[UIApplication sharedApplication].keyWindow];
    if(flag)
    {
        [self OpenCloseKeyBord:num Flag:NO];
        if (self.view.frame.origin.y == num) {
            self.view.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
        }
    }
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
            [self openCamra];
            break;
        case 1:
            [self openGallery];
            break;
        default:
            break;
    }
}

-(void)openCamra
{
    UIImagePickerController *image = [[UIImagePickerController alloc] init];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        image.sourceType = UIImagePickerControllerSourceTypeCamera;
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"תקלה" message:@"מצטערים, אין חיבור של מצלמה במכשיר זה" delegate:nil cancelButtonTitle:@"אשור" otherButtonTitles:nil];
        [alert show];
        return;
    }
    image.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:image.sourceType];
    image.delegate = self;
    [self presentViewController:image animated:YES completion:nil ];
}

-(void)openGallery
{
    UIImagePickerController *image = [[UIImagePickerController alloc] init];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum])
        image.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"תקלה" message:@"מצטערים הגלריה אינה זמינה" delegate:nil cancelButtonTitle:@"אישור" otherButtonTitles:nil];
        [alert show];
        return;
    }
    image.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:image.sourceType];
    image.delegate = self;
    [self presentViewController:image animated:YES completion:nil ];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage *newImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    [_imgView setImage:newImage];
    [_imgView setHidden:NO];
    [_imgCamera setHidden:YES];
    [_btnImag_click  setTitle:@"" forState:UIControlStateNormal];
    [_btnImag_click setBackgroundImage:newImage forState:UIControlStateNormal];
}

#pragma mark UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return filterStreet.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"MyIdentifier"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.textLabel.text = ((WSStreet*)(filterStreet[indexPath.row])).StreetName;
    cell.textLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:12.0];
    [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
    cell.backgroundColor=[UIColor lightGrayColor];
    return cell;
}

-(void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell
    *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    _txtFildAddress.text=selectedCell.textLabel.text;
    streetId=((WSStreet*)filterStreet[indexPath.row]).StreetID;
    streetName=((WSStreet*)filterStreet[indexPath.row]).StreetName;
    [self OpenCloseKeyBord:num Flag:NO];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    _txtFildAddress.text=streetName;
    [self OpenCloseKeyBord:num Flag:NO];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField.tag==1)
        [textField resignFirstResponder];
    else
    {
        if (textField.tag==2 && manualLocationMode == YES)
        {
            _txtFildAddress.text=@"";
            if(filterStreet.count>7)
                autoCompleteTableView.frame = CGRectMake(_txtFildAddress.frame.origin.x , _txtFildAddress.frame.origin.y+_txtFildAddress.frame.size.height, _txtFildAddress.frame.size.width, 8*20);
            else
                autoCompleteTableView.frame = CGRectMake(_txtFildAddress.frame.origin.x , _txtFildAddress.frame.origin.y+_txtFildAddress.frame.size.height, _txtFildAddress.frame.size.width, filterStreet.count*20);
            [autoCompleteTableView reloadData];
                autoCompleteTableView.hidden= NO;
        }
        if(!flag)
            [self OpenCloseKeyBord:-(num) Flag:YES];
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    _txtFildAddress.text=streetName;
    if([_txtViewDescription.text isEqualToString: @""])
    {
        _txtViewDescription.text=@"תאור המפגע";
        _txtViewDescription.textColor=[UIColor lightGrayColor];
    }
    if(flag){
        [self OpenCloseKeyBord:num Flag:NO];
        if (self.view.frame.origin.y == num) {
            self.view.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
        }
    }
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    _txtViewDescription.textColor=[UIColor blackColor];
    if([_txtViewDescription.text isEqualToString:@"תאור המפגע"])
        _txtViewDescription.text=@"";
    if(!flag)
        [self OpenCloseKeyBord:-(num) Flag:YES];
    return YES;
}

- (BOOL)textView:(UITextView *)txtView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"])
    {
        if([_txtViewDescription.text isEqualToString: @""])
        {
            _txtViewDescription.text=@"תאור המפגע";
            _txtViewDescription.textColor=[UIColor lightGrayColor];
        }
        [self OpenCloseKeyBord:num Flag:NO];
    }
    return YES;
}

-(void)OpenCloseKeyBord:(int)num Flag:(BOOL)openClose
{
    if(!openClose)
    {
        autoCompleteTableView.hidden= YES;
        filterStreet=[[NSMutableArray alloc]initWithArray:_listStreets];
        [self.view endEditing:YES];
    }
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+num, self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];
    flag=openClose;
}

-(BOOL)textField:(UITextField*)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString*ns=[[NSString alloc]init];
    ns=[NSString stringWithFormat:@"%@%@",textField.text,string];
    if([string isEqualToString:@""])
        ns=[[ns substringToIndex:[ns length] - 1] substringFromIndex:0];
    if(textField.tag==2)
    {
        if([ns isEqualToString:@""])
            filterStreet=[[NSMutableArray alloc] initWithArray:_listStreets];
        else
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.StreetName contains[c] %@",ns];
            filterStreet = [NSMutableArray arrayWithArray:[_listStreets filteredArrayUsingPredicate:predicate]];
        }
        if(filterStreet.count>7)
            autoCompleteTableView.frame = CGRectMake(_txtFildAddress.frame.origin.x , _txtFildAddress.frame.origin.y+_txtFildAddress.frame.size.height, _txtFildAddress.frame.size.width, 8*20);
        else
            autoCompleteTableView.frame = CGRectMake(_txtFildAddress.frame.origin.x , _txtFildAddress.frame.origin.y+_txtFildAddress.frame.size.height, _txtFildAddress.frame.size.width, filterStreet.count*20);
        if(filterStreet.count==0)
            autoCompleteTableView.hidden=YES;
        
        else
            autoCompleteTableView.hidden=NO;
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"no streets found, please check you entry" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
    }
    [autoCompleteTableView reloadData];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if(textField.tag==2)
    {
        textField.text=streetName;
        filterStreet=[[NSMutableArray alloc] initWithArray:_listStreets];
        autoCompleteTableView.hidden=YES;
    }
    return YES;
}
@end
