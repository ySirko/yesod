//
//  NewAppealViewController.h
//  Crmc
//
//  Created by MyMac on 5/5/14.
//  Copyright (c) 2014 nbsh. All rights reserved.
//

#import "SubViewController.h"
#import <CoreLocation/CoreLocation.h>

@interface NewAppealViewController : SubViewController <UITableViewDataSource,UINavigationControllerDelegate, UITableViewDelegate, UISearchDisplayDelegate,UIAlertViewDelegate, UISearchBarDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UITextFieldDelegate,UITextViewDelegate,CLLocationManagerDelegate>

@property (retain, nonatomic) UILabel*lblbackgroung;
@property (retain, nonatomic) UITextField *txtNumHouse;
@property (retain, nonatomic) UITextField *txtFildCity;
@property (retain, nonatomic) UITextField *txtFildAddress;
@property (retain, nonatomic) UITextView *txtViewDescription;
@property (retain, nonatomic) UIImageView *imgView;
@property (retain, nonatomic) UIImageView *imgCamera;
@property (retain, nonatomic) UIButton *btnImag_click;
@property (retain, nonatomic) UIButton *btnLocationMethod;
@property (retain, nonatomic) UIButton *btnSend;
@property (nonatomic, retain) NSArray *listStreets;
@property (nonatomic, retain) UIColor *color;
@property (nonatomic, retain) CLLocationManager *locationManager;

@end
