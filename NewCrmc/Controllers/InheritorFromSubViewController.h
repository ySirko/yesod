//
//  InheritorFromSubViewController.h
//  NewCrmc
//
//  Created by WebitMacMini Two on 7/30/14.
//  Copyright (c) 2014 webit. All rights reserved.
//
#import "SubViewController.h"
@interface InheritorFromSubViewController : SubViewController
@property(retain,nonatomic)NSArray*listMsg;
@property (retain, nonatomic)  UILabel *lblRunningMessage;
@property BOOL flag;
- (void)name;
@end
