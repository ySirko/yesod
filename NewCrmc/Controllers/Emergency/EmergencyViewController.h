//
//  EmergencyViewController.h
//  CRMC
//
//  Created by WebitMacMini Two on 7/6/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import "SubViewController.h"
#import "EmergencySubViewController.h"
@interface EmergencyViewController : EmergencySubViewController<UITextFieldDelegate>
@property (retain, nonatomic) UIButton *btnNum1;
@property (retain, nonatomic) UIButton *btnNum2;
@property (retain, nonatomic) UIButton *btnNum3;
@property (retain, nonatomic) UIButton *btnIcon1;
@property (retain, nonatomic) UIButton *btnIcon2;
@property (retain, nonatomic) UIButton *btnIcon3;
@end
