//
//  EmergencyViewController.m
//  CRMC
//
//  Created by WebitMacMini Two on 7/6/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import "EmergencyViewController.h"
#import "EmergencySubViewController.h"
#import "setEmergencyNumbersViewController.h"
@interface EmergencyViewController ()
{
    NSMutableArray* buttonArray;
    int counter;
    UIAlertView *alert;
}
@end

@implementation EmergencyViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _btnNum1=[[UIButton alloc]initWithFrame:CGRectMake(30, 140, 260, 50)];
    _btnNum2=[[UIButton alloc]initWithFrame:CGRectMake(30, 200, 260, 50)];
    _btnNum3=[[UIButton alloc]initWithFrame:CGRectMake(30, 260, 260, 50)];
    buttonArray=[[NSMutableArray alloc]init];
    [buttonArray addObject:_btnNum1];
    [buttonArray addObject:_btnNum2];
    [buttonArray addObject:_btnNum3];
    for(int i=0; i<3;i++)
    {
        ((UIButton*)buttonArray[i]).backgroundColor=[UIColor whiteColor];
        [(UIButton*)buttonArray[i] setTitleColor:[UIColor blackColor] forState:normal];
        [(UIButton*)buttonArray[i] setTitleEdgeInsets:UIEdgeInsetsMake(0,-90, 0, 0)];
        ((UIButton*)buttonArray[i]).layer.borderWidth=2;
        ((UIButton*)buttonArray[i]).layer.cornerRadius=4;
        [self.view addSubview:buttonArray[i]];
         ((UIButton*)buttonArray[i]).layer.borderColor=[UIColor colorWithRed:9.0f/255.0f green:134.0f/255.0f blue:134.0f/255.0f alpha:1.0f].CGColor;
        if([[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"EmergencyNumber%d",i+1]] isEqualToString:@""])
        {
            counter++;
       [((UIButton*)buttonArray[i]) setTitle:@"לא הוגדר מספר" forState:normal];
            [((UIButton*)buttonArray[i]) setEnabled:NO];
        }
        else
        {
            [((UIButton*)buttonArray[i]) setTitle:[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"EmergencyNumber%d",i+1]] forState:normal];
            [(UIButton*)buttonArray[i] addTarget:self action:@selector(btnCallClick:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    if(counter==3)
        {
            alert=[[UIAlertView alloc]initWithTitle:@"" message:@"לא הוגדרו מספרי חירום ,להגדרות הכנס בדף הבית להגדרות -> לחצן מצוקה " delegate:self cancelButtonTitle:@"אישור" otherButtonTitles: nil];
           [alert show];
        }
    _btnIcon1=[[UIButton alloc]initWithFrame:CGRectMake(252, 150, 18, 30)];
    [_btnIcon1 setImage:[UIImage imageNamed:@"purplePhone.png"] forState:normal];
    [self.view addSubview:_btnIcon1];
    _btnIcon2=[[UIButton alloc]initWithFrame:CGRectMake(252, 210, 18, 30)];
    [_btnIcon2 setImage:[UIImage imageNamed:@"purplePhone.png"] forState:normal];
    [self.view addSubview:_btnIcon2];
    _btnIcon3=[[UIButton alloc]initWithFrame:CGRectMake(252, 270, 18, 30)];
    [_btnIcon3 setImage:[UIImage imageNamed:@"purplePhone.png"] forState:normal];
    [self.view addSubview:_btnIcon3];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnCallClick:(id)sender
{
       UIApplication *myApp = [UIApplication sharedApplication];
        NSString *theCall = [NSString stringWithFormat:@"tel://%@",((UIButton*)sender).titleLabel.text];
        NSLog(@"making call with %@",theCall);
        [myApp openURL:[NSURL URLWithString:theCall]];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.view removeFromSuperview];
}

@end
