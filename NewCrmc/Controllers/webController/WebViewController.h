//
//  WebViewController.h
//  NewCrmc
//
//  Created by MyMac on 7/31/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "SubViewController.h"

@interface WebViewController : SubViewController<UIWebViewDelegate>
@property (retain, nonatomic) UIWebView *web;
@property(retain,nonatomic)NSString*nsurl;
@property(retain,nonatomic)UIColor *color;
@property BOOL IsHtml;
@end
