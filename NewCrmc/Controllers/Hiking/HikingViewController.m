//
//  HikingViewController.m
//  מגדל העמק
//
//  Created by MyMac on 12/9/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "HikingViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "LatLng.h"
#import "LatLngType.h"
#import "LatLngType.h"
@interface HikingViewController ()
{
    GMSMapView *mapView_;
    BOOL firstLocationUpdate_;
    NSMutableArray *markers;
    CLLocation *location;
    GMSPolyline *polyline;
    GMSMutablePath *path;
    NSURL *url;
    BOOL wayToSource;
    GMSMarker* marker;
    NSArray*listTyps;
}
@end

@implementation HikingViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.connection connectionToService:@"GetLatLngTypesByCityId" jsonDictionary:[self.appDelegate.city parseCityToDict] controller:self withSelector:@selector(GetLatLngTypesByCityIdResult:)];
    self.lblBackgroundTitle.backgroundColor=_color;
    self.Title.text=self.header;
    float cityLatitude=32.441038, cityLongtitude=34.914251;
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:cityLatitude longitude:cityLongtitude zoom:13.5];
    mapView_ = [GMSMapView mapWithFrame:CGRectMake(0, 132, 320, 380) camera:camera];
    mapView_.myLocationEnabled = YES;
    [self.view addSubview:mapView_];
    markers=[[NSMutableArray alloc]init];
    [mapView_ addObserver:self
               forKeyPath:@"myLocation"
                  options:NSKeyValueObservingOptionNew
                  context:NULL];
    dispatch_async(dispatch_get_main_queue(), ^{
        mapView_.myLocationEnabled = YES;
    });
    [self showTrip];
}

-(void)GetLatLngTypesByCityIdResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if(![result isEqualToString:@""]&&![result isEqualToString:@"[]"])
    {
        LatLngType*ll=[[LatLngType alloc]init];
        listTyps=[ll parseListLatLngTypeFromJson:result];
    }
    else
    {
        NSLog(@"Empty typs list");
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showTrip
{
    GMSMutablePath *tripPath = [GMSMutablePath path];
    for(LatLng*ll in _trip.lLatLngList)
    {
        NSArray*a = [listTyps filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"latLngTypeId =[cd] %d",ll.iLatLngType]];
        LatLngType*t=a[0];
        double lat=ll.nvLat.doubleValue;
        double lng=ll.nvLng.doubleValue;
        CLLocationCoordinate2D positionSrc =CLLocationCoordinate2DMake(lat,lng);
        marker = [GMSMarker markerWithPosition:positionSrc];
        marker.title =ll.nvLatLangInfo;
        marker.appearAnimation = kGMSMarkerAnimationPop;
        marker.icon = [GMSMarker markerImageWithColor:t.color];
        marker.map = mapView_;
        [markers addObject:marker];
        NSLog(@"lat:%@, long:%@", ll.nvLat,ll.nvLng);
        [tripPath addLatitude:lat longitude:lng];
    }
    [self didTapFitBounds];
    
    GMSPolyline *tripPolyline = [GMSPolyline polylineWithPath:tripPath];
    tripPolyline.strokeColor = [UIColor blueColor];
    tripPolyline.strokeWidth = 2.f;
    tripPolyline.map = mapView_;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (!firstLocationUpdate_)
    {
        // If the first location update has not yet been recieved, then jump to that
        // location.
        firstLocationUpdate_ = YES;
//        location = [change objectForKey:NSKeyValueChangeNewKey];
//        mapView_.camera = [GMSCameraPosition cameraWithTarget:location.coordinate zoom:15];
//        CLLocationCoordinate2D position =CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
//        GMSMarker* markerCurrentLocation = [GMSMarker markerWithPosition:position];
//        markerCurrentLocation.title = @"מיקום נוכחי";
//        markerCurrentLocation.appearAnimation = kGMSMarkerAnimationPop;
//        markerCurrentLocation.icon = [GMSMarker markerImageWithColor:[UIColor colorWithRed:255/255.0f green:36/255.0f blue:122/255.0f alpha:0.9]];
//        markerCurrentLocation.map = mapView_;
//        [markers addObject:markerCurrentLocation];
        GMSMutablePath *tripPath = [GMSMutablePath path];
        for(LatLng*ll in _trip.lLatLngList)
        {
            NSArray*a = [listTyps filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"latLngTypeId =[cd] %d",ll.iLatLngType]];
            LatLngType*t=a[0];
            CLLocationCoordinate2D positionSrc =CLLocationCoordinate2DMake( [ll.nvLng doubleValue],[ll.nvLat doubleValue]);
            marker = [GMSMarker markerWithPosition:positionSrc];
            marker.title =ll.nvLatLangInfo;
            marker.appearAnimation = kGMSMarkerAnimationPop;
            marker.icon = [GMSMarker markerImageWithColor:t.color];
            marker.map = mapView_;
            [markers addObject:marker];
//            if(ll==_trip.lLatLngList.firstObject)
//                wayToSource=YES;
//            else
//                wayToSource=NO;
//            [self drawDirection:((GMSMarker*)markers[markers.count-2]).position and:marker.position];
            [tripPath addLatitude:[ll.nvLng doubleValue] longitude:[ll.nvLat doubleValue]];
        }
        [self didTapFitBounds];
        
        GMSPolyline *tripPolyline = [GMSPolyline polylineWithPath:tripPath];
        tripPolyline.strokeColor = [UIColor blueColor];
        tripPolyline.strokeWidth = 2.f;
        tripPolyline.map = mapView_;
    }
}

- (void)didTapFitBounds
{
    GMSCoordinateBounds *bounds;
    for (GMSMarker *mrk in markers)
    {
        if (bounds == nil)
        {
            bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:mrk.position coordinate:mrk.position];
        }
        bounds = [bounds includingCoordinate:mrk.position];
    }
    GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds withPadding:50.0f];
    [mapView_ moveCamera:update];
}

//- (void) drawDirection:(CLLocationCoordinate2D)source and:(CLLocationCoordinate2D) dest
//{
//    polyline = [[GMSPolyline alloc] init];
//    path = [GMSMutablePath path];
//    NSArray * points = [self calculateRoutesFrom:source to:dest];
//    NSInteger numberOfSteps = points.count;
//    for (NSInteger index = 0; index < numberOfSteps; index++)
//    {
//        CLLocation *location = [points objectAtIndex:index];
//        CLLocationCoordinate2D coordinate = location.coordinate;
//        [path addCoordinate:coordinate];
//    }
//    if(points.count==0)
//        return;
//    
//    polyline.path = path;
//    if(wayToSource)
//    {
//        polyline.strokeColor =[UIColor colorWithRed:255/255.0f green:36/255.0f blue:122/255.0f alpha:0.9];
//        polyline.strokeWidth = 2.5f;
//    }
//    else
//    {
//        polyline.strokeColor = [UIColor colorWithRed:70.0f/255.0f green:120.0f/255.0f blue:34.0f/255.0f alpha:0.9];
//        polyline.strokeWidth = 2.f;
//    }
//    polyline.map = mapView_;
//    polyline.geodesic = NO;
//    
//    [self didTapFitBounds];
//}

#pragma mark - CalculateRoutes
-(NSArray*) calculateRoutesFrom:(CLLocationCoordinate2D) f to: (CLLocationCoordinate2D) t
{
    NSString* saddr = [NSString stringWithFormat:@"%f,%f", f.latitude, f.longitude];
    NSString* daddr = [NSString stringWithFormat:@"%f,%f", t.latitude, t.longitude];
    if(wayToSource)
        url=[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&sensor=false&avoid=highways&mode=driving",saddr,daddr]];
    else
        url=[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&sensor=false&avoid=highways&mode=walking",saddr,daddr]];
    
    NSError *error=nil;
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    NSURLResponse *response = nil;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error: &error];
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONWritingPrettyPrinted error:&error];
    return [self decodePolyLine:[self parseResponse:dic]];
}

- (NSString *)parseResponse:(NSDictionary *)response {
    NSArray *routes = [response objectForKey:@"routes"];
    NSDictionary *route = [routes lastObject];
    if (route) {
        NSString *overviewPolyline = [[route objectForKey:
                                       @"overview_polyline"] objectForKey:@"points"];
        return overviewPolyline;
    }
    return @"";
}

-(NSMutableArray *)decodePolyLine:(NSString *)encodedStr {
    NSMutableString *encoded = [[NSMutableString alloc]
                                initWithCapacity:[encodedStr length]];
    [encoded appendString:encodedStr];
    [encoded replaceOccurrencesOfString:@"\\\\" withString:@"\\"
                                options:NSLiteralSearch
                                  range:NSMakeRange(0,
                                                    [encoded length])];
    NSInteger len = [encoded length];
    NSInteger index = 0;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    NSInteger lat=0;
    NSInteger lng=0;
    while (index < len) {
        NSInteger b;
        NSInteger shift = 0;
        NSInteger result = 0;
        do {
            b = [encoded characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        NSInteger dlat = ((result & 1) ? ~(result >> 1)
                          : (result >> 1));
        lat += dlat;
        shift = 0;
        result = 0;
        do {
            b = [encoded characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        NSInteger dlng = ((result & 1) ? ~(result >> 1)
                          : (result >> 1));
        lng += dlng;
        NSNumber *latitude = [[NSNumber alloc] initWithFloat:lat * 1e-5];
        NSNumber *longitude = [[NSNumber alloc] initWithFloat:lng * 1e-5];
        CLLocation *location = [[CLLocation alloc] initWithLatitude:
                                [latitude floatValue] longitude:[longitude floatValue]];
        [array addObject:location];
    }
    return array;
}
@end
