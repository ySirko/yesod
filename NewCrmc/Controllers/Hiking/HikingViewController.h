//
//  HikingViewController.h
//  מגדל העמק
//
//  Created by MyMac on 12/9/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubViewController.h"
#import "Trip.h"
@interface HikingViewController : SubViewController
@property(strong,nonatomic)UIColor*color;
@property(retain,nonatomic)Trip*trip;
@end
