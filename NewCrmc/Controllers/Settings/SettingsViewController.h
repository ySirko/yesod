//
//  SettingsViewController.h
//  NewCrmc
//
//  Created by WebitMacMini Two on 7/21/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "SubViewController.h"
#import "EmergencySubViewController.h"
@interface SettingsViewController : EmergencySubViewController
@property (retain,nonatomic)NSArray*list;
@property (retain, nonatomic) UITableView *tbl;
@property (retain, nonatomic) UILabel *lblTitleQuestion;
@property (retain, nonatomic) UIButton*btnOK;
@property int isInterestArea;
@property NSMutableArray *switchesValues;
@end
