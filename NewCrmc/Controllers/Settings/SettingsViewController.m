//
//  SettingsViewController.m
//  NewCrmc
//
//  Created by WebitMacMini Two on 7/21/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "SettingsViewController.h"
#import "SettingsCell.h"
#import "InterestArea.h"
#import "EducationInstitution.h"
#import "Resident.h"
#import "HomeViewController.h"
@interface SettingsViewController ()
{
    NSArray*choose;
    Resident* resident;
}
@end

@implementation SettingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _tbl=[[UITableView alloc]initWithFrame:CGRectMake(20, 136, 280, 250)];
    _tbl.dataSource=self;
    _tbl.delegate=self;
    [self.view addSubview:_tbl];
   _lblTitleQuestion=[[UILabel alloc]initWithFrame:CGRectMake(40, 112, 243, 21)];
    _lblTitleQuestion.textColor=[UIColor whiteColor];
    _lblTitleQuestion.textAlignment=NSTextAlignmentRight;
    _lblTitleQuestion.font=[UIFont fontWithName:@"" size:17];
    [self.view addSubview:_lblTitleQuestion];
    
    _btnOK=[[UIButton alloc]initWithFrame:CGRectMake(20, 384, 280, 27)];
    _btnOK.backgroundColor=[UIColor colorWithRed:9.0f/255.0f green:134.0f/255.0f blue:134.0f/255.0f alpha:1.0f];
    [self.view addSubview:_btnOK];
    [_btnOK setTitle:@"אישור" forState:normal];
    [_btnOK addTarget:self action:@selector(btnNextClick:) forControlEvents:UIControlEventTouchUpInside];
    
    resident=[[Resident alloc]init];
    [self.generic showNativeActivityIndicator:self];
    if(_isInterestArea==1)
    {
        _lblTitleQuestion.text=@"אילו תחומי ענין מענינים אותך?";
        [self.connection connectionToService:@"GetInterestAreaToCity" jsonDictionary:[self.appDelegate.city parseCityToDict] controller:self withSelector:@selector(GetInterestAreaToCityResult:)];
        self.switchesValues=[NSMutableArray new];
    }
    else
    {
        _lblTitleQuestion.text=@"מאילו מוסדות לימוד הינך מעונין לקבל הודעות?";
        [_lblTitleQuestion setFont:[UIFont systemFontOfSize:13]];
        [self.connection connectionToService:@"GetEducationInstitutionToCity" jsonDictionary:[self.appDelegate.city parseCityToDict] controller:self withSelector:@selector(GetEducationInstitutionToCityResult:)];
    }
    _tbl.backgroundColor =[UIColor clearColor];
    _tbl.separatorColor=[UIColor clearColor];
}

-(void)GetInterestAreaToCityResult:(NSString*)result
{
    if(![result isEqualToString:@""]&&![result isEqualToString:@"[]"])
    {
        InterestArea * interestArea=[[InterestArea alloc]init];
        _list=[[NSArray alloc]initWithArray:[interestArea parseListInterestAreaFromJson:result]];
        resident.residentId=(int)[[[NSUserDefaults standardUserDefaults]objectForKey:@"residentId"]integerValue];
        resident.cityId=self.appDelegate.city.cityId;
        [self.connection connectionToService:@"GetInterestAreasToResident" jsonDictionary:[resident parseResidentIdToDict] controller:self withSelector:@selector(GetInterestAreasToResidentResult:)];
        [_switchesValues removeAllObjects];
        _switchesValues =[NSMutableArray new];
        for (int i=0;i<_list.count;i++) {
            [_switchesValues addObject:[NSNull null]];
        }
    }
    else
    {
        NSLog(@"Empty reports list");
        [self.generic hideNativeActivityIndicator:self];
    }
}
-(void)GetInterestAreasToResidentResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if(![result isEqualToString:@""]&&![result isEqualToString:@"[]"])
    {
        InterestArea * interestArea=[[InterestArea alloc]init];
        choose=[[NSArray alloc]initWithArray:[interestArea parseListInterestAreaFromJson:result]];
    }
    else
        NSLog(@"Empty interestArea list");
    [_tbl reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _list.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    SettingsCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell = [[SettingsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell.lblBackgroung=[[UILabel alloc]initWithFrame:CGRectMake(0, 3, 280, 40)];
    cell.lblBackgroung.backgroundColor=[UIColor whiteColor];
    [cell addSubview:cell.lblBackgroung];
    NSArray* filterArray;
    cell.lbl=[[UILabel alloc]initWithFrame:CGRectMake(116, 12, 149, 26)];
    [cell addSubview:cell.lbl];
    cell.lbl.layer.cornerRadius=2;
    cell.lbl.textAlignment = NSTextAlignmentRight;
    cell.btnSwitchBack=[[UIButton alloc]initWithFrame:CGRectMake(17,6,33,33)];
    [cell addSubview:cell.btnSwitchBack];
    cell.btnSwitch=[[UIButton alloc]initWithFrame:CGRectMake(20,9,27,27)];
    [cell addSubview:cell.btnSwitch];
    cell.btnSwitch.tag = indexPath.row;
    [cell.btnSwitch addTarget:cell action:@selector(btnSwichClick:) forControlEvents:UIControlEventTouchUpInside];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    if([NSStringFromClass([_list[indexPath.row]class])isEqualToString:@"EducationInstitution"])
    {
        cell.lbl.text=((EducationInstitution*)_list[indexPath.row]).educationInstitutionName;
        filterArray = [choose filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"educationInstitutionId =[cd] %d",((EducationInstitution*)_list[indexPath.row]).educationInstitutionId]];
    }
    else
    {
        cell.lbl.text=((InterestArea*)_list[indexPath.row]).interestAreaName;
        filterArray = [choose filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"interestAreaId =[cd] %d",((InterestArea*)_list[indexPath.row]).interestAreaId]];
        __weak typeof (cell) weakcell=cell;
        [cell setSwitchHandler:^(BOOL switched) {
            [self switchButton:weakcell.btnSwitch didClick:switched];
        }];
    }
    if(filterArray.count>0)
    {
        cell.turnedOn=YES;
        [self.switchesValues replaceObjectAtIndex:indexPath.row withObject:@YES];
    }
    else
    {
        cell.turnedOn=NO;
        [self.switchesValues replaceObjectAtIndex:indexPath.row withObject:@NO];
    }
    cell.btnSwitchBack.layer.borderColor=[UIColor colorWithRed:9.0f/255.0f green:134.0f/255.0f blue:134.0f/255.0f alpha:1.0f].CGColor;
    cell.btnSwitchBack.layer.borderWidth=0.5f;
    cell.backgroundColor=[UIColor clearColor];
    cell.btnSwitch.layer.cornerRadius=14;
    cell.btnSwitchBack.layer.cornerRadius=17;
    return cell;
}

-(IBAction)switchButton:(UIButton*)button didClick:(BOOL)newValue
{
    [_switchesValues replaceObjectAtIndex:button.tag withObject:[NSNumber numberWithBool:newValue]];
}

- (IBAction)btnNextClick:(id)sender
{
    Resident*resident1=[[Resident alloc]init];
    resident1.residentId=(int)[[[NSUserDefaults standardUserDefaults] objectForKey:@"residentId"] integerValue];
    [self.generic showNativeActivityIndicator:self];
    if([NSStringFromClass([_list[0]class])isEqualToString:@"InterestArea"])
    {
        resident1.interestAreasList=[[NSMutableArray alloc]init];
        for(int i=0;i<_list.count;i++)
        {
            if ([[_switchesValues objectAtIndex:i]isKindOfClass:[NSNumber class]])
            {
                if ([[_switchesValues objectAtIndex:i]boolValue]) {
                    [resident1.interestAreasList addObject:_list[i]];

                }
            }
        }
        NSDictionary *interestsDict=[resident1 parseResidentToDictSettingInterestAreas];
        [self.connection connectionToService:@"SetInterestAreasToResident"
                              jsonDictionary:interestsDict
                                  controller:self
                                withSelector:@selector(SetInterestAreasToResidentResult:)];
    }
    else
    {
        resident1.EducationInstitutionsList=[[NSMutableArray alloc]init];
        for(int i=0;i<_list.count;i++)
            if(!(((SettingsCell*)[_tbl visibleCells][i]).btnSwitch.backgroundColor==[UIColor clearColor]))
                [resident1.EducationInstitutionsList addObject:_list[i]];
        [self.connection connectionToService:@"SetEducationInstitutionsToResident"
                              jsonDictionary:[resident1
                                              parseResidentToDictSettingEducationInstitutions]
                                  controller:self
                                withSelector:@selector(SetEducationInstitutionsToResidentResult:)];
    }
}
-(void)SetInterestAreasToResidentResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if([result isEqualToString:@"true"])
    {
        [UIView animateWithDuration:0.5
                              delay:.2f
                            options: UIViewAnimationOptionAllowAnimatedContent
                         animations:^{ [self.view setAlpha:0];
                         }
                         completion:^(BOOL finished){
                             [self.view removeFromSuperview];
                         }];
    }
    else
        NSLog(@"Empty reports list");
}
-(void)GetEducationInstitutionToCityResult:(NSString*)result
{
    if(![result isEqualToString:@""]&&![result isEqualToString:@"[]"])
    {
        EducationInstitution * educationInstitution=[[EducationInstitution alloc]init];
        _list=[[NSArray alloc]initWithArray:[educationInstitution parseListEducationInstitutionFromJson:result]];
        resident.residentId=[[[NSUserDefaults standardUserDefaults]objectForKey:@"residentId"]integerValue];
        [self.connection connectionToService:@"GetEducationInstitutionsToResident" jsonDictionary:[resident parseResidentIdToDict] controller:self withSelector:@selector(GetEducationInstitutionsToResidentResult:)];
    }
    else
    {
        NSLog(@"Empty reports list");
        [self.generic hideNativeActivityIndicator:self];
    }
}

-(void)GetEducationInstitutionsToResidentResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if(![result isEqualToString:@""]&&![result isEqualToString:@"[]"])
    {
        EducationInstitution * educationInstitution=[[EducationInstitution alloc]init];
        choose=[[NSArray alloc]initWithArray:[educationInstitution parseListEducationInstitutionFromJson:result]];
    }
    else
        NSLog(@"Empty interestArea list");
    [_tbl reloadData];
}
-(void)SetEducationInstitutionsToResidentResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if([result isEqualToString:@"true"])
    {
        [UIView animateWithDuration:0.5
                              delay:.2f
                            options: UIViewAnimationOptionAllowAnimatedContent
                         animations:^{ [self.view setAlpha:0];
                         }
                         completion:^(BOOL finished){
                             [self.view removeFromSuperview];
                         }];    
    }
    else
        NSLog(@"Empty reports list");
}
@end
