//
//  HomeSettingsViewController.m
//  NewCrmc
//
//  Created by MyMac on 8/7/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "HomeSettingsViewController.h"
#import "UserDetailsViewController.h"
#import "EmergencySubViewController.h"
#import "SettingsViewController.h"
#import "setEmergencyNumbersViewController.h"

@interface HomeSettingsViewController ()
{
    EmergencySubViewController*view;
    BOOL flag;
}
@end

@implementation HomeSettingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    if(self.appDelegate.city.cityId != 9 && self.appDelegate.city.cityId != 1 && self.appDelegate.city.cityId != 18)
    {
        UIButton*btn1=[[UIButton alloc]initWithFrame:CGRectMake(30, 123, 260, 50)];
        btn1.titleLabel.font=[UIFont fontWithName:@"" size:22];
        btn1.backgroundColor=[UIColor whiteColor];
        btn1.titleLabel.textAlignment=NSTextAlignmentRight;
        btn1.layer.cornerRadius=4;
        [btn1 setTitle:@"לחצן מצוקה" forState:UIControlStateNormal];
        [btn1 addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        btn1.tag=5;
        [btn1 setTitleColor:[UIColor colorWithRed:9.0f/255.0f green:134.0f/255.0f blue:134.0f/255.0f alpha:1.0f]forState:UIControlStateNormal];
        [self.view addSubview:btn1];
        UIImageView*img1=[[UIImageView alloc]initWithFrame:CGRectMake(36, 132, 30, 30)];
        img1.image=[UIImage imageNamed:@"iconEmergency.png"];
        [self.view addSubview:img1];
        [btn1 setTitleEdgeInsets:UIEdgeInsetsMake(0,142, 0, 0)];
    }
    
    UIButton*btn2=[[UIButton alloc]initWithFrame:CGRectMake(30, 178, 260, 50)];
    UIImageView*img2=[[UIImageView alloc]initWithFrame:CGRectMake(36, 187, 30, 30)];
    if (self.appDelegate.city.cityId == 18) {
        btn2=[[UIButton alloc]initWithFrame:CGRectMake(30, 233, 260, 50)];
        img2=[[UIImageView alloc]initWithFrame:CGRectMake(36, 241, 30, 30)];
    }
    btn2.titleLabel.font=[UIFont fontWithName:@"" size:22];
    btn2.backgroundColor=[UIColor whiteColor];
    btn2.titleLabel.textAlignment=NSTextAlignmentRight;
    btn2.layer.cornerRadius=4;
    [btn2 setTitle:@"פרטים אישיים" forState:UIControlStateNormal];
    [btn2 addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    btn2.tag=2;
    [btn2 setTitleColor:[UIColor colorWithRed:9.0f/255.0f green:134.0f/255.0f blue:134.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [btn2 setTitleEdgeInsets:UIEdgeInsetsMake(0,130, 0, 0)];
    [self.view addSubview:btn2];
    
    img2.image=[UIImage imageNamed:@"iconUserDetails.png"];
    [self.view addSubview:img2];
    
    if (self.appDelegate.city.cityId == 18) {
        return;
    }
    
    UIButton*btn3=[[UIButton alloc]initWithFrame:CGRectMake(30, 233, 260, 50)];
    btn3.titleLabel.font=[UIFont fontWithName:@"" size:22];
    btn3.backgroundColor=[UIColor whiteColor];
    btn3.titleLabel.textAlignment=NSTextAlignmentRight;
    btn3.layer.cornerRadius=4;
    [btn3 setTitle:@"תחומי עניין" forState:UIControlStateNormal];
    [btn3 addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    btn3.tag=3;
    [btn3 setTitleColor:[UIColor colorWithRed:9.0f/255.0f green:134.0f/255.0f blue:134.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [btn3 setTitleEdgeInsets:UIEdgeInsetsMake(0,143, 0, 0)];
    [self.view addSubview:btn3];
    
    UIButton*btn4=[[UIButton alloc]initWithFrame:CGRectMake(30, 288, 260, 50)];
    btn4.titleLabel.font=[UIFont fontWithName:@"" size:22];
    btn4.layer.cornerRadius=4;
    btn4.backgroundColor=[UIColor whiteColor];
    [btn4.titleLabel setTextAlignment:NSTextAlignmentLeft];
    [btn4 setTitle:@"קבלת הודעות" forState:UIControlStateNormal];
    [btn4 addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    btn4.tag=4;
    [btn4 setTitleColor:[UIColor colorWithRed:9.0f/255.0f green:134.0f/255.0f blue:134.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [btn4 setTitleEdgeInsets:UIEdgeInsetsMake(0,130, 0, 0)];
    [self.view addSubview:btn4];
    
    UIImageView*img3=[[UIImageView alloc]initWithFrame:CGRectMake(36, 241, 30, 30)];
    img3.image=[UIImage imageNamed:@"iconInterestArea.png"];
    [self.view addSubview:img3];
    UIImageView*img4=[[UIImageView alloc]initWithFrame:CGRectMake(36, 298, 30, 30)];
    img4.image=[UIImage imageNamed:@"iconMessage.png"];
    [self.view addSubview:img4];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)btnClick:(id)sender
{
    switch (((UIButton*)sender).tag)
    {
        case 5:
            view=[[setEmergencyNumbersViewController alloc]init];
            break;
        case 2:
            view=[[UserDetailsViewController alloc]init];
            break;
        case 3:
            if([[NSUserDefaults standardUserDefaults]objectForKey:@"residentId"]==0)
            {
                UIAlertView* alertDetails=[[UIAlertView alloc]initWithTitle:@"" message:@"בכדי לעדכן את ההגדרות עליך להרשם במערכת" delegate:self cancelButtonTitle:@"רישום" otherButtonTitles:@"ביטול", nil];
                [alertDetails show];
                flag=YES;
            }
            else
            {
                view=[[SettingsViewController alloc]init];
                ((SettingsViewController*)view).isInterestArea=1;
            }
            break;
        case 4:
            if([[NSUserDefaults standardUserDefaults]objectForKey:@"residentId"]==0)
            {
                UIAlertView* alertDetails=[[UIAlertView alloc]initWithTitle:@"" message:@"בכדי לעדכן את ההגדרות עליך להרשם במערכת" delegate:self cancelButtonTitle:@"רישום" otherButtonTitles:@"ביטול", nil];
                [alertDetails show];
                flag=YES;
            }
            else
            {
                view=[[SettingsViewController alloc]init];
                ((SettingsViewController*)view).isInterestArea=2;
            }
            break;
            
        default:
            break;
    }
    if(!flag)
    {
        view.index=((UIButton*)sender).tag;
        view.view.alpha=0;
        [self.view addSubview:view.view];
        [UIView animateWithDuration:0.5
                              delay:.2f
                            options: UIViewAnimationOptionLayoutSubviews
                         animations:^{
                             [view.view setAlpha:1];
                         }
                         completion:^(BOOL finished){}];
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==0)
    {
        flag=NO;
        view=[[UserDetailsViewController alloc]init];
        view.index=2;
        view.view.alpha=0;
        [self.view addSubview:view.view];
        [UIView animateWithDuration:0.5
                              delay:.2f
                            options: UIViewAnimationOptionLayoutSubviews
                         animations:^{
                             [view.view setAlpha:1];
                         }
                         completion:^(BOOL finished){}];
    }
}

@end
