//
//  UserDetailsViewController.m
//  NewCrmc
//
//  Created by WebitMacMini Two on 7/23/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "UserDetailsViewController.h"
#import "Resident.h"
#import "Street.h"
#import "SettingsViewController.h"
#import "HomeViewController.h"
#import "StreetListWrapper.h"
#import "WSResult.h"
#import "WSStreet.h"
@interface UserDetailsViewController ()
{
    Street*street;
    UITableView* autoCompleteTableView;
    NSMutableArray*filterStreet;
    BOOL fill;
    BOOL flag;
    int height;
}
@end

@implementation UserDetailsViewController

- (void)viewDidLoad
{
    height=32;
    [super viewDidLoad];
    _txtLastname=[[UITextField alloc]initWithFrame:CGRectMake(30, 115, 132, height)];
    _txtLastname.delegate=self;
    _txtLastname.placeholder=@"שם משפחה";
    _txtLastname.textAlignment=NSTextAlignmentRight;
    [self.view addSubview:_txtLastname];
    _txtFirstname=[[UITextField alloc]initWithFrame:CGRectMake(164, 115, 126, height)];
    _txtFirstname.delegate=self;
    _txtFirstname.placeholder=@"שם פרטי";
    _txtFirstname.textAlignment=NSTextAlignmentRight;
    [self.view addSubview:_txtFirstname];
    _txtMobile=[[UITextField alloc]initWithFrame:CGRectMake(30,150,260, height)];
    _txtMobile.delegate=self;
    _txtMobile.placeholder=@"נייד";
    _txtMobile.textAlignment=NSTextAlignmentRight;
    [_txtMobile setKeyboardType:UIKeyboardTypeNumberPad];
    [self.view addSubview:_txtMobile];
    _txtPhone=[[UITextField alloc]initWithFrame:CGRectMake(30,185,260, height)];
    _txtPhone.delegate=self;
    _txtPhone.placeholder=@"טלפון";
    _txtPhone.textAlignment=NSTextAlignmentRight;
    [_txtPhone setKeyboardType:UIKeyboardTypeNumberPad];
    [self.view addSubview:_txtPhone];
    _txtEmail=[[UITextField alloc]initWithFrame:CGRectMake(30,220,260, height)];
    _txtEmail.delegate=self;
    _txtEmail.placeholder=@"אימייל";
    _txtEmail.textAlignment=NSTextAlignmentRight;
    [_txtEmail setKeyboardType:UIKeyboardTypeEmailAddress];
    [self.view addSubview:_txtEmail];
    _txtStreet=[[UITextField alloc]initWithFrame:CGRectMake(112,255,178, height)];
    _txtStreet.delegate=self;
    _txtStreet.placeholder=@"רחוב";
    _txtStreet.textAlignment=NSTextAlignmentRight;
    _txtStreet.tag=2;
    [self.view addSubview:_txtStreet];
    _txtHouseNum=[[UITextField alloc]initWithFrame:CGRectMake(30,255,80, height)];
    _txtHouseNum.delegate=self;
    if(self.appDelegate.city.cityId==9)
    {
        _txtHouseNum.placeholder=@"מספר חדר/משרד";
        _txtStreet.placeholder=@"שם בניין";
        _txtHouseNum.font= [UIFont fontWithName:@"Helvetica" size:11];
    }
    else
        _txtHouseNum.placeholder=@"מס׳ בית";
    _txtHouseNum.textAlignment=NSTextAlignmentRight;
    [_txtHouseNum setKeyboardType:UIKeyboardTypeNumberPad];
    [self.view addSubview:_txtHouseNum];
    
    _txtCity=[[UITextField alloc]initWithFrame:CGRectMake(30,290,260, height)];
    _txtCity.textAlignment=NSTextAlignmentCenter;
    _txtCity.enabled=NO;
    [self.view addSubview:_txtCity];
    
    street=[[Street alloc]init];
    //AutocompleteTable
    autoCompleteTableView=[[UITableView alloc]init];
    autoCompleteTableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    [[autoCompleteTableView layer]setBorderWidth:0.7];
    autoCompleteTableView.delegate= self;
    autoCompleteTableView.dataSource= self;
    autoCompleteTableView.scrollEnabled= YES;
    autoCompleteTableView.hidden= YES;
    autoCompleteTableView.rowHeight= 20;
    [self.view addSubview:autoCompleteTableView];
    StreetListWrapper*streetListWrapper=[[StreetListWrapper alloc]init];
    streetListWrapper.clientId=self.appDelegate.city.cityId;
    streetListWrapper.cityPassword=self.appDelegate.cityContent.passwordToCrmcDB;
    [self.connection connectionToService:@"GetCRMCStreetList" jsonDictionary:[streetListWrapper parseStreetListWrapperToDict] controller:self withSelector:@selector(GetStreetListWrapperResult:)];
   //
     //           [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"residentId"];
    _txtCity.text=self.appDelegate.city.cityName;
    if(!([[NSUserDefaults standardUserDefaults]objectForKey:@"residentId"]==nil))
    {
        fill=YES;
        _txtFirstname.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"firstName"];
        _txtHouseNum.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"houseNumber"];
        _txtEmail.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"email"];
        _txtLastname.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"lastName"];
        _txtMobile.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"mobileNumber"];
        _txtPhone.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"phoneNumber"];
        
        if([[[NSUserDefaults standardUserDefaults] objectForKey:@"PushNotification"]isEqualToString:@"yes"])
            _isGetNotification=true;
    }
    if(_first)
    {
        UIAlertView*alert=[[UIAlertView alloc]initWithTitle:@"" message:@"האם ברצונך להרשם למערכת?" delegate:self cancelButtonTitle:@"לא" otherButtonTitles:@"כן", nil];
        fill=NO;
        [alert show];
    }
    [self setCompoments];
    
    //add button
    _btnIsPushNotification =[[UIButton alloc]init];
    _btnIsPushNotification.layer.frame= CGRectMake(30 , 332, 260, 30);
    _btnIsPushNotification.layer.cornerRadius=4;
    _btnIsPushNotification.titleLabel.font=[UIFont fontWithName:@"Helvetica" size:13];
    [_btnIsPushNotification setTitle:@"אני מעוניין/ת לקבל הודעות" forState:UIControlStateNormal];
    [_btnIsPushNotification addTarget:self action:@selector(btnIsPushNotificationClick:) forControlEvents:UIControlEventTouchUpInside];
    _btnIsPushNotification.backgroundColor=[UIColor grayColor ];
    [self.view addSubview:_btnIsPushNotification];
    
    _imgIsPushNotification=[[UIImageView alloc]initWithFrame:CGRectMake(264, 336, 22, 22)];
    _imgIsPushNotification.backgroundColor=[UIColor whiteColor];
    _imgIsPushNotification.layer.cornerRadius=4;
    if(_isGetNotification)
        _imgIsPushNotification.image=[UIImage imageNamed:@"vMap.png"];
    [self.view addSubview:_imgIsPushNotification];
}
-(void)setCompoments
{
    _txtFirstname.layer.borderColor=[UIColor colorWithRed:9.0f/255.0f green:134.0f/255.0f blue:134.0f/255.0f alpha:1.0f].CGColor;
    _txtFirstname.layer.borderWidth=1.8;
    _txtFirstname.backgroundColor=[UIColor whiteColor];
    _txtFirstname.layer.cornerRadius=5;
    _txtFirstname.layer.sublayerTransform = CATransform3DMakeTranslation(-6, 0, 0);
    
    _txtLastname.layer.borderColor=[UIColor colorWithRed:9.0f/255.0f green:134.0f/255.0f blue:134.0f/255.0f alpha:1.0f].CGColor;
    _txtLastname.layer.borderWidth=1.8;
    _txtLastname.backgroundColor=[UIColor whiteColor];
    _txtLastname.layer.cornerRadius=5;
    _txtLastname.layer.sublayerTransform = CATransform3DMakeTranslation(-6, 0, 0);
    
    _txtMobile.layer.borderColor=[UIColor colorWithRed:9.0f/255.0f green:134.0f/255.0f blue:134.0f/255.0f alpha:1.0f].CGColor;
    _txtMobile.layer.borderWidth=1.8;
    _txtMobile.backgroundColor=[UIColor whiteColor];
    _txtMobile.layer.cornerRadius=5;
    _txtMobile.layer.sublayerTransform = CATransform3DMakeTranslation(-6, 0, 0);
    
    _txtPhone.layer.borderColor=[UIColor colorWithRed:9.0f/255.0f green:134.0f/255.0f blue:134.0f/255.0f alpha:1.0f].CGColor;
    _txtPhone.layer.borderWidth=1.8;
    _txtPhone.backgroundColor=[UIColor whiteColor];
    _txtPhone.layer.cornerRadius=5;
    _txtPhone.layer.sublayerTransform = CATransform3DMakeTranslation(-6, 0, 0);
    
    _txtStreet.layer.borderColor=[UIColor colorWithRed:9.0f/255.0f green:134.0f/255.0f blue:134.0f/255.0f alpha:1.0f].CGColor;
    _txtStreet.layer.borderWidth=1.8;
    _txtStreet.backgroundColor=[UIColor whiteColor];
    _txtStreet.layer.cornerRadius=5;
    _txtStreet.layer.sublayerTransform = CATransform3DMakeTranslation(-6, 0, 0);
    
    _txtHouseNum.layer.borderColor=[UIColor colorWithRed:9.0f/255.0f green:134.0f/255.0f blue:134.0f/255.0f alpha:1.0f].CGColor;
    _txtHouseNum.layer.borderWidth=1.8;
    _txtHouseNum.backgroundColor=[UIColor whiteColor];
    _txtHouseNum.layer.cornerRadius=5;
    _txtHouseNum.layer.sublayerTransform = CATransform3DMakeTranslation(-6, 0, 0);
    
    _txtCity.layer.borderColor=[UIColor colorWithRed:9.0f/255.0f green:134.0f/255.0f blue:134.0f/255.0f alpha:1.0f].CGColor;
    _txtCity.layer.borderWidth=1.8;
    _txtCity.backgroundColor=[UIColor whiteColor];
    _txtCity.layer.cornerRadius=5;
    _txtCity.layer.sublayerTransform = CATransform3DMakeTranslation(-6, 0, 0);
    
    _txtEmail.layer.borderColor=[UIColor colorWithRed:9.0f/255.0f green:134.0f/255.0f blue:134.0f/255.0f alpha:1.0f].CGColor;
    _txtEmail.layer.borderWidth=1.8;
    _txtEmail.backgroundColor=[UIColor whiteColor];
    _txtEmail.layer.cornerRadius=5;
    _txtEmail.layer.sublayerTransform = CATransform3DMakeTranslation(-6, 0, 0);
    
    _btnOk=[[UIButton alloc]initWithFrame:CGRectMake(30, 373, 260, 34)];
    _btnOk.backgroundColor=[UIColor colorWithRed:186.0f/255.0f green:186.0f/255.0f blue:187.0f/255.0f alpha:1.0f];
    [_btnOk setTitleColor:[UIColor colorWithRed:9.0f/255.0f green:134.0f/255.0f blue:134.0f/255.0f alpha:1.0f] forState:normal];
    [_btnOk setTitle:@"אישור" forState:normal];
    [_btnOk addTarget:self action:@selector(btnOkClick:) forControlEvents:UIControlEventTouchUpInside];
    _btnOk.layer.cornerRadius=4;
    [self.view addSubview:_btnOk];
}
- (IBAction)btnIsPushNotificationClick:(id)sender
{
    if(_isGetNotification)
    {
        _isGetNotification=NO;
       _imgIsPushNotification.image=[UIImage imageNamed:@""];
        
    }
    else
    {
        _isGetNotification=YES;
        _imgIsPushNotification.image=[UIImage imageNamed:@"vMap.png"];
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if([alertView.message isEqualToString:@"פרטיך נקלטו בהצלחה"])
    {
        [UIView animateWithDuration:0.5
                              delay:.2f
                            options: UIViewAnimationOptionAllowAnimatedContent
                         animations:^{ [self.view setAlpha:0];
                         }
                         completion:^(BOOL finished){
                             [self.view removeFromSuperview];
                         }];
    }
    if([alertView.message isEqualToString:@"האם ברצונך להרשם למערכת?"])
    {
        if(buttonIndex==0)
            [UIView animateWithDuration:0.5
                                  delay:.2f
                                options: UIViewAnimationOptionAllowAnimatedContent
                             animations:^{ [self.view setAlpha:0];
                             }
                             completion:^(BOOL finished){
                                 [self.view removeFromSuperview];
                             }];
    }
}

-(void)GetStreetListWrapperResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    WSResult* wSResult=[[WSResult alloc]init];
    wSResult=[wSResult parseWSResultFromJson:result];
    if(![result isEqualToString:@""]&&![result isEqualToString:@"[]"])
    {
        WSResult* wSResult=[[WSResult alloc]init];
        wSResult=[wSResult parseWSResultFromJson:result];
        _listStreets=[[NSMutableArray alloc]initWithArray:wSResult.ResultObjects];
        if(fill)
        {
            fill=NO;
            street.streetId=(int)[[[NSUserDefaults standardUserDefaults] objectForKey:@"streetId"] integerValue];
            NSArray* arrStreet=[_listStreets filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.StreetID =[cd] %d",street.streetId]];
            street.streetName=((WSStreet*)arrStreet[0]).StreetName;
            _txtStreet.text=street.streetName;
        }
        filterStreet=[[NSMutableArray alloc]initWithArray:_listStreets];
    }
    else
        NSLog(@"Empty streets list");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if(textField.tag==2)
    {
        textField.text=street.streetName;
        filterStreet=_listStreets;
        autoCompleteTableView.hidden=YES;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    _txtStreet.text=street.streetName;
    [self OpenCloseKeyBord:1 Flag:NO];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(!flag)
        [self OpenCloseKeyBord:-1 Flag:YES];
    if (textField.tag==2)
    {
        _txtStreet.text=@"";
        if(filterStreet.count>1)
            autoCompleteTableView.frame = CGRectMake(_txtStreet.frame.origin.x , _txtStreet.frame.origin.y+_txtStreet.frame.size.height, _txtStreet.frame.size.width, 2*14+10);
        else
            autoCompleteTableView.frame = CGRectMake(_txtStreet.frame.origin.x , _txtStreet.frame.origin.y+_txtStreet.frame.size.height, _txtStreet.frame.size.width, 1*14+10);
        autoCompleteTableView.hidden= NO;
        [autoCompleteTableView reloadData];
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(flag)
    {
        _txtStreet.text=street.streetName;
        [self OpenCloseKeyBord:1 Flag:NO];
    }
}

-(void)OpenCloseKeyBord:(int)num Flag:(BOOL)openClose
{
    if(!openClose)
    {
        autoCompleteTableView.hidden= YES;
        filterStreet=[[NSMutableArray alloc]initWithArray:_listStreets];
        [self.view endEditing:YES];
    }
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+num*98, self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];
    flag=openClose;
}

#pragma mark UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return filterStreet.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"MyIdentifier"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.textLabel.text = ((WSStreet*)(filterStreet[indexPath.row])).StreetName;
    [cell.textLabel setFont:[UIFont systemFontOfSize:12]];
    cell.textLabel.textAlignment=NSTextAlignmentRight;
    cell.backgroundColor=[UIColor lightGrayColor];
    return cell;
}

-(void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell
    *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    _txtStreet.text=selectedCell.textLabel.text;
    street.streetId=((WSStreet*)filterStreet[indexPath.row]).StreetID;
    street.streetName=((WSStreet*)filterStreet[indexPath.row]).StreetName;
    [self OpenCloseKeyBord:1 Flag:NO];
}

-(BOOL)textField:(UITextField*)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.tag==1||textField. tag==2)
    {
        NSString*ns=[[NSString alloc]init];
        ns=[NSString stringWithFormat:@"%@%@",textField.text,string];
        if([string isEqualToString:@""])
            ns=[[ns substringToIndex:[ns length] - 1] substringFromIndex:0];
        if(textField.tag==2)
        {
            if([ns isEqualToString:@""])
                filterStreet=_listStreets;
            else
            {
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.StreetName contains[c] %@",ns];
                filterStreet = [NSMutableArray arrayWithArray:[_listStreets filteredArrayUsingPredicate:predicate]];
            }
            if(filterStreet.count>1)
                autoCompleteTableView.frame = CGRectMake(_txtStreet.frame.origin.x , _txtStreet.frame.origin.y+_txtStreet.frame.size.height, _txtStreet.frame.size.width, 2*14+10);
            else
                autoCompleteTableView.frame = CGRectMake(_txtStreet.frame.origin.x , _txtStreet.frame.origin.y+_txtStreet.frame.size.height, _txtStreet.frame.size.width, 1*14+10);
            if(filterStreet.count==0)
                autoCompleteTableView.hidden=YES;
            else
                autoCompleteTableView.hidden=NO;
        }
        [autoCompleteTableView reloadData];
        return YES;
    }
    return YES;
}

- (IBAction)btnOkClick:(id)sender
{
    NSString*st=[[NSUserDefaults standardUserDefaults]objectForKey:@"residentId"];
    if([self isEmpty]&&[self validationText:_txtFirstname.text]&&[self validationText:_txtLastname.text]&&[self validationText:_txtCity.text]&&[self validationNumber:_txtMobile.text]&&[self validationNumber:_txtPhone.text]&&[self validationNumber:_txtMobile.text]&&[self validationPhoneNumber:_txtPhone.text]&&[self validationMobileNumber:_txtMobile.text]&&[self validateEmailWithString:_txtEmail.text])
    {
        [self.generic showNativeActivityIndicator:self];
        if(!([[NSUserDefaults standardUserDefaults]objectForKey:@"residentId"]==nil))
        {
            int res=[[[NSUserDefaults standardUserDefaults] objectForKey:@"residentId"]integerValue];
            Resident* resident=[[Resident alloc]init:res FirstName: _txtFirstname.text LastName:_txtLastname.text Email:_txtEmail.text MobileNumber:_txtMobile.text PhoneNumber:_txtPhone.text HouseNumber:_txtHouseNum.text StreetId:street.streetId CityId:self.appDelegate.city.cityId IsPushNotification:_isGetNotification];
            [self.connection connectionToService:@"EditResidentDetails" jsonDictionary:[resident parseResidentToDict] controller:self withSelector:@selector(RegistNewResidentResult:)];
        }
        else
        {
            Resident* resident=[[Resident alloc]init: _txtFirstname.text LastName:_txtLastname.text Email:_txtEmail.text MobileNumber:_txtMobile.text PhoneNumber:_txtPhone.text HouseNumber:_txtHouseNum.text StreetId:street.streetId CityId:self.appDelegate.city.cityId IsPushNotification:_isGetNotification];
            [self.connection connectionToService:@"RegistNewResident" jsonDictionary:[resident parseResidentToDict] controller:self withSelector:@selector(RegistNewResidentResult:)];
        }
    }
    else
    {
        UIAlertView* alert=[[UIAlertView alloc]initWithTitle:@"" message:@"חלק משדות החובה חסרים או שתוכנם אינו חוקי. אנא מלאו ונסו שוב" delegate:self cancelButtonTitle:@"אישור" otherButtonTitles: nil];
        [alert show];
    }
}

-(void)RegistNewResidentResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if(![result isEqualToString:@"-2"]&&![result isEqualToString:@"false"])
    {
        if(![result isEqualToString:@"true"])
            [[NSUserDefaults standardUserDefaults]setObject:result forKey:@"residentId"];
        [[NSUserDefaults standardUserDefaults]setObject:_txtFirstname.text forKey:@"firstName"];
        [[NSUserDefaults standardUserDefaults]setObject:_txtLastname.text forKey:@"lastName"];
        [[NSUserDefaults standardUserDefaults]setObject:_txtEmail.text forKey:@"email"];
        [[NSUserDefaults standardUserDefaults]setObject:_txtMobile.text forKey:@"mobileNumber"];
        [[NSUserDefaults standardUserDefaults]setObject:_txtPhone.text forKey:@"phoneNumber"];
        [[NSUserDefaults standardUserDefaults]setObject:_txtHouseNum.text forKey:@"houseNumber"];
        [[NSUserDefaults standardUserDefaults]setInteger:street.streetId forKey:@"streetId"];
        if(_isGetNotification)
        [[NSUserDefaults standardUserDefaults]setObject:@"yes" forKey:@"PushNotification"];
        else
        [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"PushNotification"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        UIAlertView* alert=[[UIAlertView alloc]initWithTitle:@"" message:@"פרטיך נקלטו בהצלחה" delegate:self cancelButtonTitle:@"אישור" otherButtonTitles: nil];
        [alert show];
    }
    else
        NSLog(@"Empty streets list");
}

-(bool) isEmpty
{
    if([_txtFirstname.text isEqualToString:@""]||[_txtLastname.text isEqualToString:@""]||[_txtMobile.text isEqualToString:@""]||[_txtPhone.text isEqualToString:@""]||[_txtEmail.text isEqualToString:@""]||[_txtStreet.text isEqualToString:@""]||[_txtHouseNum.text isEqualToString:@""]||[_txtCity.text isEqualToString:@""])
    {
        return NO;
    }
    return YES;
}
-(BOOL)validationText:(NSString*)text
{
    NSString *myRegex = @"[A-Zא-תa-z ]*";
    NSPredicate *myTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", myRegex];
    NSString *string = text;
    BOOL valid = [myTest evaluateWithObject:string];
    return valid;
}
-(BOOL)validationNumber:(NSString*)text
{
    NSString *myRegex = @"[0-9-]*";
    NSPredicate *myTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", myRegex];
    NSString *string = text;
    BOOL valid = [myTest evaluateWithObject:string];
    return valid;
}
-(BOOL)validationPhoneNumber:(NSString*)phoneNumber
{
    if([phoneNumber isEqualToString:@""])
        return NO;
    NSString *phoneRegex = @"[0-9]{7}([0-9]{2,3})?";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    BOOL matches = [test evaluateWithObject:phoneNumber];
    return matches;
}
-(BOOL)validationMobileNumber:(NSString*)mobileNumber
{
    if([mobileNumber isEqualToString:@""])
        return NO;
    NSString *mobileRegex=@"[0]{1}[0-9]{9}";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileRegex];
    BOOL matches = [test evaluateWithObject:mobileNumber];
    return matches;
}
- (BOOL)validateEmailWithString:(NSString*)email
{
    if([email isEqualToString:@""])
        return YES;
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

@end