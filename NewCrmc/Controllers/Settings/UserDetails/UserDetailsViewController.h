//
//  UserDetailsViewController.h
//  NewCrmc
//
//  Created by WebitMacMini Two on 7/23/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "SubViewController.h"
#import "EmergencySubViewController.h"

@interface UserDetailsViewController : EmergencySubViewController <UIAlertViewDelegate>

@property (retain, nonatomic) UITextField *txtLastname;
@property (retain, nonatomic) UITextField *txtFirstname;
@property (retain, nonatomic) UITextField *txtMobile;
@property (retain, nonatomic) UITextField *txtPhone;
@property (retain, nonatomic) UITextField *txtEmail;
@property (retain, nonatomic) UITextField *txtStreet;
@property (retain, nonatomic) UITextField *txtHouseNum;
@property (retain, nonatomic) UITextField *txtCity;
@property (retain, nonatomic) UIImageView *imgIsPushNotification;
@property (retain, nonatomic) UIButton *btnIsPushNotification;
@property (retain, nonatomic) UIButton *btnOk;
@property (nonatomic,retain) NSArray *listStreets;
@property BOOL first;
@property BOOL isGetNotification;

@end
