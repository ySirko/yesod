//
//  EmergencySubViewController.m
//  NewCrmc
//
//  Created by MyMac on 8/7/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "EmergencySubViewController.h"

@interface EmergencySubViewController ()

@end

@implementation EmergencySubViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _connection=[[Connection alloc]init];
    _generic=[[Generic alloc]init];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    self.view.backgroundColor=[UIColor clearColor];
    
    //button to exit
    _btn=[[UIButton alloc ]initWithFrame:CGRectMake(20, 66, 30, 30)];
    _btn.titleLabel.font=[UIFont fontWithName:@"" size:17];
    [_btn setTitle:@"X" forState:UIControlStateNormal];
    [_btn addTarget:self action:@selector(X:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btn];
    [self.view sendSubviewToBack:_btn];
    
    //line down15, 60, 290, 400
	_lbl6=[[UILabel alloc]initWithFrame:CGRectMake(15, 415, 290, 6)];
    _lbl6.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:_lbl6];
    [self.view sendSubviewToBack:_lbl6];
    
    //icon25, 106, 270, 315
     _imgIcon=[[UIImageView alloc]initWithFrame:CGRectMake(270, 68, 30, 30)];
    [self.view addSubview:_imgIcon];
    
    //titleText
    _lbl5=[[UILabel alloc]initWithFrame:CGRectMake(75, 71, 185, 21)];
    _lbl5.font=[UIFont fontWithName:@"Helvetica-Bold" size:22];
    _lbl5.textAlignment = NSTextAlignmentRight;
    _lbl5.textColor=[UIColor colorWithRed:9.0f/255.0f green:134.0f/255.0f blue:134.0f/255.0f alpha:1.0f];
    [self.view addSubview:_lbl5];
    [self.view sendSubviewToBack:_lbl5];
    
    //title2
        _lbl4=[[UILabel alloc]initWithFrame:CGRectMake(15, 78, 290, 28)];
    _lbl4.backgroundColor=[UIColor colorWithRed:195.0f/255.0f green:195.0f/255.0f blue:195.0f/255.0f alpha:1.0f];
    [self.view addSubview:_lbl4];
    [self.view sendSubviewToBack:_lbl4];
    
    //title1
     _lbl3=[[UILabel alloc]initWithFrame:CGRectMake(15, 60, 290, 35)];
    _lbl3.backgroundColor=[UIColor colorWithRed:195.0f/255.0f green:195.0f/255.0f blue:195.0f/255.0f alpha:1.0f];
    _lbl3.layer.cornerRadius=5;
    [self.view addSubview:_lbl3];
    [self.view sendSubviewToBack:_lbl3];

    //imgBack
    _img=[[UIImageView alloc]initWithFrame:CGRectMake(15, 60, 290, 365)];
    _img.image=[UIImage imageNamed:@"Background.png"];
    _img.alpha=0.05;
    [self.view addSubview:_img];
    [self.view sendSubviewToBack:_img];

    //grayBackground
    _lbl2=[[UILabel alloc]initWithFrame:CGRectMake(15, 60, 290, 400)];
    _lbl2.backgroundColor=[UIColor colorWithRed:111.0f/255.0f green:113.0f/255.0f blue:121.0f/255.0f alpha:1.0f];
    _lbl2.layer.cornerRadius=5;
    [self.view addSubview:_lbl2];
    [self.view sendSubviewToBack:_lbl2];
    
    ///back alpha
    _lbl1=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 320, 560)];
    _lbl1.backgroundColor=[UIColor whiteColor];
    _lbl1.alpha=0.6;
    [self.view addSubview:_lbl1];
    [self.view sendSubviewToBack:_lbl1];
    
    switch (_index) {
        case 1:
            _lbl5.text=@"הגדרות";
            _imgIcon.image=[UIImage imageNamed:@"iconSetting.png"];
            break;
        case 2:
            _lbl5.text=@"פרטים אישיים";
            _imgIcon.image=[UIImage imageNamed:@"iconUserDetails.png"];
            break;
        case 3:
            _lbl5.text=@"תחומי עניין";
            _imgIcon.image=[UIImage imageNamed:@"iconInterestArea.png"];
            break;
        case 4:
            _lbl5.text=@"קבלת הודעות";
            _imgIcon.image=[UIImage imageNamed:@"iconMessage.png"];
            break;
        case 5:
            _lbl5.text=@"לחצן מצוקה";
            _imgIcon.image=[UIImage imageNamed:@"iconEmergency.png"];
        default:
            break;
    }
}

-(void)hideComponnets
{
    [_lbl1 setHidden:YES];
    [_lbl3 setHidden:YES];
    [_lbl4 setHidden:YES];
    [_lbl5 setHidden:YES];
    [_lbl6 setHidden:YES];
    [_img setHidden:YES];
    [_imgIcon setHidden:YES];
    _lbl2.layer.borderColor=[UIColor grayColor].CGColor;
    _lbl2.layer.borderWidth=4;
}
-(IBAction)X:(id)sender
{
    [UIView animateWithDuration: 0.5
                          delay:.2f
                        options: UIViewAnimationOptionAllowAnimatedContent
                     animations:^{ [self.view setAlpha:0];
}
                     completion:^(BOOL finished){
                        [self.view removeFromSuperview];
                     }];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
