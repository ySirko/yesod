//
//  setEmergencyNumbersViewController.m
//  NewCrmc
//
//  Created by MyMac on 8/10/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "setEmergencyNumbersViewController.h"

@interface setEmergencyNumbersViewController ()
{
    BOOL flag;
    UIAlertView*alert;
}
@end

@implementation setEmergencyNumbersViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _txtEmergencyNumber1=[[UITextField alloc]initWithFrame:CGRectMake(30, 161, 260, 50)];
    [self.view addSubview:_txtEmergencyNumber1];
    _txtEmergencyNumber2=[[UITextField alloc]initWithFrame:CGRectMake(30, 221, 260, 50)];
    [self.view addSubview:_txtEmergencyNumber2];
    _txtEmergencyNumber3=[[UITextField alloc]initWithFrame:CGRectMake(30, 281, 260, 50)];
    [self.view addSubview:_txtEmergencyNumber3];
    
//    if(!([[NSUserDefaults standardUserDefaults]objectForKey:@"residentId"]==nil))
//    {
        _txtEmergencyNumber1.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"EmergencyNumber1"];
        _txtEmergencyNumber2.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"EmergencyNumber2"];
        _txtEmergencyNumber3.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"EmergencyNumber3"];
   // }
    [self setCompoments];
}
-(void)setCompoments
{
    _lbl=[[UILabel alloc]initWithFrame:CGRectMake(60, 120, 225, 36)];
    _lbl.textColor=[UIColor whiteColor];
    _lbl.text=@"הגדר/י מספרים לשעת חירום:";
    _lbl.textAlignment=NSTextAlignmentRight;
    [self.view addSubview:_lbl];
    
    _txtEmergencyNumber1.delegate=self;
    [_txtEmergencyNumber1 addTarget:self action:@selector(txtClick:) forControlEvents:UIControlEventTouchUpInside];
    _txtEmergencyNumber1.layer.borderColor=[UIColor colorWithRed:9.0f/255.0f green:134.0f/255.0f blue:134.0f/255.0f alpha:1.0f].CGColor;
    _txtEmergencyNumber1.layer.borderWidth=1.8;
    _txtEmergencyNumber1.backgroundColor=[UIColor whiteColor];
    _txtEmergencyNumber1.layer.cornerRadius=5;
    _txtEmergencyNumber1.layer.sublayerTransform = CATransform3DMakeTranslation(12, 0, 0);
    
    _txtEmergencyNumber2.delegate=self;
    [_txtEmergencyNumber2 addTarget:self action:@selector(txtClick:) forControlEvents:UIControlEventTouchUpInside];
    _txtEmergencyNumber2.layer.borderColor=[UIColor colorWithRed:9.0f/255.0f green:134.0f/255.0f blue:134.0f/255.0f alpha:1.0f].CGColor;
    _txtEmergencyNumber2.layer.borderWidth=1.8;
    _txtEmergencyNumber2.backgroundColor=[UIColor whiteColor];
    _txtEmergencyNumber2.layer.cornerRadius=5;
    _txtEmergencyNumber2.layer.sublayerTransform = CATransform3DMakeTranslation(12, 0, 0);
    
    _txtEmergencyNumber3.delegate=self;
    [_txtEmergencyNumber3 addTarget:self action:@selector(txtClick:) forControlEvents:UIControlEventTouchUpInside];
    _txtEmergencyNumber3.layer.borderColor=[UIColor colorWithRed:9.0f/255.0f green:134.0f/255.0f blue:134.0f/255.0f alpha:1.0f].CGColor;
    _txtEmergencyNumber3.layer.borderWidth=1.8;
    _txtEmergencyNumber3.backgroundColor=[UIColor whiteColor];
    _txtEmergencyNumber3.layer.cornerRadius=5;
    _txtEmergencyNumber3.layer.sublayerTransform = CATransform3DMakeTranslation(12, 0, 0);

    _btnEmergencyNum=[[UIButton alloc]initWithFrame:CGRectMake(30, 351, 260, 45)];
    _btnEmergencyNum.backgroundColor=[UIColor colorWithRed:186.0f/255.0f green:186.0f/255.0f blue:187.0f/255.0f alpha:1.0f];
    [ _btnEmergencyNum setTitleColor: [UIColor colorWithRed:0.0f/255.0f green:138.0f/255.0f blue:135.0f/255.0f alpha:1.0f] forState:normal];
    [[_btnEmergencyNum layer]setCornerRadius:4];
    [_btnEmergencyNum setTitle:@"עדכן" forState:normal];
    [_btnEmergencyNum.titleLabel setFont:[UIFont systemFontOfSize:18]];
    [_btnEmergencyNum setTitleEdgeInsets:UIEdgeInsetsMake(0,4, 0, 0)];
    [_btnEmergencyNum addTarget:self action:@selector(btnEmergencyNumClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btnEmergencyNum];
}
-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    self.view.frame=CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+40, self.view.frame.size.width,self.view.frame.size.height);
    [UIView commitAnimations];
    flag=NO;
    [self.view endEditing:YES];
    return YES;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(flag)
    {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        self.view.frame=CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+40, self.view.frame.size.width,self.view.frame.size.height);
        [UIView commitAnimations];
        flag=NO;
    }
    [self.view endEditing:YES];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(!flag)
    {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    self.view.frame=CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-40, self.view.frame.size.width,self.view.frame.size.height);
    [UIView commitAnimations];
        flag=YES;
    }
}

- (IBAction)btnEmergencyNumClick:(id)sender
{
    if(([self validationMobileNumber:_txtEmergencyNumber1.text]||[self validationPhoneNumber:_txtEmergencyNumber1.text]||[_txtEmergencyNumber1.text isEqualToString:@""])&&([self validationMobileNumber:_txtEmergencyNumber2.text]||[self validationPhoneNumber:_txtEmergencyNumber2.text]||[_txtEmergencyNumber2.text isEqualToString:@""])&&([self validationMobileNumber:_txtEmergencyNumber3.text]||[self validationPhoneNumber:_txtEmergencyNumber3.text]||[_txtEmergencyNumber3.text isEqualToString:@""]))
    {
        [[NSUserDefaults standardUserDefaults]setObject:_txtEmergencyNumber1.text forKey:@"EmergencyNumber1"];
        [[NSUserDefaults standardUserDefaults]setObject:_txtEmergencyNumber2.text forKey:@"EmergencyNumber2"];
        [[NSUserDefaults standardUserDefaults]setObject:_txtEmergencyNumber3.text forKey:@"EmergencyNumber3"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [UIView animateWithDuration:0.5
                              delay:.2f
                            options: UIViewAnimationOptionAllowAnimatedContent
                         animations:^{ [self.view setAlpha:0];
                         }
                         completion:^(BOOL finished){
                             [self.view removeFromSuperview];
                         }];
    }
    else
    {
        alert=[[UIAlertView alloc]initWithTitle:@"" message:@"אחד או יותר מהשדות שהזנת אינו חוקי." delegate:self cancelButtonTitle:@"אישור" otherButtonTitles: nil];
        [alert show];
    }
}

- (IBAction)txtClick:(id)sender
{
    [self.view endEditing:YES];
}
-(BOOL)validationPhoneNumber:(NSString*)phoneNumber
{
    if([phoneNumber isEqualToString:@""])
        return NO;
    NSString *phoneRegex = @"[0-9]{7}([0-9]{2,3})?";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    BOOL matches = [test evaluateWithObject:phoneNumber];
    return matches;
}
-(BOOL)validationMobileNumber:(NSString*)mobileNumber
{
    if([mobileNumber isEqualToString:@""])
        return NO;
    NSString *mobileRegex=@"[0]{1}[0-9]{9}";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileRegex];
    BOOL matches = [test evaluateWithObject:mobileNumber];
    return matches;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}


-(void)OpenCloseKeyBord:(int)num Flag:(BOOL)openClose
{
        [self.view endEditing:YES];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
      [UIView commitAnimations];
    flag=openClose;
}

@end
