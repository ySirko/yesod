//
//  setEmergencyNumbersViewController.h
//  NewCrmc
//
//  Created by MyMac on 8/10/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "EmergencySubViewController.h"

@interface setEmergencyNumbersViewController : EmergencySubViewController<UITextFieldDelegate>
@property (retain, nonatomic) UILabel*lbl;
@property (retain, nonatomic) UITextField *txtEmergencyNumber1;
@property (retain, nonatomic) UITextField *txtEmergencyNumber2;
@property (retain, nonatomic) UITextField *txtEmergencyNumber3;
@property (retain, nonatomic) UIButton*btnEmergencyNum;
@end
