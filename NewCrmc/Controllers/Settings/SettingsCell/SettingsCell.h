//
//  SettingsCell.h
//  NewCrmc
//
//  Created by WebitMacMini Two on 7/22/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsCell : UITableViewCell
@property (retain, nonatomic) UILabel *lblBackgroung;
@property (retain, nonatomic) UILabel *lbl;
@property (retain, nonatomic) UIButton *btnSwitch;
@property (retain, nonatomic) UIButton *btnSwitchBack;
@property BOOL turnedOn;

@property (copy) void (^switchHandler)(BOOL switched);

@end
