//
//  SettingsCell.m
//  NewCrmc
//
//  Created by WebitMacMini Two on 7/22/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "SettingsCell.h"

@implementation SettingsCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setTurnedOn:(BOOL)turnedOn
{
    _turnedOn=turnedOn;
    if (_turnedOn)
    {
        _btnSwitch.backgroundColor=[UIColor colorWithRed:9.0f/255.0f green:134.0f/255.0f blue:134.0f/255.0f alpha:1.0f];
    }
    else
    {
        _btnSwitch.backgroundColor=[UIColor clearColor];
    }
    if (self.switchHandler) {
        self.switchHandler(_turnedOn);
    }
}

- (IBAction)btnSwichClick:(id)sender
{
    if (_turnedOn)
    {
        self.turnedOn = NO;
    }
    else
    {
        self.turnedOn = YES;
    }
}
@end
