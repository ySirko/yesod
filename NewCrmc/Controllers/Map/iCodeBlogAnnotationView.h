//
//  iCodeBlogAnnotationView.h
//  CRMC
//
//  Created by Lior Ronen on 5/5/14.
//  Copyright (c) 2014 neomi&batya&shuli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "iCodeBlogAnnotation.h"
@interface iCodeBlogAnnotationView : MKAnnotationView
@property (nonatomic, retain) UIImageView *imageView;
@end
