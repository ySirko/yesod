//
//  iCodeMapViewController.h
//  CRMC
//
//  Created by Lior Ronen on 5/5/14.
//  Copyright (c) 2014 neomi&batya&shuli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "iCodeBlogAnnotationView.h"
#import "iCodeBlogAnnotation.h"
#import "CategoryCell.h"
#import "AppDelegate.h"
#import "SubViewController.h"
#import "Categories.h"
@interface iCodeMapViewController : SubViewController<MKMapViewDelegate,UITableViewDataSource,UITableViewDelegate,CLLocationManagerDelegate>
@property (nonatomic, retain)  MKMapView *mapView;
@property(strong,nonatomic) UITableView* categoryTableView;
@property (nonatomic, retain) MKAnnotationView *selectedAnnotationView;
@property (nonatomic, retain)NSArray*listCategory;
@property (nonatomic, retain)NSMutableArray*listPoints;
@property (retain, nonatomic) UIButton *btnInerestPoint;
@property (retain, nonatomic)UIColor*color;
@property (retain, nonatomic)NSString*strColor;
@end
