//
//  iCodeBlogAnnotationView.m
//  CRMC
//
//  Created by Lior Ronen on 5/5/14.
//  Copyright (c) 2014 neomi&batya&shuli. All rights reserved.
//

#import "iCodeBlogAnnotationView.h"
#import "NSData+Base64.h"
@implementation iCodeBlogAnnotationView
#define kHeight 40
#define kWidth  17
#define kBorder 2

- (id)initWithAnnotation:(id <MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
	iCodeBlogAnnotation* myAnnotation = (iCodeBlogAnnotation*)annotation;
    self = [super initWithAnnotation:myAnnotation reuseIdentifier:reuseIdentifier];
    self.frame = CGRectMake(0, 0, kWidth, kHeight);
    self.backgroundColor = [UIColor clearColor];
    _imageView=[[UIImageView alloc] initWithImage:[UIImage imageWithData:myAnnotation.icon]];
    _imageView.frame = CGRectMake(0, 0, kWidth - 2 * kBorder, kWidth - 2 * kBorder);
    [self addSubview:_imageView];
	[_imageView setContentMode:UIViewContentModeScaleAspectFill];
	return self;
}

@end
