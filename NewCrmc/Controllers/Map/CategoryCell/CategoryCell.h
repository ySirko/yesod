//
//  CategoryCell.h
//  CRMC
//
//  Created by Lior Ronen on 5/5/14.
//  Copyright (c) 2014 neomi&batya&shuli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCodeBlogAnnotation.h"
@interface CategoryCell : UITableViewCell
@property (retain, nonatomic) UILabel *categoryName;
@property (retain, nonatomic) UIImageView *imagechecked;
@property (retain, nonatomic) UILabel *x_v;
-(void)setCategoryCell;

@end
