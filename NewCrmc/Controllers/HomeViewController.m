//
//  HomeViewController.m
//  NewCrmc
//
//  Created by WebitMacMini Two on 7/17/14.
//  Copyright (c) 2014 webit. All rights reserved.
//
#import "MenuSubViewController.h"
#import "SubViewController.h"
#import "HomeViewController.h"
#import "MapImageViewController.h"
@interface HomeViewController ()
{
    EmergencyViewController* emergencyview;
    NSArray*btnarray;
    NSArray*btnIconArray;
    UIViewController*view;
    CGRect*frameLayout;
    int yOfButton;//the  Yorigion of the buttons below
        NSString* DeviceName;
}
@end

@implementation HomeViewController

- (void)viewDidLoad
{
    self.screenName=@"HomeViewController";
    [super viewDidLoad];
    yOfButton=50;
    [self setDynamicObjects];
    self.bt.hidden=true;//כפתור רשת צר לא נמצא בתפריטים
    _backAlpha.textColor=[UIColor redColor];
    _listMenu =  self.appDelegate.menu;
    if(_listMenu.count>6)
    {
        CGRect frameRect =_images.frame;
        frameRect.size.height=frameRect.size.height+14;
        _images.frame=frameRect;
        frameRect=_backAlpha.frame;
        frameRect.origin.y=frameRect.origin.y-29;
        frameRect.size.height= frameRect.size.height+80;
        _backAlpha.frame=frameRect;
        for (int i=0; i<6; i++)
        {
            ((UIButton*)btnarray[i]).frame=[self moveUpIcon:((UIButton*)btnarray[i]).frame];
            ((UIImageView*)btnIconArray[i]).frame=[self moveUpIcon:((UIImageView*)btnIconArray[i]).frame];
        }
    }
    if(self.appDelegate.city.cityId==1 || self.appDelegate.city.cityId==18)
    {
        UIButton *btnPhones =[[UIButton alloc]initWithFrame:CGRectMake(208, self.view.frame.size.height-yOfButton, 86, 30)];
        btnPhones.backgroundColor=[UIColor clearColor];
        btnPhones.layer.borderColor=[[UIColor brownColor]CGColor];
        btnPhones.layer.borderWidth=2;
        [btnPhones setTitle:@"טלפונים" forState:normal];
        [btnPhones.titleLabel setFont:[UIFont systemFontOfSize:13]];
        [btnPhones setTitleColor:[UIColor brownColor] forState:UIControlStateNormal];

        [btnPhones addTarget:self action:@selector(btnPhonesClick:) forControlEvents:UIControlEventTouchUpInside];
        [[btnPhones layer]setCornerRadius:3];
        [self.view addSubview:btnPhones];
        
        UIButton *btnHallSide =[[UIButton alloc]initWithFrame:CGRectMake(24, self.view.frame.size.height-yOfButton, 86, 30)];

        btnHallSide.backgroundColor=[UIColor clearColor];
        btnHallSide.layer.borderColor=[[UIColor brownColor]CGColor];
        btnHallSide.layer.borderWidth=2;
        [btnHallSide setTitle:@"אתר המועצה" forState:normal];
        [btnHallSide.titleLabel setFont:[UIFont systemFontOfSize:13]];
        [btnHallSide setTitleColor:[UIColor brownColor] forState:UIControlStateNormal];

        [btnHallSide addTarget:self action:@selector(btnHallSideClick:) forControlEvents:UIControlEventTouchUpInside];
        [[btnHallSide layer]setCornerRadius:3];
        [self.view addSubview:btnHallSide];
        
        UIButton *btnFaceBook =[[UIButton alloc]initWithFrame:CGRectMake(116,self.view.frame.size.height-yOfButton, 86, 30)];

        btnFaceBook.backgroundColor=[UIColor clearColor];
        btnFaceBook.layer.borderColor=[[UIColor brownColor]CGColor];
        btnFaceBook.layer.borderWidth=2;
        [btnFaceBook setTitle:@"פייסבוק" forState:normal];
        [btnFaceBook.titleLabel setFont:[UIFont systemFontOfSize:13]];
        [btnFaceBook setTitleColor:[UIColor brownColor] forState:UIControlStateNormal];

        [btnFaceBook addTarget:self action:@selector(btnFaceBookClick:) forControlEvents:UIControlEventTouchUpInside];
        [[btnFaceBook layer]setCornerRadius:3];
        [self.view addSubview:btnFaceBook];
    }
    else if(self.appDelegate.city.cityId==10)
    {
        _btnMoked=[[UIButton alloc]initWithFrame:CGRectMake(20, self.view.frame.size.height-yOfButton, 134, 43)];
        _btnMoked.backgroundColor=[UIColor colorWithRed:39.0f/255.0f green:172.0f/255.0f blue:70.0f/255.0f alpha:1.0f];
        [_btnMoked setTitle:@"חיוג למועצה" forState:normal];
        [_btnMoked.titleLabel setFont:[UIFont systemFontOfSize:13]];
        [_btnMoked setTitleEdgeInsets:UIEdgeInsetsMake(0,15, 0, 0) ];
        [_btnMoked addTarget:self action:@selector(btnMohazaClick:) forControlEvents:UIControlEventTouchUpInside];
        [[_btnMoked layer]setCornerRadius:2];
        [self.view addSubview:_btnMoked];
        
        _btnEmergencyNum=[[UIButton alloc]initWithFrame:CGRectMake(166, self.view.frame.size.height-yOfButton, 134, 43)];
        _btnEmergencyNum.backgroundColor=[UIColor colorWithRed:237.0f/255.0f green:28.0f/255.0f blue:36.0f/255.0f alpha:1.0f];
        [[_btnEmergencyNum layer]setCornerRadius:2];
        [_btnEmergencyNum setTitleEdgeInsets:UIEdgeInsetsMake(0, 15, 0, 25) ];
        [_btnEmergencyNum setTitle:@"חיוג למוקד השירות לתושב" forState:normal];
        _btnEmergencyNum.titleLabel.numberOfLines=0;
        _btnEmergencyNum.titleLabel.textAlignment=NSTextAlignmentCenter;
        [_btnEmergencyNum.titleLabel setFont:[UIFont systemFontOfSize:13]];
        [_btnEmergencyNum addTarget:self action:@selector(btnMokedClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_btnEmergencyNum];
        UIImageView*imgMoked=[[UIImageView alloc]initWithFrame:CGRectMake(32, self.view.frame.size.height-yOfButton+6, 28,24)];
        imgMoked.image=[UIImage imageNamed:@"PhoneIcon.png"];
        [self.view addSubview:imgMoked];
        
        UIImageView*imgEmergency=[[UIImageView alloc]initWithFrame:CGRectMake(264, self.view.frame.size.height-yOfButton+6, 28, 24)];
        imgEmergency.image=[UIImage imageNamed:@"Emergency.png"];
        [self.view addSubview:imgEmergency];
    }
    else
    {
        _btnMoked=[[UIButton alloc]initWithFrame:CGRectMake(20, self.view.frame.size.height-yOfButton, 134, 43)];
        _btnMoked.backgroundColor=[UIColor colorWithRed:39.0f/255.0f green:172.0f/255.0f blue:70.0f/255.0f alpha:1.0f];
        [_btnMoked setTitle:@"חיוג למוקד" forState:normal];
        [_btnMoked.titleLabel setFont:[UIFont systemFontOfSize:13]];
        [_btnMoked setTitleEdgeInsets:UIEdgeInsetsMake(0,15, 0, 0) ];
        [_btnMoked addTarget:self action:@selector(btnMokedClick:) forControlEvents:UIControlEventTouchUpInside];
        [[_btnMoked layer]setCornerRadius:2];
        [self.view addSubview:_btnMoked];
        
        _btnEmergencyNum=[[UIButton alloc]initWithFrame:CGRectMake(166, self.view.frame.size.height-yOfButton, 134, 43)];
        _btnEmergencyNum.backgroundColor=[UIColor colorWithRed:237.0f/255.0f green:28.0f/255.0f blue:36.0f/255.0f alpha:1.0f];
        [[_btnEmergencyNum layer]setCornerRadius:2];
        [_btnEmergencyNum setTitleEdgeInsets:UIEdgeInsetsMake(0, 15, 0, 25) ];
        [_btnEmergencyNum setTitle:@"לחצן מצוקה" forState:normal];
        _btnEmergencyNum.titleLabel.numberOfLines=0;
        _btnEmergencyNum.titleLabel.textAlignment=NSTextAlignmentCenter;
        [_btnEmergencyNum.titleLabel setFont:[UIFont systemFontOfSize:13]];
        [_btnEmergencyNum addTarget:self action:@selector(btnEmergencyNumClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_btnEmergencyNum];
        UIImageView*imgMoked=[[UIImageView alloc]initWithFrame:CGRectMake(32, self.view.frame.size.height-yOfButton+6, 28,24)];
        imgMoked.image=[UIImage imageNamed:@"PhoneIcon.png"];
        [self.view addSubview:imgMoked];
        
        UIImageView*imgEmergency=[[UIImageView alloc]initWithFrame:CGRectMake(264, self.view.frame.size.height-yOfButton+6, 28, 24)];
        imgEmergency.image=[UIImage imageNamed:@"Emergency.png"];
        [self.view addSubview:imgEmergency];
    }
    for (int i=0; i<_listMenu.count; i++)//create menu button
    {
        UIButton*btn=[[UIButton alloc]init];
        btn=(UIButton*)btnarray[i];
        Menu*menuItem=[[Menu alloc]init];
        menuItem=_listMenu[i];
        btn.backgroundColor=menuItem.color;
        [btn.titleLabel setFont:[UIFont systemFontOfSize:13.5]];
        btn.tag=menuItem.ID;
        btn.hidden=NO;
        [btn setTitle:menuItem.name forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(TouchDown:) forControlEvents:UIControlEventTouchDown];// עושה highlightedהלחיצה
        [btn addTarget:self action:@selector(TouchUpOutside:) forControlEvents:UIControlEventTouchUpOutside];//touch
        ((UIImageView*) btnIconArray[i]).image=menuItem.icon;
    }
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"residentId"]==nil) {
        emergencyview=[[UserDetailsViewController alloc]init];
        ((UserDetailsViewController*)emergencyview).first=YES;
        [self.view addSubview:emergencyview.view];
    }
    
    if (self.appDelegate.openFromPush) {
        if (self.appDelegate.city.cityId == 5 || self.appDelegate.city.cityId == 9) {
            [self btnClick:_btn4];
        }
        if (self.appDelegate.city.cityId == 1) {
            [self btnClick:_btn6];
        }
        self.appDelegate.openFromPush = NO;
    }
    _btn1.exclusiveTouch = YES;
    _btn2.exclusiveTouch = YES;
    _btn3.exclusiveTouch = YES;
    _btn4.exclusiveTouch = YES;
    _btn5.exclusiveTouch = YES;
    _btn6.exclusiveTouch = YES;
    _btn7.exclusiveTouch = YES;
    _btn8.exclusiveTouch = YES;
    _btn9.exclusiveTouch = YES;

}

-(void)TouchDown:(UIButton*)sender
{
    NSArray* filterArray;
    filterArray = [self.listMenu filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"ID =[cd] %d",((UIButton*) sender).tag]];
    sender.backgroundColor=((Menu*)filterArray[0]).HighlightedColor;
}

-(void)TouchUpOutside:(UIButton*)sender
{
    NSArray* filterArray = [_listMenu filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"ID =[cd] %d",((UIButton*) sender).tag]];
    sender.backgroundColor=((Menu*)filterArray[0]).HighlightedColor;
}

- (IBAction)btnMokedClick:(id)sender
{
    self.flag=YES;
    if(self.appDelegate.city.cityId==9)//עיריות שדרשו טלפון חירום מסוים
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:089349106"]];
    else if (self.appDelegate.city.cityId==10)
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:046389746"]];
    else if (self.appDelegate.city.cityId==13)
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:039723090"]];
    else
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:106"]];
    
}
- (IBAction)btnMohazaClick:(id)sender
{
    self.flag=YES;
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:046186400"]];
}
- (IBAction)btnEmergencyNumClick:(id)sender
{
    if(self.appDelegate.city.cityId==9)//לא רצו שיפתח מספרי חירום
    {
        self.flag=YES;
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:089342999"]];
    }
    else//פותח מס׳ חירום
    {
        emergencyview=[[EmergencyViewController alloc]init];
        emergencyview.index=5;
        [emergencyview.view setAlpha:0];
        [self.view addSubview:emergencyview.view];
        [UIView animateWithDuration:0.8
                              delay:.2f
                            options: UIViewAnimationOptionLayoutSubviews
                         animations:^{
                             [emergencyview.view setAlpha:1];
                         }
                         completion:^(BOOL finished){}];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)btnClick:(id)sender
{

    self.flag=YES;
    NSArray* filterArray = [_listMenu filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"ID =[cd] %d",((UIButton*) sender).tag]];
    Menu*m=filterArray[0];
    ((UIButton*) sender).backgroundColor=m.color;// להחזיר צבע מהאיליטד לרגיל
    switch (m.active)
    {
        case 1:
            switch (m.tag)
            {
                case 1://אירועים
                case 2://חדשות ועדכונים
                case 3://הודעות עבורך
                case 4://מסלולים
                case 5://אתרי מורשת,תיירות
                case 14://גופי סמך
                case 15://מבלים בעיר //spent in town
                    view=[[EventsTableViewController alloc]init];
                    ((EventsTableViewController*)view).index=m.tag;
                    ((EventsTableViewController*)view).color=m.color;
                    ((EventsTableViewController*)view).highlightedColor=m.HighlightedColor;
                    ((EventsTableViewController*)view).chetz=m.strColor;
                    ((EventsTableViewController*)view).highlightedChetz=[NSString stringWithFormat:@"%@Highlighted",m.strColor];
                    ((EventsTableViewController*)view).header=m.name;
                    break;
                case 6://טלפונים//phone numbers
                    view=[[CouncilOfficialsViewController alloc]init];
                    ((CouncilOfficialsViewController*)view).color=m.color;
                    ((CouncilOfficialsViewController*)view).strColor=m.strColor;
                    ((CouncilOfficialsViewController*)view).header=m.name;
                    break;
                case 7://מפה//map
                    view=[[iCodeMapViewController alloc]init];
                    ((iCodeMapViewController*)view).color=m.color;
                    ((iCodeMapViewController*)view).header=m.name;
                    ((iCodeMapViewController*)view).strColor=m.strColor;
                    break;
                case 8://פניה חדשה//new "face"
                    view=[[NewAppealViewController alloc]init];
                    ((NewAppealViewController*)view).color=m.color;
                    ((NewAppealViewController*)view).header=m.name;
                    break;
                case 9://הפניות שלי//my references
                    view=[[MyAppealTableViewController alloc]init];
                    ((MyAppealTableViewController*)view).color=m.color;
                    ((MyAppealTableViewController*)view).header=m.name;
                    break;
                case 10://דבר ראש העיר
                case 11://אודות העיר
                case 12://שעת חירום//emeregency
                    view=[[MayorViewController alloc]init];
                    if(m.tag==10)
                    {
                        [self.generic showNativeActivityIndicator:self];
                        [self.connection connectionToService:@"GetMayorImage" jsonDictionary:[self.appDelegate.city parseCityToDict] controller:self withSelector:@selector(GetMayorImageResult:)];
                    }
                    if(self.appDelegate.city.cityId == 5 && m.tag == 11)
                    {
                        ((MayorViewController*)view).imgMayor=[[UIImageView alloc]initWithFrame:CGRectMake(200, 10, 110, 84)];
                        ((MayorViewController*)view).imgMayor.image=[UIImage imageNamed:@"imageCity.jpg"];
                        ((MayorViewController*)view).txtview.textContainer.exclusionPaths  = @[[UIBezierPath bezierPathWithRect:CGRectMake(((MayorViewController*)view).txtview.frame.size.width-((MayorViewController*)view).imgMayor.frame.size.width+10,0, ((MayorViewController*)view).imgMayor.frame.size.width+10, ((MayorViewController*)view).imgMayor.frame.size.height)]];
                        [((MayorViewController*)view).txtview addSubview:((MayorViewController*)view).imgMayor];
                    }
                    ((MayorViewController*)view).index=m.tag;
                    ((MayorViewController*)view).color=m.color;
                    ((MayorViewController*)view).header=m.name;
                    break;
                case 13://סקרים//completed
                    view=[[SurveyViewController alloc]init];
                    ((SurveyViewController*)view).color=m.color;
                    ((SurveyViewController*)view).header=m.name;
                    break;
                case 16://מפת חדרה//hadera map
                    view=[[MapImageViewController alloc]init];
                    ((MapImageViewController*)view).color=m.color;
                    ((MapImageViewController*)view).header=m.name;
                    
                    break;
                default:
                    break;
            }
            [self.navigationController pushViewController:view animated:YES];
            break;
        case 2:
            view=[[WebViewController alloc]init];
            ((WebViewController*)view).nsurl=[[NSString alloc]init];
            ((WebViewController*)view).nsurl=m.link;
            ((WebViewController*)view).header=m.name;
            ((WebViewController*)view).color=m.color;
            [self.navigationController pushViewController:view animated:YES];
            break;
        case 3:
            view=[[MenuSubViewController alloc]init];
            ((MenuSubViewController*)view).listMenu=m.listMenu;
            ((MenuSubViewController*)view).color=m.color;
            ((MenuSubViewController*)view).header=m.name;
            [self.navigationController pushViewController:view animated:YES];
            break;
        case 4:
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:m.phone]];
            break;
        case 5:
        {
            MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
            [mc setToRecipients:[NSArray arrayWithObject:m.email]];
            [mc setSubject:m.emailSubject];
            mc.mailComposeDelegate=self;
            [self presentViewController:mc animated:YES completion:NULL];
            break;
        }
            break;
        default:
            break;
    }
}

-(CGRect)moveUpIcon:(CGRect)frame
{
    return CGRectMake(frame.origin.x, frame.origin.y-36, frame.size.width, frame.size.height);
}
// 3  הפונקציות האלה רק לחדרה 
- (IBAction)btnPhonesClick:(id)sender
{
    view=[[CouncilOfficialsViewController alloc]init];
    ((CouncilOfficialsViewController*)view).color=self.appManager.purple;
    ((CouncilOfficialsViewController*)view).strColor=@"purple";
    ((CouncilOfficialsViewController*)view).header=@"טלפונים";
    [self.navigationController pushViewController:view animated:YES];
}
- (IBAction)btnHallSideClick:(id)sender
{
    view=[[WebViewController alloc]init];
    ((WebViewController*)view).nsurl=[[NSString alloc]init];
    ((WebViewController*)view).nsurl=@"http://www.yesud-hamaala.muni.il";
    ((WebViewController*)view).header=@"אתר העירייה";
    ((WebViewController*)view).color=self.appManager.purple;
    [self.navigationController pushViewController:view animated:YES];
}
- (IBAction)btnFaceBookClick:(id)sender
{
    view=[[WebViewController alloc]init];
    ((WebViewController*)view).nsurl=[[NSString alloc]init];
    ((WebViewController*)view).nsurl=self.appDelegate.cityContent.facebook;
    ((WebViewController*)view).header=@"פייסבוק";
    ((WebViewController*)view).color=self.appManager.purple;
    [self.navigationController pushViewController:view animated:YES];
}
-(void)setDynamicObjects
{
    if(self.appDelegate.back)
    {
        self.background.image=self.appDelegate.back;
        self.background.alpha=1;
    }
    
    _btn1=[[UIButton alloc]init];
    _btn1.layer.frame= CGRectMake(208 , 180, 86, 86);
    _btn1.titleLabel.textAlignment=NSTextAlignmentCenter;
    _btn1.layer.cornerRadius=4;
    _btn1.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [_btn1 setTitle:@"" forState:UIControlStateNormal];
    [_btn1 setHidden:YES];
    [_btn1 addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    _btn1.tag=5;
    [_btn1 setTitleEdgeInsets:UIEdgeInsetsMake(50,0, 0, 0)];
    
    [self.view addSubview:_btn1];
    
    _btn2=[[UIButton alloc]init];
    _btn2.layer.frame= CGRectMake(116 , 180, 86, 86);
    _btn2.titleLabel.textAlignment=NSTextAlignmentRight;
    _btn2.layer.cornerRadius=4;
    [_btn2 setTitle:@"" forState:UIControlStateNormal];
    [_btn2 setHidden:YES];
    [_btn2 addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    _btn2.tag=5;
    [_btn2 setTitleEdgeInsets:UIEdgeInsetsMake(50,0, 0, 0)];
    [self.view addSubview:_btn2];
    
    _btn3=[[UIButton alloc]init];
    _btn3.layer.frame= CGRectMake(24 , 180, 86, 86);
    _btn3.layer.cornerRadius=4;
    _btn3.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _btn3.titleLabel.textAlignment = NSTextAlignmentCenter;
    [_btn3 setTitle:@"" forState:UIControlStateNormal];
    [_btn3 setHidden:YES];
    [_btn3 addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    _btn3.tag=5;
    [_btn3 setTitleEdgeInsets:UIEdgeInsetsMake(50,0, 0, 0)];
    [self.view addSubview:_btn3];
    
    _btn4=[[UIButton alloc]init];
    _btn4.layer.frame= CGRectMake(208 , 271, 86, 86);
    _btn4.titleLabel.textAlignment=NSTextAlignmentRight;
    _btn4.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _btn4.titleLabel.textAlignment = NSTextAlignmentCenter;
    _btn4.layer.cornerRadius=4;
    [_btn4 setTitle:@"" forState:UIControlStateNormal];
    [_btn4 setHidden:YES];
    [_btn4 addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    _btn4.tag=5;
    [_btn4 setTitleEdgeInsets:UIEdgeInsetsMake(50,0, 0, 0)];
    [self.view addSubview:_btn4];
    
    _btn5=[[UIButton alloc]init];
    _btn5.layer.frame= CGRectMake(116 , 271, 86, 86);
    _btn5.titleLabel.textAlignment=NSTextAlignmentRight;
    _btn5.layer.cornerRadius=4;
    [_btn5 setTitle:@"" forState:UIControlStateNormal];
    [_btn5 setHidden:YES];
    [_btn5 addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    _btn5.tag=5;
    [_btn5 setTitleEdgeInsets:UIEdgeInsetsMake(50,0, 0, 0)];
    [self.view addSubview:_btn5];
    
    _btn6=[[UIButton alloc]init];
    _btn6.layer.frame= CGRectMake(24 , 271, 86, 86);
    _btn6.titleLabel.textAlignment=NSTextAlignmentRight;
    _btn6.layer.cornerRadius=4;
    [_btn6 setTitle:@"" forState:UIControlStateNormal];
    [_btn6 setHidden:YES];
    [_btn6 addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    _btn6.tag=5;
    [_btn6 setTitleEdgeInsets:UIEdgeInsetsMake(50,0, 0, 0)];
    [self.view addSubview:_btn6];
    
    _btn7=[[UIButton alloc]init];
    _btn7.layer.frame= CGRectMake(208 , 327, 86, 86);
    _btn7.titleLabel.textAlignment=NSTextAlignmentRight;
    _btn7.layer.cornerRadius=4;
    [_btn7 setTitle:@"" forState:UIControlStateNormal];
    [_btn7 setHidden:YES];
    [_btn7 addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    _btn7.tag=5;
    [_btn7 setTitleEdgeInsets:UIEdgeInsetsMake(50,0, 0, 0)];
    [self.view addSubview:_btn7];
    
    _btn8=[[UIButton alloc]init];
    _btn8.layer.frame= CGRectMake(116 , 327, 86, 86);
    _btn8.titleLabel.textAlignment=NSTextAlignmentRight;
    _btn8.layer.cornerRadius=4;
    [_btn8 setTitle:@"" forState:UIControlStateNormal];
    [_btn8 setHidden:YES];
    [_btn8 addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    _btn8.tag=5;
    [_btn8 setTitleEdgeInsets:UIEdgeInsetsMake(50,0, 0, 0)];
    [self.view addSubview:_btn8];
    
    _btn9=[[UIButton alloc]init];
    _btn9.layer.frame= CGRectMake(24 , 327, 86, 86);
    _btn9.titleLabel.textAlignment=NSTextAlignmentRight;
    _btn9.layer.cornerRadius=4;
    [_btn9 setTitle:@"" forState:UIControlStateNormal];
    [_btn9 setHidden:YES];
    [_btn9 addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    _btn9.tag=5;
    [_btn9 setTitleEdgeInsets:UIEdgeInsetsMake(50,0, 0, 0)];
    [self.view addSubview:_btn9];
    btnarray=[[NSArray alloc]initWithObjects:_btn1,_btn2,_btn3,_btn4,_btn5,_btn6,_btn7,_btn8,_btn9, nil];
    
    //add icon Buttons
    _btnIcon1=[[UIImageView alloc]initWithFrame:CGRectMake(237, 204, 30, 30)];
    _btnIcon1.image=[UIImage imageNamed:@""];
    [self.view addSubview:_btnIcon1];
    
    _btnIcon2=[[UIImageView alloc]initWithFrame:CGRectMake(145, 204, 30, 30)];
    _btnIcon2.image=[UIImage imageNamed:@""];
    [self.view addSubview:_btnIcon2];
    
    _btnIcon3=[[UIImageView alloc]initWithFrame:CGRectMake(52, 204, 30, 30)];
    _btnIcon3.image=[UIImage imageNamed:@""];
    [self.view addSubview: _btnIcon3];
    
    
    _btnIcon4=[[UIImageView alloc]initWithFrame:CGRectMake(237, 295, 30, 30)];
    _btnIcon4.image=[UIImage imageNamed:@""];
    [self.view addSubview:_btnIcon4];
    
    
    _btnIcon5=[[UIImageView alloc]initWithFrame:CGRectMake(145, 295, 30, 30)];
    _btnIcon5.image=[UIImage imageNamed:@""];
    [self.view addSubview:_btnIcon5];
    
    _btnIcon6=[[UIImageView alloc]initWithFrame:CGRectMake(52, 295, 30, 30)];
    _btnIcon6.image=[UIImage imageNamed:@""];
    [self.view addSubview: _btnIcon6];
    
    _btnIcon7=[[UIImageView alloc]initWithFrame:CGRectMake(237, 351, 30, 30)];
    _btnIcon7.image=[UIImage imageNamed:@""];
    [self.view addSubview:_btnIcon7];
    
    _btnIcon8=[[UIImageView alloc]initWithFrame:CGRectMake(145,351, 30, 30)];
    _btnIcon8.frame=CGRectMake(145,351, 30, 30);
    _btnIcon8.image=[UIImage imageNamed:@""];
    [self.view addSubview:_btnIcon8];
    
    _btnIcon9=[[UIImageView alloc]initWithFrame:CGRectMake(52,351, 30, 30)];
    _btnIcon9.image=[UIImage imageNamed:@""];
    [self.view addSubview: _btnIcon9];
    [self setFrame];
    btnIconArray=[[NSArray alloc]initWithObjects:_btnIcon1,_btnIcon2,_btnIcon3,_btnIcon4,_btnIcon5,_btnIcon6,_btnIcon7,_btnIcon8,_btnIcon9, nil];
}
-(void)setFrame
{
    int constraintLeftSide=27;
    int constraintLeftSideIcon=constraintLeftSide+28;
    int constraintRightSide=-111;
    int constraintRightSideIcon=constraintRightSide+28;
    int widthButton=86;
    int widthIcon=30;
    int w=self.view.frame.size.width+constraintRightSide;
    int x=constraintLeftSide+widthButton;
        int space=((w-x)-widthButton)/2;
   // int space=5;
    int wIcon=self.view.frame.size.width+constraintRightSideIcon;
    int xIcon=constraintLeftSideIcon+widthIcon;
    int spaceIcon=((wIcon-xIcon)-widthIcon)/2;
    int yButton=180;
    int spaceInButton=23;
    
    _btn1.layer.frame= CGRectMake(self.view.frame.size.width+constraintRightSide , yButton, widthButton, widthButton);
    _btn2.layer.frame= CGRectMake(constraintLeftSide+widthButton+space , yButton, 86, 86);
    _btn3.layer.frame= CGRectMake(constraintLeftSide , yButton, 86, 86);
    _btn4.layer.frame= CGRectMake(self.view.frame.size.width+constraintRightSide ,yButton+widthButton+space,widthButton, widthButton);
    _btn5.layer.frame= CGRectMake(constraintLeftSide+widthButton+space , yButton+widthButton+space,widthButton,widthButton);
    _btn6.layer.frame= CGRectMake(constraintLeftSide , yButton+widthButton+space,widthButton,widthButton);
    _btn7.layer.frame= CGRectMake(self.view.frame.size.width+constraintRightSide , yButton+((widthButton+space)*2),widthButton, widthButton);
    _btn8.layer.frame= CGRectMake(constraintLeftSide+widthButton+space , yButton+((widthButton+space)*2),widthButton,widthButton);
    _btn9.layer.frame= CGRectMake(constraintLeftSide ,  yButton+((widthButton+space)*2),widthButton,widthButton);
    
    
    _btnIcon1.frame=CGRectMake(self.view.frame.size.width+constraintRightSideIcon,yButton+spaceInButton,widthIcon, widthIcon);
    _btnIcon2.frame=CGRectMake(constraintLeftSideIcon+widthIcon+spaceIcon,yButton+spaceInButton, widthIcon, widthIcon);
    _btnIcon3.frame=CGRectMake(constraintLeftSideIcon,yButton+spaceInButton, widthIcon, widthIcon);
    _btnIcon4.frame=CGRectMake(self.view.frame.size.width+constraintRightSideIcon,yButton+widthButton+space+spaceInButton, widthIcon, widthIcon);
    _btnIcon5.frame=CGRectMake(constraintLeftSideIcon+widthIcon+spaceIcon,yButton+widthButton+space+spaceInButton, widthIcon, widthIcon);
    _btnIcon6.frame=CGRectMake(constraintLeftSideIcon,yButton+widthButton+space+spaceInButton, widthIcon, widthIcon);
    _btnIcon7.frame=CGRectMake(self.view.frame.size.width+constraintRightSideIcon,yButton+((widthButton+space)*2)+spaceInButton, widthIcon, widthIcon);
    _btnIcon8.frame=CGRectMake(constraintLeftSideIcon+widthIcon+spaceIcon,yButton+((widthButton+space)*2)+spaceInButton, widthIcon, widthIcon);
    _btnIcon9.frame=CGRectMake(constraintLeftSideIcon,yButton+((widthButton+space)*2)+spaceInButton, widthIcon, widthIcon);
    
    
    
    
    
}
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
