//
//  EventDetailsViewController.m
//  CRMC
//
//  Created by WebitMacMini Two on 6/18/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import "EventDetailsViewController.h"
#import "Event.h"
#import "WebViewController.h"
@interface EventDetailsViewController ()

@end

@implementation EventDetailsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setDynamicObject];
    self.lblBackgroundTitle.backgroundColor=_color;
    self.Title.text=self.header;
    _lblSubTitle.text=_event.title;
    _lblAddress.text=_event.address;
    _lblNots.text=_event.notes;
    _lblPrice.text=_event.price;
    _lblDate.text=_event.eventDate;
    _lblDescription.text=_event.descriptionn;
    _lblSubTitle.lineBreakMode = NSLineBreakByWordWrapping;
    _lblSubTitle.numberOfLines=2;
    _lblNots.numberOfLines=2;
    if(_index==2||_index==3)
    {
        _lblStrAdderss.hidden=YES;
        _lblPrice.hidden=YES;
        _lblStrNote.hidden=YES;
        _btnLink.hidden=YES;
        _lblStrPrice.hidden=YES;
        _lblStrDescription.frame = CGRectMake(240, 255, 39, 21);
        _lblDescription.frame=CGRectMake(_lblDescription.frame.origin.x+10, 250, _lblDescription.frame.size.width, _lblDescription.frame.size.height+150);
        [_lblDescription setUserInteractionEnabled:NO];
    }
    if(_index==13)
    {
        
        _lblPrice.hidden=YES;
        _lblStrNote.hidden=YES;
        _lblStrPrice.hidden=YES;
        _lblStrAdderss.hidden=YES;
       
    }
}

- (IBAction)btnLinkClick:(id)sender
{
    WebViewController* view=[[WebViewController alloc]init];
    ((WebViewController*)view).nsurl=[[NSString alloc]init];
    ((WebViewController*)view).nsurl=_event.link;
    ((WebViewController*)view).color=_color;
    [self.navigationController pushViewController:view animated:YES];
}
-(void)setDynamicObject
{
    //add subTitle Label
    _lblSubTitle=[[UILabel alloc]initWithFrame:CGRectMake(0, 178, 320, 44)];
    _lblSubTitle.font=[UIFont fontWithName:@"Helvetica-Bold" size:17];
    [_lblSubTitle setTextAlignment:NSTextAlignmentCenter];
    _lblSubTitle.layer.sublayerTransform = CATransform3DMakeTranslation(-8, 0, 0);
    [self.view addSubview:_lblSubTitle];
    
    _lblStrDate=[[UILabel alloc]initWithFrame:CGRectMake(240, 233, 39, 21)];
    _lblStrDate.font=[UIFont fontWithName:@"Helvetica-Bold" size:13];
    [_lblStrDate setText:@"מועד:"];
    [self.view addSubview:_lblStrDate];
    
    _lblStrAdderss=[[UILabel alloc]initWithFrame:CGRectMake(232, 262, 47, 21)];
    _lblStrAdderss.font=[UIFont fontWithName:@"Helvetica-Bold" size:13];
    [_lblStrAdderss setText:@"כתובת:"];
    [self.view addSubview:_lblStrAdderss];
    
    if(self.appDelegate.city.cityId==13)
    {
        _webDescription = [[UIWebView alloc] initWithFrame:CGRectMake(20, 287, 210, 70)];
        _webDescription.backgroundColor=[UIColor clearColor];
        [_webDescription loadHTMLString:_event.descriptionn baseURL:nil];
        [self.view addSubview:_webDescription];
    }
    
    _lblStrDescription=[[UILabel alloc]initWithFrame:CGRectMake(240, 293, 39, 21)];
    _lblStrDescription.font=[UIFont fontWithName:@"Helvetica-Bold" size:13];
    [_lblStrDescription setText:@"תאור:"];
    [self.view addSubview:_lblStrDescription];
    
    _lblStrPrice=[[UILabel alloc]initWithFrame:CGRectMake(240, 364, 39, 21)];
    _lblStrPrice.font=[UIFont fontWithName:@"Helvetica-Bold" size:13];
    [_lblStrPrice setText:@"מחיר:"];
    if (self.appDelegate.city.cityId == 18) {

    }
    else{
    [self.view addSubview:_lblStrPrice];
    }
    _lblStrNote=[[UILabel alloc]initWithFrame:CGRectMake(232, 393, 47, 21)];
    _lblStrNote.font=[UIFont fontWithName:@"Helvetica-Bold" size:13];
    [_lblStrNote setText:@"הערות:"];
    [self.view addSubview:_lblStrNote];
    
    _lblDate=[[UILabel alloc]initWithFrame:CGRectMake(20, 235, 214, 21)];
    _lblDate.font=[UIFont fontWithName:@"Helvetica" size:13];
    [_lblDate setTextAlignment:NSTextAlignmentRight];
    _lblDate.layer.sublayerTransform = CATransform3DMakeTranslation(-8, 0, 0);
    
    [self.view addSubview:_lblDate];
    
    _lblAddress=[[UILabel alloc]initWithFrame:CGRectMake(20, 262, 210, 21)];
    _lblAddress.font=[UIFont fontWithName:@"Helvetica" size:13];
    [_lblAddress setTextAlignment:NSTextAlignmentRight];
    _lblAddress.layer.sublayerTransform = CATransform3DMakeTranslation(-8, 0, 0);
    
    [self.view addSubview:_lblAddress];
    if(self.appDelegate.city.cityId==13)
    {
        ////not see the webview
        UIWebView *webView = [[UIWebView alloc] initWithFrame:_lblDescription.frame];
        
        //make the background transparent
        [webView setBackgroundColor:[UIColor clearColor]];
        
        //pass the string to the webview
        [webView loadHTMLString:[_event.descriptionn description] baseURL:nil];
        
        //add it to the subview
        [self.view addSubview:webView];
    }
    else{
    _lblDescription=[[UITextView alloc]initWithFrame:CGRectMake(20, 287, 210, 70)];
    _lblDescription.font=[UIFont fontWithName:@"Helvetica" size:13];
    [_lblDescription setTextAlignment:NSTextAlignmentRight];
   _lblDescription.backgroundColor=[UIColor clearColor];
    [self.view addSubview:_lblDescription];
    }
    
    _lblPrice=[[UILabel alloc]initWithFrame:CGRectMake(20, 366, 214, 21)];
    _lblPrice.font=[UIFont fontWithName:@"Helvetica" size:13];
    [_lblPrice setTextAlignment:NSTextAlignmentRight];
    _lblPrice.layer.sublayerTransform = CATransform3DMakeTranslation(-8, 0, 0);
    [self.view addSubview:_lblPrice];
    
    _lblNots=[[UILabel alloc]initWithFrame:CGRectMake(20, 393, 214, 21)];
    _lblNots.font=[UIFont fontWithName:@"Helvetica" size:13];
    [_lblNots setTextAlignment:NSTextAlignmentRight];
    _lblNots.layer.sublayerTransform = CATransform3DMakeTranslation(-8, 0, 0);
    [self.view addSubview:_lblNots];
    
    //add button
    _btnLink =[[UIHyperlinksButton alloc]init];
    _btnLink.layer.frame= CGRectMake(163 , 417, 116, 30);
    [_btnLink.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13.0]];
    [_btnLink setTitle:@"לצפיה במידע נוסף >>" forState:UIControlStateNormal];
    [_btnLink addTarget:self action:@selector(btnLinkClick:) forControlEvents:UIControlEventTouchUpInside];
    _btnLink.titleLabel.textColor=self.appManager.lightBlue;
    [_btnLink setTitleColor:self.appManager.lightBlue forState:normal];
    [_btnLink setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    if (![_event.link isEqualToString:@""]) {
        [self.view addSubview:_btnLink];
    }
    
    //add up line label
    UILabel *lblUpLine=[[UILabel alloc]initWithFrame:CGRectMake(0, 157, 320, 6)];
    lblUpLine.backgroundColor=_color;
    [self.view addSubview:lblUpLine];
    ///add down line label
    UILabel *lblDownLine=[[UILabel alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-6, 320, 6)];
    lblDownLine.backgroundColor=_color;
    [self.view addSubview:lblDownLine];
}


@end
