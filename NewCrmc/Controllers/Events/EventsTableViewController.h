//
//  EventsTableViewController.h
//  CRMC
//
//  Created by WebitMacMini Two on 6/15/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import "EventCell.h"
#import "Event.h"
#import "SubViewController.h"
@interface EventsTableViewController : SubViewController<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) UITableView *tableViewEvents;
@property (retain, nonatomic) NSMutableArray* listEvents;
@property (retain, nonatomic) NSMutableArray* cellHeights;
@property (strong, nonatomic) UILabel *lblUpLine;
@property (strong, nonatomic) UILabel *lblDownLine;
@property (strong, nonatomic) UIColor *color;
@property (strong, nonatomic) UIColor *highlightedColor;
@property (retain, nonatomic) NSString *chetz;
@property (retain, nonatomic) NSString *highlightedChetz;
@property int index;
@property (retain, nonatomic) NSString* link;
@end
