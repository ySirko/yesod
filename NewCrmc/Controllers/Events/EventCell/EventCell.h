//
//  EventCell.h
//  CRMC
//
//  Created by WebitMacMini Two on 6/15/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"

@interface EventCell : UITableViewCell

@property (strong, nonatomic) UILabel *lblTitle;
@property (strong, nonatomic) UILabel *lblDate;
@property (strong, nonatomic) UILabel *lblAddress;
@property (strong, nonatomic) UIButton *imgOpen;
@property BOOL isHighlighted;
-(void)setEventCell:(Event*)event;
@property (strong, nonatomic)  UILabel *line;
@property (strong,nonatomic) UIColor*color;
@property (strong,nonatomic) UIColor*highlightedColor;
@property (retain,nonatomic) NSString*chetz;
@property (retain,nonatomic) NSString*highlightedChetz;
@property int index;

-(void)setDynamicObject;
-(void)setEventRssCell:(Event*)event;
-(void)setSemechDynamicObject;
-(void)setSemechCell:(Event*)event;

-(float)heightForThisCell;

@end

