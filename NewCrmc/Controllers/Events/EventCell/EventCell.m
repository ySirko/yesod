//
//  EventCell.m
//  CRMC
//
//  Created by WebitMacMini Two on 6/15/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import "EventCell.h"

@implementation EventCell

-(void)setEventCell:(Event*)event
{
    [self setDynamicObject];
    _lblTitle.text=event.title;
    if(!(event.eventDate ==[NSNull null]))
        _lblDate.text=[NSString stringWithFormat:@"תאריך: %@",event.eventDate];
    if(![event.address isEqualToString:@""] )
            _lblAddress.text=[NSString stringWithFormat:@"מקום: %@",event.address];
}
-(void)setEventRssCell:(Event*)event
{
    [self setDynamicObject];
    _lblTitle.text=event.title;
    _lblDate.text=event.eventDate;
}
- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    if(_isHighlighted)
    {
        if(highlighted)
        {
            _imgOpen.imageView.image=[UIImage imageNamed:_highlightedChetz];
            _line.backgroundColor=_highlightedColor;
        }
        else
        {
            _imgOpen.imageView.image=[UIImage imageNamed:_chetz];
            _line.backgroundColor=_color;
        }
    }
}
-(void)setDynamicObject
{
    //add top Label
    _lblTitle=[[UILabel alloc]init];
    _lblTitle.frame=CGRectMake(34 , 0, 275, 15);
    _lblTitle.font=[UIFont fontWithName:@"Helvetica-Bold" size:13];
    _lblTitle.numberOfLines=3;
    [_lblTitle setTextAlignment:NSTextAlignmentRight];
    [self addSubview:_lblTitle];
    
    if(_index!=13&&_index!=14)
    {
        _lblDate=[[UILabel alloc]initWithFrame:CGRectMake(34, 15, 275, 15)];
        _lblDate.font=[UIFont fontWithName:@"Helvetica" size:12];
        [_lblDate setTextAlignment:NSTextAlignmentRight];
        [self addSubview:_lblDate];
        
        _lblAddress=[[UILabel alloc]initWithFrame:CGRectMake(34, 28, 275, 15)];
        _lblAddress.font=[UIFont fontWithName:@"Helvetica" size:12];
        [_lblAddress setTextAlignment:NSTextAlignmentRight];
        [self addSubview:_lblAddress];
    }
    if (_index!=14)
    {
        //add button navigation
        _imgOpen=[[UIButton alloc]initWithFrame:CGRectMake(14, 11, 17, 26)];
        UIImage *btnImage = [UIImage imageNamed:_chetz];
        [_imgOpen setImage:btnImage forState:UIControlStateNormal];
        [_imgOpen setUserInteractionEnabled:NO];
        [self addSubview:_imgOpen];
        
//        _imgOpen=[[UIButton alloc]initWithFrame:CGRectMake(14, 11, 17, 26)];
//        UIImage *btnImage = [[UIImage imageNamed:_chetz]
//                             imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//        [_imgOpen setImage:btnImage forState:UIControlStateNormal];
//        [_imgOpen setUserInteractionEnabled:NO];
//        _imgOpen.tintColor = [UIColor greenColor];
//        [self addSubview:_imgOpen];
    }
    ///add buttom label
    _line=[[UILabel alloc]initWithFrame:CGRectMake(0, 50, 320, 4)];
    _line.backgroundColor=_color;
    [self addSubview:_line];
    
}
-(void)setSemechDynamicObject
{
    //add top Label
    _lblTitle=[[UILabel alloc]init];
    _lblTitle.frame=CGRectMake(34 , 0, 275, 15);
    _lblTitle.font=[UIFont fontWithName:@"Helvetica-Bold" size:13];
    [_lblTitle setTextAlignment:NSTextAlignmentRight];
    [self addSubview:_lblTitle];
    _imgOpen=[[UIButton alloc]initWithFrame:CGRectMake(14, 11, 17, 26)];
    UIImage *btnImage = [UIImage imageNamed:_chetz];
    [_imgOpen setImage:btnImage forState:UIControlStateNormal];
    [_imgOpen setUserInteractionEnabled:NO];
    [self addSubview:_imgOpen];
    
    ///add buttom label
    _line=[[UILabel alloc]initWithFrame:CGRectMake(0, 50, 320, 4)];
    _line.backgroundColor=_color;
    [self addSubview:_line];
    
}
-(void)setSemechCell:(Event*)event
{
    [self setSemechDynamicObject];
    _lblTitle.text=event.title;
    //_lblDate.text=[NSString stringWithFormat:@"תאריך: %@",event.eventDate];
    if(![event.address isEqual: [NSNull null]])
    {
        _lblAddress.text=[NSString stringWithFormat:@"מקום: %@",event.address];
    }
}

-(float)heightForThisCell
{
    //title label size
    UIFont *titleFont = [UIFont fontWithName:@"Helvetica-Bold" size:13];
    CGRect titleLabelRect = [_lblTitle.text boundingRectWithSize:CGSizeMake(275, 1000)
                                                         options:NSStringDrawingUsesLineFragmentOrigin
                                                      attributes:@{NSFontAttributeName:titleFont}
                                                         context:nil];
    
    UIFont *textFont = [UIFont fontWithName:@"Helvetica" size:12];
    CGRect textLabelRect = [_lblDate.text boundingRectWithSize:CGSizeMake(275, 1000)
                                                         options:NSStringDrawingUsesLineFragmentOrigin
                                                      attributes:@{NSFontAttributeName:textFont}
                                                         context:nil];
    NSLog(@"title:%f %f, textHeight: %f %f",titleLabelRect.size.width,titleLabelRect.size.height,textLabelRect.size.width,textLabelRect.size.height);

    CGRect titleRect=_lblTitle.frame;
    titleRect.size.height=titleLabelRect.size.height;
    _lblTitle.frame=titleRect;
    
    CGRect textRect = _lblDate.frame;
    textRect.origin.y =_lblTitle.frame.origin.y+_lblTitle.frame.size.height+5;
    textRect.size.height = textLabelRect.size.height;
    _lblDate.frame = textRect;
    
    float height = titleLabelRect.size.height+textLabelRect.size.height+10+5;
    if (height<60)
    {
        height=60;
    }

    CGRect lineFrame=_line.frame;
    lineFrame.origin.y=height-10;
    _line.frame=lineFrame;
    
    _imgOpen.center=CGPointMake(_imgOpen.frame.origin.x+_imgOpen.frame.size.width/2, height/2-5);
    
    return height;
}


@end
