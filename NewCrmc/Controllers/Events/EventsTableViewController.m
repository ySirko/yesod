  //
//  EventsTableViewController.m
//  CRMC
//
//  Created by WebitMacMini Two on 6/15/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import "EventsTableViewController.h"
#import "EventDetailsViewController.h"
#import "News.h"
#import "HikingViewController.h"
#import "Trip.h"
#import "Message.h"
#import "Resident.h"

@interface EventsTableViewController ()
{
    NSXMLParser *parser;
    NSMutableArray *feeds;
    NSMutableDictionary *item;
    NSMutableString *title;
    NSMutableString *link;
    NSMutableString *description;
    NSMutableString *pubDate;
    NSString *element;
    Resident* resident;
}
@end

@implementation EventsTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setDynamicObject];
    [self.generic showNativeActivityIndicator:self];
    self.lblBackgroundTitle.backgroundColor=self.color;
    self.Title.text=self.header;
    _listEvents=[[NSMutableArray alloc]init];
    NSMutableDictionary*dic=[[NSMutableDictionary alloc]init];
    NSMutableDictionary*dict=[[NSMutableDictionary alloc]init];
    switch (_index)
    {
        case 1:
            if(self.appDelegate.city.cityId==13)
            {
                feeds = [[NSMutableArray alloc] init];
                NSURL *url = [NSURL URLWithString:@"http://www.shoham.muni.il/RSS/Events.asp?CategoryID=141"];
                parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
                [parser setDelegate:self];
                [parser setShouldResolveExternalEntities:NO];
                [parser parse];
                [self.generic hideNativeActivityIndicator:self];
            }
            else
            {
                [dic setObject:@"general" forKey:@"typeName"];// ארועים
                [dic setObject:[NSString stringWithFormat:@"%d",self.appDelegate.city.cityId] forKey:@"cityId"];
                [dict setObject:dic forKey:@"type"];
                [self.connection connectionToService:@"GetEventsByCityAndTypeId" jsonDictionary:dict controller:self withSelector:@selector(GetEventsByCityIdResult:)];
            }
            break;
        case 2:
            if(self.appDelegate.city.cityId==13)
            {
                feeds = [[NSMutableArray alloc] init];
                NSURL *url = [NSURL URLWithString:@"http://www.shoham.muni.il/RSS/Articles.asp?CategoryID=167"];
                parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
                [parser setDelegate:self];
                [parser setShouldResolveExternalEntities:NO];
                [parser parse];
                [self.generic hideNativeActivityIndicator:self];
            }
            else
                if(self.appDelegate.city.cityId==1 || self.appDelegate.city.cityId==18)
                {
                    [dic setObject:@"news" forKey:@"typeName"];// ארועים
                    [dic setObject:[NSString stringWithFormat:@"%d",self.appDelegate.city.cityId] forKey:@"cityId"];
                    [dict setObject:dic forKey:@"type"];
                    [self.connection connectionToService:@"GetEventsByCityAndTypeId" jsonDictionary:dict controller:self withSelector:@selector(GetEventsByCityIdResult:)];
  
                }
//            else
//            [self.connection connectionToService:@"GetCityNews" jsonDictionary:[self.appDelegate.city parseCityToDict] controller:self withSelector:@selector(GetCityNewsResult:)];
            break;
        case 3:
        {
            //mess
            resident=[[Resident alloc]init];
            NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
            if(!([defaults objectForKey:@"residentId"]==nil))
            {
                resident.residentId=(int)[[[NSUserDefaults standardUserDefaults] objectForKey:@"residentId"] integerValue];
                [self.connection connectionToService:@"GetMessagesToResident" jsonDictionary:[resident parseResidentIdToDict] controller:self withSelector:@selector(GetMessagessResult:)];
            }
            else{
                    [self.generic hideNativeActivityIndicator:self];
                UIAlertView*alert=[[UIAlertView alloc]initWithTitle:@"" message:@"אינך רשום למערכת לכן אינך מקבל הודעות" delegate:self cancelButtonTitle:@"אישור"otherButtonTitles: nil];
                alert.delegate=self;
                [alert show];
                break;

  
            }
            break;
        }
        case 4:
            [dic setObject:@"general2" forKey:@"typeName"];
            [dic setObject:[NSString stringWithFormat:@"%d",self.appDelegate.city.cityId] forKey:@"cityId"];
            [dict setObject:dic forKey:@"type"];
            
            [self.connection connectionToService:@"GetTripsByCityId" jsonDictionary:dict controller:self withSelector:@selector(GetTripsByCityIdResult:)];
            break;
        case 5:
            [dic setObject:@"moreshet" forKey:@"typeName"];
            [dic setObject:[NSString stringWithFormat:@"%d",self.appDelegate.city.cityId] forKey:@"cityId"];
            [dict setObject:dic forKey:@"type"];
            [self.connection connectionToService:@"GetEventsByCityAndTypeId" jsonDictionary:dict controller:self withSelector:@selector(GetEventsByCityIdResult:)];
            break;
//        case 6:
//            [dic setObject:@"groups" forKey:@"eventType"];
//            [dic setObject:[NSString stringWithFormat:@"%d",self.appDelegate.city.cityId] forKey:@"cityId"];
//            [dict setObject:dic forKey:@"type"];
//            [self.connection connectionToService:@"GetEventsByCityAndTypeIdTask" jsonDictionary:dict controller:self withSelector:@selector(GetEventsByCityIdResult:)];
//        case 7:
//            [dic setObject:@"general" forKey:@"eventType"];
//            [dic setObject:[NSString stringWithFormat:@"%d",self.appDelegate.city.cityId] forKey:@"cityId"];
//            [dict setObject:dic forKey:@"type"];
//            [self.connection connectionToService:@"GetEventsByCityAndTypeIdTask" jsonDictionary:dict controller:self withSelector:@selector(GetEventsByCityIdResult:)];
//            break;
        case 14:
            [dic setObject:@"semech" forKey:@"typeName"];
            [dic setObject:[NSString stringWithFormat:@"%d",self.appDelegate.city.cityId] forKey:@"cityId"];
            [dict setObject:dic forKey:@"type"];
            [self.connection connectionToService:@"GetEventsByCityAndTypeId" jsonDictionary:dict controller:self withSelector:@selector(GetEventsByCityIdResult:)];

            break;
         
        case 15:
            [dic setObject:@"general2" forKey:@"typeName"];
            [dic setObject:[NSString stringWithFormat:@"%d",self.appDelegate.city.cityId] forKey:@"cityId"];
            [dict setObject:dic forKey:@"type"];
            [self.connection connectionToService:@"GetEventsByCityAndTypeId" jsonDictionary:dict controller:self withSelector:@selector(GetEventsByCityIdResult:)];
            break;
            
        case 18:
            [dic setObject:@"groups" forKey:@"typeName"];// ארועים
            [dic setObject:[NSString stringWithFormat:@"%d",self.appDelegate.city.cityId] forKey:@"cityId"];
            [dict setObject:dic forKey:@"type"];
            [self.connection connectionToService:@"GetEventsByCityAndTypeId" jsonDictionary:dict controller:self withSelector:@selector(GetEventsByCityIdResult:)];
            break;
            
        case 19:
            if(self.appDelegate.city.cityId==13)
            {
                feeds = [[NSMutableArray alloc] init];
                NSURL *url = [NSURL URLWithString:@"http://www.shoham.muni.il/RSS/Articles.asp?CategoryID=167"];
                parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
                [parser setDelegate:self];
                [parser setShouldResolveExternalEntities:NO];
                [parser parse];
                [self.generic hideNativeActivityIndicator:self];            }
            
            
        default:
        case 21:
        {//
            [dic setObject:@"emergency" forKey:@"typeName"];// חירום
            [dic setObject:[NSString stringWithFormat:@"%d",self.appDelegate.city.cityId] forKey:@"cityId"];
            [dict setObject:dic forKey:@"type"];
            [self.connection connectionToService:@"GetEventsByCityAndTypeId" jsonDictionary:dict controller:self withSelector:@selector(GetEventsByCityIdResult:)];
        }
            break;
    }
    _tableViewEvents.backgroundColor =[UIColor clearColor];
    _tableViewEvents.separatorColor=[UIColor clearColor];
    _cellHeights = [NSMutableArray new];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if([alertView.message isEqualToString:@"אינך רשום למערכת לכן אינך מקבל הודעות"])
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)GetMessagessResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if(![result isEqualToString:@""]&&![result isEqualToString:@"[]"])
    {
        _listEvents=[[NSMutableArray alloc]init];
        Message*message=[[Message alloc]init];
        _listEvents=[message parseListMessageFromJson:result];
        _listEvents = [NSMutableArray arrayWithArray:[[_listEvents reverseObjectEnumerator] allObjects]];
        [_tableViewEvents reloadData];
    }
}
-(void)GetEventsByCityIdResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if(![result isEqualToString:@""]&&![result isEqualToString:@"[]"])
    {
        _listEvents=[[NSMutableArray alloc]init];
        Event*event=[[Event alloc]init];
        _listEvents=[event parseListEventFromJson:result];
        if (_index == 14) {
            _listEvents = [NSMutableArray arrayWithArray:[_listEvents sortedArrayUsingComparator:^NSComparisonResult(Event *obj1, Event *obj2) {
                NSString *first = obj1.title;
                NSString *second = obj2.title;
                
                return [first compare:second];
            }]];
        } else if (_index != 21 ){
            _listEvents = [NSMutableArray arrayWithArray:[_listEvents sortedArrayUsingComparator:^NSComparisonResult(Event *obj1, Event *obj2) {
                
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
                
                NSString *first = obj1.eventDate;
                NSString *second = obj2.eventDate;
//                NSDate *dateFirst = [dateFormat dateFromString:first];
//                NSDate *dateSecond = [dateFormat dateFromString:second];
                
                NSDate *dateFirst;
                NSDate *dateSecond;
                
                if (first != (NSString *)[NSNull null])
                {
                    dateFirst = [dateFormat dateFromString:first];
                }
                
                if (second != (NSString *)[NSNull null])
                {
                    dateSecond = [dateFormat dateFromString:second];
                }
                
                return [dateSecond compare:dateFirst];
            }]];
        }
        [_tableViewEvents reloadData];
    }
    else
        NSLog(@"Empty events list");
}

-(void)GetTripsByCityIdResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if(![result isEqualToString:@""]&&![result isEqualToString:@"[]"])
    {
        _listEvents=[[NSMutableArray alloc]init];
        Trip *trip=[[Trip alloc]init];
        _listEvents=[trip parseListTripFromJson:result];
        [_cellHeights removeAllObjects];
        for (int i=0; i<_listEvents.count; i++)
        {
            [_cellHeights addObject:@60];
        }
         [_tableViewEvents reloadData];
    }
    else
        NSLog(@"Empty events list");
}

-(void)GetCityNewsResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if(![result isEqualToString:@""]&&![result isEqualToString:@"[]"])
    {
        _listEvents=[[NSMutableArray alloc]init];
        News *news=[[News alloc]init];
        _listEvents=[news parseListNewsFromJson:result];
        [_tableViewEvents reloadData];
    }
    else
        NSLog(@"empty list");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_cellHeights.count>indexPath.row)
    {
        return [[_cellHeights objectAtIndex:indexPath.row]floatValue];
    }
    else
    {
        return 60;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listEvents.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    EventCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell = [[EventCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell.selectionStyle= UITableViewCellSelectionStyleNone;
    cell.color=[[UIColor alloc]init];
    cell.color=_color;
    cell.chetz=_chetz;
    cell.highlightedChetz=_highlightedChetz;
    cell.highlightedColor=_highlightedColor;
    switch (_index)
    {
        case 1:
        case 14:
        case 15:
        case 18:
        case 19:
        case 5:
        case 21:
        case 2:
        {
            Event *event;
            
            event = [_listEvents objectAtIndex:indexPath.row];
            
            if (_index==14)
            {
                cell.index=14;
                [cell setSemechCell:event];
            }
            else
            {
                if(self.appDelegate.city.cityId==13)
                    [cell setEventRssCell:event];
                else{
                    if (self.appDelegate.city.cityId == 18) {
                        [cell setEventCell:event];
                        cell.isHighlighted=YES;
//                        [cell.lblDate setHidden:YES];
//                        [cell.lblDate removeFromSuperview];

                    } else {
                    [cell setEventCell:event];
                cell.isHighlighted=YES;
                    }
                }
            }
        }
            break;
            //        case 2:
            //        {
            //            News *news;
            //        // [cell setDynamicObject];
            //            news = [_listEvents objectAtIndex:indexPath.row];
            //           if(self.appDelegate.city.cityId==13)
            //               cell.index=13;
            //
            //            [cell setDynamicObject];
            //
            //            cell.lblTitle.text=news.title;
            //            cell.lblDate.text=news.content;
            //            cell.lblDate.numberOfLines=2;
            //            cell.lblDate.frame=CGRectMake(cell.lblDate.frame.origin.x,cell.lblDate.frame.origin.y,cell.lblDate.frame.size.width, 35);
            //            cell.isHighlighted=YES;
            //        }
            break;
        case 3:
        {
            Message *message;
            [cell setDynamicObject];
            message = [_listEvents objectAtIndex:indexPath.row];
            cell.lblTitle.text=message.message;
            //            cell.lblDate.text=news.content;
            //            cell.lblDate.numberOfLines=2;
            //            cell.lblDate.frame=CGRectMake(cell.lblDate.frame.origin.x,cell.lblDate.frame.origin.y,cell.lblDate.frame.size.width, 35);
            //            cell.isHighlighted=YES;
        }
            break;
        case 4:
        {
            Trip*trip;
            trip=[_listEvents objectAtIndex:indexPath.row];
            [cell setDynamicObject];
            cell.lblTitle.text=trip.nvTripName;
            cell.lblDate.text=trip.nvDescription;
            cell.lblDate.frame=CGRectMake(cell.lblDate.frame.origin.x,cell.lblDate.frame.origin.y,cell.lblDate.frame.size.width, 35);
            cell.lblDate.numberOfLines=0;
            cell.isHighlighted=YES;
            [self.cellHeights replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithFloat:[cell heightForThisCell]]];
        }
            break;
            
        default:
            break;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(_index==4)
    {
        HikingViewController*view=[[HikingViewController alloc]init];
        view.color=_color;
        view.trip=[_listEvents objectAtIndex:indexPath.row];
        view.header=view.trip.nvTripName;
        [self.navigationController pushViewController:view animated:YES];
    }
    else{
        if(_index==14||_index==21||_index==18)
        {
            Event*e=[_listEvents objectAtIndex:indexPath.row];
            
            WebViewController* view=[[WebViewController alloc]init];
            ((WebViewController*)view).nsurl=[[NSString alloc]init];
            ((WebViewController*)view).nsurl=e.link;
            ((WebViewController*)view).color=_color;
            [self.navigationController pushViewController:view animated:YES];
            
        }
        else
        {
            EventDetailsViewController *view=[[EventDetailsViewController alloc]init];
            view.event=[[Event alloc]init];
            view.color=_color;
            view.index=_index;
            if(_index==1||_index==5||_index==15||_index==15||_index==2)
            {
                if(self.appDelegate.city.cityId==13)
                    view.index=13;
                view.event=[_listEvents objectAtIndex:indexPath.row];
            }
            else
            {
                if ([_listEvents[indexPath.row] isKindOfClass:[Message class]])
                {
                    Message *m=_listEvents[indexPath.row];
                    view.event.descriptionn=m.message;
                    view.event.title=@"הודעה";
                }
                else if([_listEvents[indexPath.row] isKindOfClass:[News class]])
                {
                    view.event.descriptionn=((News*)_listEvents[indexPath.row]).content;
                    view.event.title=((News*)_listEvents[indexPath.row]).title;
                }
            }
            view.header=self.header;
            [self.navigationController pushViewController:view animated:YES];
        }
    }
}

-(void)setDynamicObject
{
    _lblUpLine=[[UILabel alloc]initWithFrame:CGRectMake(0, 152, 320, 6)];
    _lblUpLine.backgroundColor=_color;
    [self.view addSubview:_lblUpLine];
    
    _tableViewEvents=[[UITableView alloc]initWithFrame:CGRectMake(0, 163, 320, self.view.frame.size.height-163)];
    _tableViewEvents.delegate=self;
    _tableViewEvents.dataSource=self;
    [self.view addSubview:_tableViewEvents];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    element = elementName;
    if ([element isEqualToString:@"item"])
    {
        item    = [[NSMutableDictionary alloc] init];
        title   = [[NSMutableString alloc] init];
        link    = [[NSMutableString alloc] init];
        description=[[NSMutableString alloc] init];
        pubDate=[[NSMutableString alloc] init];
    }
}
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"item"])
    {
        [item setObject:title forKey:@"title"];
        [item setObject:link forKey:@"link"];
        [item setObject:description forKey:@"description"];
        [item setObject:pubDate forKey:@"pubDate"];
        if(_index==1)
        {
            Event*event=[[Event alloc]init];
            event=[event parseEventFromDictFeeds:item];
            [_listEvents addObject:event];
        }
        if(_index==2)
        {
            News*event=[[News alloc]init];
            event=[event parseNewsFromDictFeeds:item];
            [_listEvents addObject:event];
        }
    }
}
    
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if ([element isEqualToString:@"title"])
        [title appendString:string];
    else if([element isEqualToString:@"link"])
        [link appendString:string];
    else if([element isEqualToString:@"description"])
        [description appendString:string];
    else if([element isEqualToString:@"pubDate"])
        [pubDate appendString:string];
}
    
- (void)parserDidEndDocument:(NSXMLParser *)parser
{
      [_tableViewEvents reloadData];
}
    
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
        if ([[segue identifier] isEqualToString:@"showDetail"])
        {
            
            NSIndexPath *indexPath = [_tableViewEvents indexPathForSelectedRow];
            NSString *string = [_listEvents[indexPath.row] objectForKey: @"link"];
        }
}

@end

