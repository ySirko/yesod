//
//  SurveyViewController.h
//  NewCrmc
//
//  Created by WebitMacMini Two on 7/24/14.
//  Copyright (c) 2014 webit. All rights reserved.
//
#define DEGREES_TO_RADIANS(angle) (angle / 180.0 * M_PI)
@interface SurveyViewController : SubViewController
@property (retain, nonatomic) UITextField *txtGender;
@property (retain, nonatomic) UITextField *txtAge;
@property (retain, nonatomic) UIImageView*imgArrowGender;
@property (retain, nonatomic) UIImageView*imgArrowAge;
@property (retain,nonatomic)UIPickerView* pickerViewGender;
@property (retain,nonatomic) UIToolbar* toolbar;
@property (retain,nonatomic) UIBarButtonItem* btnOK;
@property (retain,nonatomic) UIBarButtonItem* btnCancel;
@property (retain,nonatomic) NSArray *arrGender;
@property (retain, nonatomic) UILabel *lblSurveyDescription;
@property (retain, nonatomic) UILabel*line1;
@property (retain, nonatomic) UILabel*line2;
@property (retain, nonatomic) UILabel*lblBack;
@property (retain, nonatomic) UILabel*lbl;
@property (retain, nonatomic)  UIButton *btn1;
@property (retain, nonatomic)  UIButton *btn2;
@property (retain, nonatomic)  UIButton *btn3;
@property (retain, nonatomic)  UIButton *btn4;
@property (retain, nonatomic)  UILabel *lbl1;
@property (retain, nonatomic)  UILabel *lbl2;
@property (retain, nonatomic)  UILabel *lbl3;
@property (retain, nonatomic)  UILabel *lbl4;
@property (retain, nonatomic)  UIColor *color;
@property (retain, nonatomic)  UIButton *btnPercent1;
@property (retain, nonatomic)  UIButton *btnPercent2;
@property (retain, nonatomic)  UIButton *btnPercent3;
@property (retain, nonatomic)  UIButton *btnPercent4;
@end
