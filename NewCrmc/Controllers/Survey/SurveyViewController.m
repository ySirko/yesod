//
//  SurveyViewController.m
//  NewCrmc
//
//  Created by WebitMacMini Two on 7/24/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

@interface SurveyViewController ()
{
    NSArray*listSurvey;
    Survey*survey;
    SurveyVote*surveyVote;
    BOOL finish;
    NSArray*btnArray;
    NSArray*lblPArray;
    int YorigionLineUp;
    int YorigionLineDown;
    int btnPercentWidth;
    
    int percent;
    //    int percentUP=self.view.frame.size.height*0.718;
    // int percentUP=158;
    
    int btnbWidth;
    int freeBlackBoard;
    int spaceFromLine;
    
    
    int constraintLeftSide;
    int constraintRightSide;
//    int w=self.view.frame.size.width+constraintRightSide;
//    int x=constraintLeftSide+widthButton;
    //    int space=((w-x)-widthButton)/2;
    int space;
}
@end

@implementation SurveyViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
   if([self.appManager.DeviceName isEqualToString:@"IPHONE 5"])
   {
//      btnbWidth=100;
//       constraintLeftSide=60;
//       constraintRightSide=self.view.frame.size.width-constraintLeftSide-btnbWidth;
//       space=constraintRightSide-(btnbWidth+constraintLeftSide);
       
       space=15;
       btnbWidth=100;
       constraintLeftSide=(self.view.frame.size.width/2)-(space/2)-btnbWidth;
       constraintRightSide= (self.view.frame.size.width/2)+(space/2);
       btnPercentWidth=btnbWidth*0.4;
   }
   else{
//       space=15;
//        btnbWidth=80;
//       constraintLeftSide=(self.view.frame.size.width/2)-(space/2)-btnbWidth;
//       constraintRightSide= (self.view.frame.size.width/2)+(space/2)+btnbWidth;
//      
//       constraintLeftSide=69;
//       constraintRightSide=self.view.frame.size.width-constraintLeftSide-btnbWidth;
//       space=constraintRightSide-constraintLeftSide-btnbWidth;
       space=15;
       btnbWidth=80;
       constraintLeftSide=(self.view.frame.size.width/2)-(space/2)-btnbWidth;
       constraintRightSide= (self.view.frame.size.width/2)+(space/2);
       btnPercentWidth=btnbWidth*0.4;
   }
   percent=self.view.frame.size.height*0.04;
//    int percentUP=self.view.frame.size.height*0.718;
       // int percentUP=158;
    YorigionLineUp=158;
    YorigionLineDown=self.view.frame.size.height-percent;
    
      freeBlackBoard=YorigionLineDown-256;
    spaceFromLine=freeBlackBoard*0.13;
   
    int sizeTitle=131;
    _lblBack=[[UILabel alloc]initWithFrame:CGRectMake(0, 220, 320, 36)];
    _lblBack.backgroundColor=_color;
    _lblBack.alpha=0.6;
    [self.view addSubview:_lblBack];
    _txtAge=[[UITextField alloc]initWithFrame:CGRectMake(8, 223, 150, 30)];
    _txtAge.borderStyle=UITextBorderStyleRoundedRect;
    _txtAge.placeholder=@"גיל";
    _txtAge.textAlignment=NSTextAlignmentCenter;
    _txtAge.delegate=self;
    _txtAge.font= [UIFont boldSystemFontOfSize:14.0f];
    [_txtAge setKeyboardType:UIKeyboardTypeNumberPad];
    [self.view addSubview:_txtAge];
    _txtGender=[[UITextField alloc]initWithFrame:CGRectMake(164, 223, 150, 30)];
    _txtGender.borderStyle=UITextBorderStyleRoundedRect;
    _txtGender.placeholder=@"מין";
    _txtGender.textAlignment=NSTextAlignmentCenter;
    _txtGender.delegate=self;
    _txtGender.font= [UIFont boldSystemFontOfSize:14.0f];
    [self.view addSubview:_txtGender];
    _imgArrowAge=[[UIImageView alloc]initWithFrame:CGRectMake(55, 237, 9, 4)];
    _imgArrowAge.image=[UIImage imageNamed:@"redd.png"];
    [self.view addSubview:_imgArrowAge];
    _imgArrowGender=[[UIImageView alloc]initWithFrame:CGRectMake(213, 237, 9, 4)];
    _imgArrowGender.image=[UIImage imageNamed:@"redd.png"];
    [self.view addSubview:_imgArrowGender];
    _lblSurveyDescription=[[UILabel alloc]initWithFrame:CGRectMake(9, 164, 302, 34)];
    _lblSurveyDescription.textAlignment=NSTextAlignmentCenter;
    _lblSurveyDescription.numberOfLines=2;
    _lblSurveyDescription.font = [UIFont fontWithName:@"Helvetica Neue" size:14.0];
    [self.view addSubview:_lblSurveyDescription];
    _line1=[[UILabel alloc]initWithFrame:CGRectMake(0, 158, 320, 6)];
    _line1.backgroundColor=_color;
    [self.view addSubview:_line1];
    _line2=[[UILabel alloc]initWithFrame:CGRectMake(0, YorigionLineDown, 320, 6)];
    _line2.backgroundColor=_color;
    [self.view addSubview:_line2];

//    _btn1=[[UIButton alloc]initWithFrame:CGRectMake(170, 265,btnbWidth, btnbWidth)];
//    _btn1.layer.cornerRadius=3;
//    _btn1.layer.borderWidth=1;
//    _btn1.tag=1;
//    [_btn1 addTarget:self action:@selector(btnImgClick:) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:_btn1];
//    _btn2=[[UIButton alloc]initWithFrame:CGRectMake(69, 265, btnbWidth, btnbWidth)];
//    _btn2.layer.cornerRadius=3;
//    _btn2.layer.borderWidth=1;
//    _btn2.tag=2;
//    [_btn2 addTarget:self action:@selector(btnImgClick:) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:_btn2];
//    _btn3=[[UIButton alloc]initWithFrame:CGRectMake(170, 365, btnbWidth, btnbWidth)];
//    _btn3.layer.cornerRadius=3;
//    _btn3.layer.borderWidth=1;
//    _btn3.tag=3;
//    [_btn3 addTarget:self action:@selector(btnImgClick:) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:_btn3];
//    _btn4=[[UIButton alloc]initWithFrame:CGRectMake(69, 365, btnbWidth, btnbWidth)];
//    _btn4.layer.cornerRadius=3;
//    _btn4.layer.borderWidth=1;
//    _btn4.tag=4;
//    [_btn4 addTarget:self action:@selector(btnImgClick:) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:_btn4];

    _btn1=[[UIButton alloc]initWithFrame:CGRectMake(constraintRightSide, 240+spaceFromLine,btnbWidth, btnbWidth)];
    _btn1.layer.cornerRadius=3;
    _btn1.layer.borderWidth=1;
    _btn1.tag=1;
    [_btn1 addTarget:self action:@selector(btnImgClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btn1];
    _btn2=[[UIButton alloc]initWithFrame:CGRectMake(constraintLeftSide, 240+spaceFromLine, btnbWidth, btnbWidth)];
    _btn2.layer.cornerRadius=3;
    _btn2.layer.borderWidth=1;
    _btn2.tag=2;
    [_btn2 addTarget:self action:@selector(btnImgClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btn2];
    _btn3=[[UIButton alloc]initWithFrame:CGRectMake(constraintRightSide, 240+spaceFromLine+btnbWidth+20, btnbWidth, btnbWidth)];
    _btn3.layer.cornerRadius=3;
    _btn3.layer.borderWidth=1;
    _btn3.tag=3;
    [_btn3 addTarget:self action:@selector(btnImgClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btn3];
    _btn4=[[UIButton alloc]initWithFrame:CGRectMake(constraintLeftSide, 240+spaceFromLine+btnbWidth+20, btnbWidth, btnbWidth)];
    _btn4.layer.cornerRadius=3;
    _btn4.layer.borderWidth=1;
    _btn4.tag=4;
    [_btn4 addTarget:self action:@selector(btnImgClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btn4];
    //label1
    _lbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 196, 320, 21)];
    _lbl.textAlignment=NSTextAlignmentCenter;
    _lbl.text=@"ע״מ לענות על שאלה זו מלא את הפרטים והמשך לסקר.";
    _lbl.font = [UIFont fontWithName:@"Helvetica Neue" size:13.0];
    [self.view addSubview:_lbl];
    _lbl1=[[UILabel alloc]initWithFrame:CGRectMake(constraintRightSide, _btn1.frame.origin.y+btnbWidth-5, btnbWidth, 25)];
    _lbl1.textAlignment=NSTextAlignmentCenter;
    _lbl1.font = [UIFont fontWithName:@"Helvetica Neue" size:9.0];
    [self.view addSubview:_lbl1];
    _lbl2=[[UILabel alloc]initWithFrame:CGRectMake(constraintLeftSide,  _btn1.frame.origin.y+btnbWidth-5, btnbWidth, 25)];
    _lbl2.textAlignment=NSTextAlignmentCenter;
    _lbl2.font = [UIFont fontWithName:@"Helvetica Neue" size:9.0];
    [self.view addSubview:_lbl2];
    _lbl3=[[UILabel alloc]initWithFrame:CGRectMake(constraintRightSide+5, _lbl1.frame.origin.y+_lbl1.frame.size.height+btnbWidth-5, btnbWidth, 25)];
    _lbl3.textAlignment=NSTextAlignmentCenter;
    _lbl3.font = [UIFont fontWithName:@"Helvetica Neue" size:9.0];
    [self.view addSubview:_lbl3];
    _lbl4=[[UILabel alloc]initWithFrame:CGRectMake(constraintLeftSide+5,  _lbl1.frame.origin.y+_lbl1.frame.size.height+btnbWidth-5, btnbWidth, 25)];
    _lbl4.textAlignment=NSTextAlignmentCenter;
    _lbl4.font = [UIFont fontWithName:@"Helvetica Neue" size:9.0];
    [self.view addSubview:_lbl4];
    //
    self.lblBackgroundTitle.backgroundColor=_color;
    self.Title.text=self.header;
    listSurvey=[[NSArray alloc]init];
    surveyVote=[[SurveyVote alloc]init];
    _pickerViewGender=[[UIPickerView alloc]init];
    _pickerViewGender.showsSelectionIndicator=YES;
    //definition
    [_lbl1 setFont:[UIFont systemFontOfSize:11]];
    [_lbl2 setFont:[UIFont systemFontOfSize:11]];
    [_lbl3 setFont:[UIFont systemFontOfSize:11]];
    [_lbl4 setFont:[UIFont systemFontOfSize:11]];
    _pickerViewGender.dataSource=self;
    _pickerViewGender.delegate=self;
    _arrGender=[[NSArray alloc]initWithObjects:@"זכר",@"נקבה", nil];
    _toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    _btnOK=[[UIBarButtonItem alloc]initWithTitle:@"אישור" style:(UIBarButtonItemStyle) UIBarButtonItemStyleBordered target:self action:@selector(OK_Click:)];
    _btnCancel=[[UIBarButtonItem alloc]initWithTitle:@"ביטול" style:(UIBarButtonItemStyle) UIBarButtonItemStyleBordered target:self action:@selector(CancelClick:)];
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    NSMutableArray *arrBtn=[[NSMutableArray alloc]initWithObjects:_btnOK,flexibleItem,_btnCancel, nil];
    [_toolbar setItems:arrBtn];
    _txtGender.inputView=_pickerViewGender;
    _txtGender.inputAccessoryView=_toolbar;
    btnArray=[[NSArray alloc]initWithObjects:_btn1,_btn2,_btn3,_btn4, nil];
    for (int i=0; i<4; i++)
    {
        ((UIButton*)btnArray[i]).layer.borderColor=[_color CGColor];
        [(UIButton*)btnArray[i] addTarget:self action:@selector(TouchDown:) forControlEvents:UIControlEventTouchDown];
    }
    
    [self.generic showNativeActivityIndicator:self];
    [self.connection connectionToService:@"GetSurveyToCity" jsonDictionary:[self.appDelegate.city parseCityToDict] controller:self withSelector:@selector(GetSurveyToCityResult:)];
    
}
-(void)TouchDown:(UIButton*)sender
{
    if(!([_txtGender.text isEqualToString:@""]||(!([ _txtAge.text isEqualToString:@""]))))
    {
        sender.imageView.image=[UIImage imageNamed:@""];
        sender.backgroundColor=_color;
    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
    
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return _arrGender.count;
}
    
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return _arrGender[row];
}
    
- (IBAction)OK_Click:(id)sender
{
    int row = (int)[_pickerViewGender selectedRowInComponent:0];
    _txtGender.text=[_arrGender objectAtIndex:row];
    [_txtGender resignFirstResponder];
}

- (IBAction)CancelClick:(id)sender
{
    [_txtGender resignFirstResponder];
}

-(void)GetSurveyToCityResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if(![result isEqualToString:@""]&&![result isEqualToString:@"[]"])
    {
        survey=[[Survey alloc]init];
        listSurvey=[survey parseListSurveyFromJson:result];
        int r = arc4random() % listSurvey.count+1;
        survey=listSurvey[r-1];
        _lblSurveyDescription.text=survey.surveyTitle;
        [_btn1 setImage:[UIImage imageWithData:[NSData dataFromBase64String:[NSString stringWithFormat:@"%@",((SurveyOption*)survey.optionsList[0]).image]]] forState:normal];
        [_btn2 setImage:[UIImage imageWithData:[NSData dataFromBase64String:[NSString stringWithFormat:@"%@",((SurveyOption*)survey.optionsList[1]).image]]]forState:normal];
        [_btn3 setImage:[UIImage imageWithData:[NSData dataFromBase64String:[NSString stringWithFormat:@"%@",((SurveyOption*)survey.optionsList[2]).image]]]forState:normal];
        [_btn4 setImage:[UIImage imageWithData:[NSData dataFromBase64String:[NSString stringWithFormat:@"%@",((SurveyOption*)survey.optionsList[3]).image]]]forState:normal];
        _lbl1.text=((SurveyOption*)survey.optionsList[0]).optionDescroption;
        _lbl2.text=((SurveyOption*)survey.optionsList[1]).optionDescroption;
        _lbl3.text=((SurveyOption*)survey.optionsList[2]).optionDescroption;
        _lbl4.text=((SurveyOption*)survey.optionsList[3]).optionDescroption;
        
        UIImageView*imgPrecent1=[[UIImageView alloc]initWithFrame:CGRectMake(constraintRightSide-2, _btn1.frame.origin.y+btnbWidth-btnPercentWidth+2, btnPercentWidth, btnPercentWidth)];
        imgPrecent1.transform=CGAffineTransformMakeRotation( DEGREES_TO_RADIANS(270));
        imgPrecent1.image=[UIImage imageNamed:@"precent.png"];
        [self.view addSubview:imgPrecent1];
        
                UILabel*lblPrecent1=[[UILabel alloc]initWithFrame:CGRectMake(constraintRightSide-2, _btn1.frame.origin.y+btnbWidth-(btnPercentWidth/2), 31, 10)];
//        UILabel*lblPrecent1=[[UILabel alloc]initWithFrame:CGRectMake(_btn1.frame.origin.x-1, _btn1.frame.origin.y+62, 31, 10)];
        lblPrecent1.textColor=[UIColor whiteColor];
        lblPrecent1.textAlignment=NSTextAlignmentCenter;
        lblPrecent1.text=[NSString stringWithFormat:@"%d%%",((SurveyOption*)survey.optionsList[0]).votePercent];
        lblPrecent1.font=[UIFont fontWithName:@"Helvetica" size:12];
        [self.view addSubview:lblPrecent1];
        
        UIImageView*imgPrecent2=[[UIImageView alloc]initWithFrame:CGRectMake(constraintLeftSide+btnbWidth-btnPercentWidth+2,_btn1.frame.origin.y+btnbWidth-btnPercentWidth+2, btnPercentWidth, btnPercentWidth)];
        imgPrecent2.transform=CGAffineTransformMakeRotation( DEGREES_TO_RADIANS(180));
        imgPrecent2.image=[UIImage imageNamed:@"precent.png"];
        [self.view addSubview:imgPrecent2];
        
        UILabel*lblPrecent2=[[UILabel alloc]initWithFrame:CGRectMake(constraintLeftSide+btnbWidth-(btnPercentWidth/2)-6, _btn1.frame.origin.y+btnbWidth-(btnPercentWidth/2), 31, 10)];
        lblPrecent2.textColor=[UIColor whiteColor];
        lblPrecent2.textAlignment=NSTextAlignmentCenter;
        lblPrecent2.text=[NSString stringWithFormat:@"%d%%",((SurveyOption*)survey.optionsList[1]).votePercent];
        lblPrecent2.font=[UIFont fontWithName:@"Helvetica" size:12];
        [self.view addSubview:lblPrecent2];
        
        UIImageView*imgPrecent3=[[UIImageView alloc]initWithFrame:CGRectMake(constraintRightSide-2, _btn3.frame.origin.y-2, btnPercentWidth, btnPercentWidth)];
        imgPrecent3.transform=CGAffineTransformMakeRotation( DEGREES_TO_RADIANS(0));
        imgPrecent3.image=[UIImage imageNamed:@"precent.png"];
        [self.view addSubview:imgPrecent3];
           UILabel*lblPrecent3=[[UILabel alloc]initWithFrame:CGRectMake(constraintRightSide, _btn3.frame.origin.y+9, 31, 10)];
//        UILabel*lblPrecent3=[[UILabel alloc]initWithFrame:CGRectMake(constraintLeftSide+btnbWidth-btnPercentWidth-2, _btn3.frame.origin.y+9, 31, 10)];
        lblPrecent3.textColor=[UIColor whiteColor];
        lblPrecent3.textAlignment=NSTextAlignmentCenter;
        lblPrecent3.text=[NSString stringWithFormat:@"%d%%",((SurveyOption*)survey.optionsList[2]).votePercent];
        lblPrecent3.font=[UIFont fontWithName:@"Helvetica" size:12];
        [self.view addSubview:lblPrecent3];
        
        UIImageView*imgPrecent4=[[UIImageView alloc]initWithFrame:CGRectMake(constraintLeftSide+btnbWidth-btnPercentWidth+2,_btn3.frame.origin.y-2, btnPercentWidth, btnPercentWidth)];
        imgPrecent4.transform=CGAffineTransformMakeRotation( DEGREES_TO_RADIANS(90));
        imgPrecent4.image=[UIImage imageNamed:@"precent.png"];
     [self.view addSubview:imgPrecent4];
        
        UILabel*lblPrecent4=[[UILabel alloc]initWithFrame:CGRectMake(constraintLeftSide+btnbWidth-(btnPercentWidth/2)-9, _btn3.frame.origin.y+9, 31, 10)];
      
        lblPrecent4.textColor=[UIColor whiteColor];
        lblPrecent4.textAlignment=NSTextAlignmentCenter;
        lblPrecent4.text=[NSString stringWithFormat:@"%d%%",((SurveyOption*)survey.optionsList[3]).votePercent];
        lblPrecent4.font=[UIFont fontWithName:@"Helvetica" size:12];
      [self.view addSubview:lblPrecent4];
    }
    else
    {
        UIAlertView*alert=[[UIAlertView alloc]initWithTitle:@"" message:@"אין סקרים זמינים כרגע" delegate:self cancelButtonTitle:@"אישור" otherButtonTitles: nil];
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if([alertView.message isEqualToString:@"אין סקרים זמינים כרגע"])
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if(![alertView.message isEqualToString:@"יש למלא מין וגיל"])
    {
        SurveyResultsViewController* view=[[SurveyResultsViewController alloc]init];
        view.color=_color;
        view.header=self.header;
        [self.navigationController pushViewController:view animated:YES];
        finish=YES;
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)btnImgClick:(id)sender
{
    if([_txtGender.text isEqualToString:@""]||[ _txtAge.text isEqualToString:@""])
    {
        UIAlertView* alert=[[UIAlertView alloc]initWithTitle:@"" message:@"יש למלא מין וגיל" delegate:self cancelButtonTitle:@"אישור" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        surveyVote.age=(int)[_txtAge.text integerValue];
        if([_txtGender.text isEqualToString:@"זכר"])
            surveyVote.gender=1;
        else
            surveyVote.gender=2;
        surveyVote.selectedOptionId=((SurveyOption*)survey.optionsList[((UIButton*)sender).tag-1]).surveyOptionId;
        [self.generic showNativeActivityIndicator:self];
        [self.connection connectionToService:@"AddSurveyVote" jsonDictionary:[surveyVote parseSurveyVoteToDict] controller:self withSelector:@selector(AddSurveyVoteResult:)];
    }
}
-(void)AddSurveyVoteResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if([result isEqualToString:@"true"])
    {
        UIAlertView*alert=[[UIAlertView alloc]initWithTitle:@"" message:@"דעתך חשובה לנו, תודה על שהקדשת מזמנך לטיפוח העיר" delegate:self cancelButtonTitle:@"אישור" otherButtonTitles:nil];
        [alert show];
    }
    else
        NSLog(@"Exeption");
}
@end
