//
//  SurveyResultTableViewCell.m
//  NewCrmc
//
//  Created by WebitMacMini Two on 8/6/14.
//  Copyright (c) 2014 webit. All rights reserved.
//



@implementation SurveyResultTableViewCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

-(void)SetCell:(Survey*)survey
{
    _lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(20, 5, 286, 21)];
    _lblTitle.textAlignment=NSTextAlignmentRight;
    _lblTitle.text=survey.surveyTitle;
    _lblTitle.font=[UIFont fontWithName:@"Helvetica Neue" size:11.0];
    [self addSubview:_lblTitle];
    
    _lbl1=[[UILabel alloc]initWithFrame:CGRectMake(171, 75, 111, 4)];
    _lbl1.backgroundColor=[UIColor lightGrayColor];
    [self addSubview:_lbl1];
    
    _lbl2=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 320, 3)];
    _lbl2.backgroundColor=_color;
    [self addSubview:_lbl2];
    
    _lblColor1=[[UILabel alloc]initWithFrame:CGRectMake(141, 33, 6, 6)];
    _lblColor1.backgroundColor=[UIColor redColor];
    [self addSubview:_lblColor1];
    _lblColor2=[[UILabel alloc]initWithFrame:CGRectMake(141, 44, 6, 6)];
    _lblColor2.backgroundColor=[UIColor yellowColor];
    [self addSubview:_lblColor2];
    _lblColor3=[[UILabel alloc]initWithFrame:CGRectMake(141, 55, 6, 6)];
    _lblColor3.backgroundColor= [UIColor colorWithRed:0.0f/255.0f green:176.0f/255.0f blue:66.0f/255.0f alpha:1.0f];
    [self addSubview:_lblColor3];
    _lblColor4=[[UILabel alloc]initWithFrame:CGRectMake(141, 66, 6, 6)];
    _lblColor4.backgroundColor=[UIColor colorWithRed:0.0f/255.0f green:154.0f/255.0f blue:238.0f/255.0f alpha:1.0f];
    [self addSubview:_lblColor4];
   
    _lblDiagram1=[[UILabel alloc]initWithFrame:CGRectMake(179, 56-((SurveyOption*)survey.optionsList[0]).votePercent/1.8+17, 21, ((SurveyOption*)survey.optionsList[0]).votePercent/1.8)];
    _lblDiagram2=[[UILabel alloc]initWithFrame:CGRectMake(204, 56-((SurveyOption*)survey.optionsList[1]).votePercent/1.8+17,21, ((SurveyOption*)survey.optionsList[1]).votePercent/1.8) ];
    _lblDiagram3=[[UILabel alloc]initWithFrame:CGRectMake(229,56-((SurveyOption*)survey.optionsList[2]).votePercent/1.8+17, 21, ((SurveyOption*)survey.optionsList[2]).votePercent/1.8) ];
    _lblDiagram4=[[UILabel alloc]initWithFrame:CGRectMake(254, 56-((SurveyOption*)survey.optionsList[3]).votePercent/1.8+17,21, ((SurveyOption*)survey.optionsList[3]).votePercent/1.8) ];
    _lblDiagram1.backgroundColor=[UIColor redColor];
    [self addSubview:_lblDiagram4];
    _lblDiagram2.backgroundColor=[UIColor yellowColor];
    [self addSubview:_lblDiagram3];
    _lblDiagram3.backgroundColor=[UIColor colorWithRed:0.0f/255.0f green:176.0f/255.0f blue:66.0f/255.0f alpha:1.0f];
    [self addSubview:_lblDiagram2];
    _lblDiagram4.backgroundColor=[UIColor colorWithRed:0.0f/255.0f green:154.0f/255.0f blue:238.0f/255.0f alpha:1.0f];
    [self addSubview:_lblDiagram1];
    
    _lblNum1=[[UILabel alloc]initWithFrame:CGRectMake(177, 57, 25, 21)];
    _lblNum1.font=[UIFont fontWithName:@"Helvetica Neue" size:8.0];
    _lblNum1.textAlignment=NSTextAlignmentCenter;
    [self addSubview:_lblNum1];
    _lblNum2=[[UILabel alloc]initWithFrame:CGRectMake(202, 57, 25, 21)];
    _lblNum2.font=[UIFont fontWithName:@"Helvetica Neue" size:8.0];
    _lblNum2.textAlignment=NSTextAlignmentCenter;
    [self addSubview:_lblNum2];
    _lblNum3=[[UILabel alloc]initWithFrame:CGRectMake(227, 57, 25, 21)];
    _lblNum3.font=[UIFont fontWithName:@"Helvetica Neue" size:8.0];
    _lblNum3.textAlignment=NSTextAlignmentCenter;
    [self addSubview:_lblNum3];
    _lblNum4=[[UILabel alloc]initWithFrame:CGRectMake(253, 57, 25, 21)];
    _lblNum4.font=[UIFont fontWithName:@"Helvetica Neue" size:8.0];
    _lblNum4.textAlignment=NSTextAlignmentCenter;
    [self addSubview:_lblNum4];
    _lblNum1.text=[NSString stringWithFormat:@"%d%%",((SurveyOption*)survey.optionsList[0]).votePercent];
    _lblNum2.text=[NSString stringWithFormat:@"%d%%",((SurveyOption*)survey.optionsList[1]).votePercent];
    _lblNum3.text=[NSString stringWithFormat:@"%d%%",((SurveyOption*)survey.optionsList[2]).votePercent];
    _lblNum4.text=[NSString stringWithFormat:@"%d%%",((SurveyOption*)survey.optionsList[3]).votePercent];
    
    _lblOption1=[[UILabel alloc]initWithFrame:CGRectMake(10, 24, 127, 21)];
    _lblOption1.font=[UIFont fontWithName:@"Helvetica Neue" size:10.0];
    _lblOption1.textAlignment=NSTextAlignmentRight;
    [self addSubview:_lblOption1];
    _lblOption2=[[UILabel alloc]initWithFrame:CGRectMake(10, 35, 127, 21)];
    _lblOption2.font=[UIFont fontWithName:@"Helvetica Neue" size:10.0];
    _lblOption2.textAlignment=NSTextAlignmentRight;
    [self addSubview:_lblOption2];
    _lblOption3=[[UILabel alloc]initWithFrame:CGRectMake(10, 46, 127, 21)];
    _lblOption3.font=[UIFont fontWithName:@"Helvetica Neue" size:10.0];
    _lblOption3.textAlignment=NSTextAlignmentRight;
    [self addSubview:_lblOption3];
    _lblOption4=[[UILabel alloc]initWithFrame:CGRectMake(10, 57, 127, 21)];
    _lblOption4.font=[UIFont fontWithName:@"Helvetica Neue" size:10.0];
    _lblOption4.textAlignment=NSTextAlignmentRight;
    [self addSubview:_lblOption4];
    _lblOption1.text=[NSString stringWithFormat:@"%@- %d%%",((SurveyOption*)survey.optionsList[0]).optionDescroption,((SurveyOption*)survey.optionsList[0]).votePercent];
    _lblOption2.text=[NSString stringWithFormat:@"%@- %d%%",((SurveyOption*)survey.optionsList[1]).optionDescroption,((SurveyOption*)survey.optionsList[1]).votePercent];
    _lblOption3.text=[NSString stringWithFormat:@"%@- %d%%",((SurveyOption*)survey.optionsList[2]).optionDescroption,((SurveyOption*)survey.optionsList[2]).votePercent];
    _lblOption4.text=[NSString stringWithFormat:@"%@- %d%%",((SurveyOption*)survey.optionsList[3]).optionDescroption,((SurveyOption*)survey.optionsList[3]).votePercent];
}
@end
