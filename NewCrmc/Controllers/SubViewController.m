//
//  SubViewController.m
//  NewCrmc
//
//  Created by WebitMacMini Two on 7/17/14.
//  Copyright (c) 2014 webit. All rights reserved.
//
#import "SubViewController.h"
typedef enum {
    GetAllCities
    
} functionName;

@interface SubViewController ()
{
    HomeSettingsViewController*view;
}
@end

@implementation SubViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
   //  self.screenName=@"SubViewController";
    _bt=[[UIButton alloc]initWithFrame:CGRectMake(0, 132, self.view.frame.size.width, 25)];
    [_bt setBackgroundImage:[UIImage imageNamed:@"רשת לאיפון"] forState:normal];
    [self.view addSubview:_bt];
    _appManager = (AppManager *)[AppManager sharedInstance];
    _appManager.deviceName=[AppManager yesButWhichDeviceIsIt];
    
    self.view.backgroundColor=[UIColor whiteColor];
    _appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _connection=[[Connection alloc]init];
    _generic=[[Generic alloc]init];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    CGSize g=self.view.frame.size;
    _background=[[UIImageView alloc]initWithFrame:CGRectMake(0, g.height-350, 320, 350)];
    [_background setAlpha:0.3];
    _background.image=[UIImage imageNamed:@"Background.png"];
    _background.contentMode=UIViewContentModeScaleToFill;
    [self.view addSubview:_background];
    [self.view sendSubviewToBack:_background];
    
    UILabel*settings=[[UILabel alloc]initWithFrame:CGRectMake(0, 18, 320, 31)];
    settings.backgroundColor=[UIColor grayColor];
    [self.view addSubview:settings];
    
    //   _logo=[[UIImageView alloc]initWithFrame:CGRectMake(4, 73, 91, 27)];
    _logo=[[UIImageView alloc]initWithFrame:CGRectMake(4, 63, 105, 40)];
   if(_appDelegate.logoCity==nil)
   {
   [self.generic showNativeActivityIndicator:self];
    [self.connection connectionToService:@"GetLogoImage" jsonDictionary:[self.appDelegate.city parseCityToDict] controller:self withSelector:@selector(GetLogoImageResult:)];
 }
  else
        _logo.image=_appDelegate.logoCity;
    [self.view addSubview:_logo];
    
    _buildings=[[UIImageView alloc]initWithFrame:CGRectMake(91, 50, 220, 49)];

    _buildings.image=[UIImage imageNamed:@"LogoBuilders.png"];
    [self.view addSubview:_buildings];
    if(_appDelegate.city.cityId==5)
        _logo.frame=CGRectMake(4, 73, 91, 27);
    if(_appDelegate.city.cityId==13)
        _logo.frame=CGRectMake(10, 60, 136, 42);
    if(_appDelegate.city.cityId==1)
        _buildings.hidden=YES;
    if(_appDelegate.city.cityId==10)
        _logo.frame=CGRectMake(6, 65, 85, 34);
    if(_appDelegate.city.cityId==9)
        _logo.frame= CGRectMake(8, 70, 82, 30);
    if(_appDelegate.city.cityId == 18)
        _logo.frame= CGRectMake(40, 60, 30, 40);
    //run msg back label
    _lblBackgroundTitle=[[UILabel alloc]initWithFrame:CGRectMake(0, 103, 320, 29)];
    _lblBackgroundTitle.backgroundColor=[UIColor colorWithRed:224.0f/255.0f green:226.0f/255.0f blue:226.0f/255.0f alpha:1.0f];
    [self.view addSubview:_lblBackgroundTitle];
    //run msg
    _Title=[[UILabel alloc]initWithFrame:CGRectMake(0, 97, 304, 34)];
    _Title.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    _Title.textAlignment = NSTextAlignmentRight;
    _Title.textColor=[UIColor whiteColor];
    [self.view addSubview:_Title];

      UIButton* btnSettings=[[UIButton alloc]initWithFrame:CGRectMake(10, 23, 27, 20)];
    [btnSettings addTarget:self action:@selector(btnSettingsClick:) forControlEvents:UIControlEventTouchUpInside];
    [btnSettings setImage:[UIImage imageNamed:@"SettingIcon.png"] forState:normal];
    [self.view addSubview:btnSettings];
// [_btn1 setTitleEdgeInsets:UIEdgeInsetsMake(40,0, 0, 0)];
    ///לסדר שיקטין את תוכן הכפתור במקום כפתור נוסף edges)focus(
    UIButton* btnSettingBack=[[UIButton alloc]initWithFrame:CGRectMake(7, 17, 33, 33)];
    btnSettingBack.backgroundColor=[UIColor clearColor];
     [btnSettingBack addTarget:self action:@selector(btnSettingsClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnSettingBack];
  
    if(self.navigationController.viewControllers[0]!=self&&![self.navigationController.viewControllers[self.navigationController.viewControllers.count-2] isKindOfClass:[PromotionalPageViewController class]])
    {
        UIButton* btnPopUp=[[UIButton alloc]initWithFrame:CGRectMake(55, 20, 27, 27)];
        [btnPopUp addTarget:self action:@selector(btnPopUpClick:) forControlEvents:UIControlEventTouchUpInside];
        [btnPopUp setImage:[UIImage imageNamed:@"BackIcon.png"] forState:normal];
        [self.view addSubview:btnPopUp];
        
        UIButton* btnPopUpBack=[[UIButton alloc]initWithFrame:CGRectMake(52, 17, 33, 33)];
        btnPopUpBack.backgroundColor=[UIColor clearColor];
        [btnPopUpBack addTarget:self action:@selector(btnPopUpClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btnPopUpBack];
    }
    
    if(![self isKindOfClass:[HomeViewController class]])
    {
        UIButton* btnHomePage=[[UIButton alloc]initWithFrame:CGRectMake(100, 23, 27, 20)];
        [btnHomePage addTarget:self action:@selector(btnHomePageClick:) forControlEvents:UIControlEventTouchUpInside];
        [btnHomePage setImage:[UIImage imageNamed:@"HomeIcon.png"] forState:normal];
        [self.view addSubview:btnHomePage];
    //לצמצם
        UIButton* btnHomePageBack=[[UIButton alloc]initWithFrame:CGRectMake(97, 17, 33, 33)];
        btnHomePageBack.backgroundColor=[UIColor clearColor];
        [btnHomePageBack addTarget:self action:@selector(btnHomePageClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btnHomePageBack];
    }
    
    [[[[UIApplication sharedApplication] delegate] window] setWindowLevel:UIWindowLevelNormal];
     self.view.clipsToBounds = NO;
}
//להעביר לפרומפט ולהכניס לappdele
-(void)GetLogoImageResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if(![result isEqualToString:@"\"\""]&&![result isEqualToString:@"[]"]&&![result isEqualToString:@""])
    {
        UIImage *image=[UIImage imageWithData:[NSData dataFromBase64String:[NSString stringWithFormat:@"%@", result]]];
        _appDelegate.logoCity=image;
        _logo.image=_appDelegate.logoCity;
    }
    else
    {
        _appDelegate.logoCity=[UIImage imageNamed:@"chadera_logo.jpg"];
        _logo.image=_appDelegate.logoCity;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(UIViewController*)GetHomeVC
{
    for (UIViewController* vc in self.navigationController.viewControllers) {
        if([vc isKindOfClass:[HomeViewController class]])
            return vc;
    }
    return [[HomeViewController alloc]init];
}
-(IBAction)btnHomePageClick:(id)sender
{
    if(![self isKindOfClass:[HomeViewController class]])
    {
        [self name];
        [self.navigationController popToViewController:[self GetHomeVC] animated:YES];
    }
}

- (IBAction)btnSettingsClick:(id)sender
{
    view = [[HomeSettingsViewController alloc] init];
    view.index = 1;
    view.view.alpha=0;
    //בתוך האנימציה מה שבסוף
    [self.view addSubview:view.view];
    [UIView animateWithDuration:0.5
                          delay:.2f
                        options: UIViewAnimationOptionAllowAnimatedContent
                     animations:^{
                         view.view.alpha=1;
                     }
                     completion:^(BOOL finished){}];
}

- (IBAction)btnPopUpClick:(id)sender
{
    [self name];
    if([self isKindOfClass:[SurveyResultsViewController class]])
        [self.navigationController popToViewController:[ self.navigationController.viewControllers objectAtIndex:1] animated:NO];
    else
        [self.navigationController popViewControllerAnimated:YES];
}

- (void)name//to  stop run message
{
    
}


@end
