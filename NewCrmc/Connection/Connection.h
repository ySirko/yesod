//
//  Connection.h
//  JewishCard
//
//  Created by nissim h on 6/6/13.
//  Copyright (c) 2013 webit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Generic.h"


@interface Connection : NSObject {
    BOOL recordResults;
    NSMutableData *webData;
}

@property(nonatomic, strong) NSMutableData *webData;
@property(nonatomic, readwrite) BOOL recordResults;
@property (nonatomic, retain) UIViewController *controller;
@property (nonatomic, strong) Generic *generic;

- (void)connectionToService: (NSString *)action jsonDictionary:(NSDictionary *)jsonDict controller:(UIViewController *)cont withSelector:(SEL)sel;
- (BOOL)connected;

@end
