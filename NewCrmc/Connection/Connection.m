//
//  Connection.m
//  JewishCard
//
//  Created by nissim h on 6/6/13.
//  Copyright (c) 2013 webit. All rights reserved.
//

#import "Connection.h"
#import <dispatch/dispatch.h>
#import "Reachability.h"
#import "Generic.h"

@interface Connection ()
{
    dispatch_queue_t queue;
    NSOperationQueue *operationQueue;
}
@end

@implementation Connection
@synthesize recordResults;
@synthesize webData;

Generic *generic;

- (void)connectionToService: (NSString *)action jsonDictionary:(NSDictionary *)jsonDict controller:(UIViewController *)cont withSelector:(SEL)sel
{
    generic = [[Generic alloc] init];
    operationQueue =[NSOperationQueue new];
    if([self connected]){
        self.controller = cont;
        recordResults = FALSE;
       NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://ourcityws.mokedcrmc.com/Service.svc/%@", action]];

        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        NSError *error;
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        if(jsonDict)
        {
            NSData *requestData = [NSJSONSerialization dataWithJSONObject:jsonDict options:0 error:&error];
            [request setValue:[NSString stringWithFormat:@"%d", [requestData length]] forHTTPHeaderField:@"Content-Length"];
            [request setHTTPBody: requestData];
        }
        
        
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:operationQueue // created at class init
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                                   NSString * str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                   
                                   dispatch_async(dispatch_get_main_queue(),^{
                                       if(error == NULL){
                                           [cont performSelector:(sel) withObject:str];
                                       }else{
                                           [generic hideNativeActivityIndicator:cont];
                                           [self showError:@"אירעה שגיאה לא צפויה, אנא נסה שנית מאוחר יותר"];
                                       }
                                   });
                               }];
    } else
    {
        [generic hideNativeActivityIndicator:cont];
        [self showError:@"Your device is not connected with the internet, Please check your connections"];
    }
}

-(BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus == NotReachable);
}

- (void)showError:(NSString *)msg
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"הודעה" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
}

@end
