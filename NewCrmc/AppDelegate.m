//
//  AppDelegate.m
//  NewCrmc
//
//  Created by WebitMacMini Two on 7/17/14.
//  Copyright (c) 2014 webit. All rights reserved.
//
////////////is changed/////////////

#import "AppDelegate.h"
#import "PromotionalPageViewController.h"
#import "EventsTableViewController.h"
#import "NSData+Base64.h"
#import "HomeViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "Menu.h"
#import "StringHeader.h"
#import "GAI.h"

typedef enum {
    Chadera = 1,
    Migdal_Haemek = 5,
    Machon_Weitzman = 9,
    Binyamina_Givat_Ada = 10,
    Shoham = 13,
    Yesod = 18
} CityName;

@implementation AppDelegate
{
    PromotionalPageViewController *viewController;
    EventsTableViewController *eventsTableViewController;
    Connection *connection;
    Generic *generic;
    CityName cityName;
    UINavigationController *navViewController;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    _window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    cityName = 18;
    
    // Optional: automatically send uncaught exceptions to Google Analytics.
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    [GAI sharedInstance].dispatchInterval = 20;
    
    // Optional: set Logger to VERBOSE for debug information.
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    
    // Initialize tracker. Replace with your tracking ID.
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-63953061-1"];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    tracker.allowIDFACollection = YES;
    
    [self registerForNotifications];
    
    if (cityName == Yesod)
        [GMSServices provideAPIKey:@"AIzaSyAD6Pyvc3UA_7Th1GVx0ucUVcKno35Mf9c"];
    City* city=[[City alloc]init];
    connection=[[Connection alloc]init];
    generic=[[Generic alloc]init];
    [generic showNativeActivityIndicator:viewController];
    [connection connectionToService:@"GetAllCities" jsonDictionary:[city parseCityToDict] controller:self withSelector:@selector(GetCitiesByCityResult:)];
    [launchOptions valueForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    
    [self GetPromotionalImageResultt:@""];
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithDictionary:launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey]];
    if (userInfo.count) {
        _openFromPush = YES;
    }
    
    [self.window makeKeyAndVisible];
    
    return YES;
}

-(void)setMenu:(NSArray *)menu
{
    _menu = menu;
}

- (BOOL) pushNotificationOnOrOff
{
    if ([UIApplication instancesRespondToSelector:@selector(isRegisteredForRemoteNotifications)]) {
        return ([[UIApplication sharedApplication] isRegisteredForRemoteNotifications]);
    } else {
        UIRemoteNotificationType types = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
        return (types & UIRemoteNotificationTypeAlert);
    }
}

#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:   (UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    //handle the actions
    if ([identifier isEqualToString:@"declineAction"])
    {
        
    }
    else if ([identifier isEqualToString:@"answerAction"])
    {
        
    }
}
#endif

- (void) application:(UIApplication *)application
   didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    _dt=[[NSString alloc]init];//notification
    _dt = [[[[deviceToken description]
                                stringByReplacingOccurrencesOfString: @"<" withString: @""]
                               stringByReplacingOccurrencesOfString: @">" withString: @""]
                              stringByReplacingOccurrencesOfString: @" " withString: @""] ;
}

- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    if (state == UIApplicationStateBackground || state == UIApplicationStateInactive) {
        _openFromPush = YES;
        UIViewController *cont = navViewController.viewControllers[1];
        [navViewController popToViewController:cont animated:YES];
        [cont viewDidLoad];
    }
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
	NSLog(@"Failed to get token, error: %@", error);
}

-(void)GetCitiesByCityResult:(NSString*)result
{
    [generic hideNativeActivityIndicator:viewController];
    if(![result isEqualToString:@""]&&![result isEqualToString:@"[]"])
    {
        City* city=[[City alloc]init];
        
        //choose city
        _listCities = [NSMutableArray arrayWithArray:[[city parseListCityFromJson:result] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"cityId =[cd] %d",cityName]]];
        _city=[[City alloc]init:((City*)_listCities[0]).cityId Name:((City*)_listCities[0]).cityName];
        [generic showNativeActivityIndicator:viewController];
        
        viewController = [[PromotionalPageViewController alloc] init];
        NSString *imageName = [NSString stringWithFormat:@"%uBackGround", cityName];
        ((PromotionalPageViewController*)viewController).imageView=[[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    
        //navigation from appdelegate
        navViewController=[[UINavigationController alloc]initWithRootViewController:viewController];
        [_window setRootViewController:navViewController];
    }
    else
        NSLog(@"Empty cities list");
}
-(void)GetPromotionalImageResultt:(NSString*)result
{
    UIViewController *present = [UIViewController new];
    NSString *imageName = [NSString stringWithFormat:@"%uBackGround", cityName];
    UIImageView *backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    present.view.frame = [[UIScreen mainScreen] bounds];;
    backgroundImage.frame = [[UIScreen mainScreen] bounds];;
    [present.view addSubview:backgroundImage];
    [_window setRootViewController:present];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}

#pragma mark - Private

- (void)registerForNotifications
{
    if ([[UIDevice currentDevice].systemVersion floatValue] > 8.0) {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeSound|UIRemoteNotificationTypeAlert) categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    } else {
        UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeSound;
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:myTypes];
    }
}

@end
