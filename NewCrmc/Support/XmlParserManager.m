//
//  XmlParserManager.m
//  מגדל העמק
//
//  Created by MyMac on 12/8/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "XmlParserManager.h"

@implementation XmlParserManager

-(id)initWithURL: (NSURL*)url
{
    [self startParse:url];
    return self;
}
-(void)startParse:(NSURL*)url
{
    _feeds=[[NSMutableArray alloc]init];
    _parser=[[NSXMLParser alloc]initWithContentsOfURL:url];
    _parser.delegate=self;
    _parser.shouldProcessNamespaces = false;
    _parser.shouldReportNamespacePrefixes = false;
    _parser.shouldResolveExternalEntities = false;
    [_parser parse];
}

-(NSMutableArray*) allFeeds
{
    return _feeds;
}

-(void)parser:(NSXMLParser*)parser didStartElement:(NSString*)elementName NamespaceURI:(NSString*)namespaceURI qualifiedName:(NSString*)qualifiedName attributes:(NSDictionary*)attributeDict
{
    _element=elementName;
    if([_element isEqualToString:@"item"])
    {
        _elements=[[NSMutableDictionary alloc]init];
        _ftitle=[[NSMutableString alloc]initWithString:@""];
        _link=[[NSMutableString alloc]initWithString:@""];
        _fdescription=[[NSMutableString alloc]initWithString:@""];
        _fdate=[[NSMutableString alloc]initWithString:@""];
    }
}
-(void)parser:(NSXMLParser*)parser didStartElement:(NSString*)elementName NamespaceURI:(NSString*)namespaceURI qualifiedName:(NSString*)qualifiedName
{
    if([elementName isEqualToString:@"item"])
    {
        if(![_ftitle isEqualToString:@""])
            [_elements setObject:_ftitle forKey:@"title"];
        if(![_link isEqualToString:@""])
            [_elements setObject:_link forKey:@"link"];
        if(![_fdescription isEqualToString:@""])
            [_elements setObject:_fdescription forKey:@"description"];
        if(![_fdate isEqualToString:@""])
            [_elements setObject:_fdate forKey:@"pubDate"];
        [_feeds addObject:_elements];
    }
}
-(void)parser:(NSXMLParser*)parser foundCharacters:(NSString*)string
{
    if([_element isEqualToString:@"title"])
        [_ftitle appendString:string];
    else if ([_element isEqualToString:@"link"])
        [_link appendString:string];
    else if ([_element isEqualToString:@"description"])
        [_fdescription appendString:string];
    else if ([_element isEqualToString:@"pubDate"])
        [_fdate appendString:string];

}
@end
