//
//  UIHyperlinksButton.m
//  UIHyperlinksButtonDemo
//
//  Created by yaoqianyi on 13-8-13.
//  Copyright (c) 2013年 yaoqianyi. All rights reserved.
//

#import "UIHyperlinksButton.h"

@implementation UIHyperlinksButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+ (UIHyperlinksButton*) hyperlinksButton
{
    UIHyperlinksButton* button = [[UIHyperlinksButton alloc] init];
    return button;
}
-(void) drawRect:(CGRect)rect
{
    CGRect textRect = self.titleLabel.frame;
    CGContextRef contextRef = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(contextRef, 0.1, 0.1, 0.3, 1.0);
    CGFloat descender = self.titleLabel.font.descender;
    CGContextMoveToPoint(contextRef, textRect.origin.x, textRect.origin.y + textRect.size.height + descender+1);
    CGContextAddLineToPoint(contextRef, textRect.origin.x + textRect.size.width, textRect.origin.y + textRect.size.height + descender+1);
    UIColor *c=[[UIColor alloc]initWithCGColor:(self.titleLabel.textColor).CGColor];
    CGContextSetStrokeColorWithColor(contextRef,c.CGColor);
    CGContextClosePath(contextRef);
    CGContextDrawPath(contextRef, kCGPathStroke);
}


@end
