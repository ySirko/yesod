//
//  XmlParserManager.h
//  מגדל העמק
//
//  Created by MyMac on 12/8/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XmlParserManager : NSObject<NSXMLParserDelegate>
@property(retain,nonatomic)NSXMLParser*parser;
@property(retain,nonatomic)NSMutableArray*feeds;
@property(retain,nonatomic)NSMutableDictionary*elements;
@property(retain,nonatomic)NSString*element;
@property(retain,nonatomic)NSMutableString*ftitle;
@property(retain,nonatomic)NSMutableString*link;
@property(retain,nonatomic)NSMutableString*fdescription;
@property(retain,nonatomic)NSMutableString*fdate;
-(id)initWithURL: (NSURL*)url;
-(void)startParse:(NSURL*)url;
-(NSMutableArray*) allFeeds;
-(void)parser:(NSXMLParser*)parser didStartElement:(NSString*)elementName NamespaceURI:(NSString*)namespaceURI qualifiedName:(NSString*)qualifiedName attributes:(NSDictionary*)attributeDict;
-(void)parser:(NSXMLParser*)parser didStartElement:(NSString*)elementName NamespaceURI:(NSString*)namespaceURI qualifiedName:(NSString*)qualifiedName;
-(void)parser:(NSXMLParser*)parser foundCharacters:(NSString*)string;
@end
