//
//  LatLng.h
//  מגדל העמק
//
//  Created by MyMac on 12/9/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LatLng : NSObject
@property int iLatLngId;
@property(retain,nonatomic)NSString*nvLatLangInfo;
@property int iLatLngType;
@property(retain,nonatomic)NSString*nvLat;
@property(retain,nonatomic)NSString*nvLng;
@property int iTripId;
-(NSMutableArray*)parseListLatLngFromJson:(NSArray*) strJson;
@end
