//
//  Entity.h
//  CRMC
//
//  Created by MyMac on 6/22/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Entity : NSObject
@property int entityId;
@property int cityId;
@property(retain,nonatomic)NSString* entityName;
@property(retain,nonatomic)NSString* phoneNumber;
@property(retain,nonatomic)NSString* emailAddress;
@property(retain,nonatomic)NSString* information;
@property(retain,nonatomic)NSString*details;
-(NSMutableArray*)parseListEntityFromJson:(NSString*) strJson;
-(Entity*)parseEntityFromDict:(NSDictionary*)dict;
@end
