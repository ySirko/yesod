//
//  WSStreet.h
//  Global
//
//  Created by MyMac on 10/23/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WSStreet : NSObject
@property int StreetID;
@property(retain,nonatomic)NSString*StreetName;
@property(retain,nonatomic)NSString*houseNumber;
-(WSStreet*)parseWSStreetFromDict:(NSDictionary*)dict;
-(NSMutableArray*)parseListWSStreetFromJson:(NSArray*) strJson;

@end
