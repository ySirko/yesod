//
//  Survey.m
//  NewCrmc
//
//  Created by WebitMacMini Two on 7/23/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "Survey.h"
#import "SurveyOption.h"
@implementation Survey

-(NSMutableArray*)parseListSurveyFromJson:(NSString*) strJson
{
    NSError* error;
    NSArray* arr=[NSJSONSerialization JSONObjectWithData:[strJson dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    NSMutableArray* arrSurvey;
    if(arr.count>0)
    {
        arrSurvey=[[NSMutableArray alloc]init];
        for (NSDictionary* dict in arr)
            [arrSurvey addObject:[self parseSurveyFromDict:dict]];
    }
    return  arrSurvey;
}

-(Survey*)parseSurveyFromDict:(NSDictionary*)dict
{
    Survey* survey=[[Survey alloc]init];
    survey.surveyId=(int)[[dict objectForKey:@"surveyId"] integerValue];
    survey.surveyTitle=[dict objectForKey:@"surveyTitle"];
    SurveyOption*surveyOption=[[SurveyOption alloc]init];
    survey.optionsList=[surveyOption parseListSurveyOptionFromJson:[dict objectForKey:@"optionsList"]];
    return survey;
}
@end
