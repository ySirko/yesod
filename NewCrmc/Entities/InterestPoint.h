//
//  InterestPoint.h
//  CRMC
//
//  Created by MyMac on 6/22/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
@interface InterestPoint : NSObject

@property int interestPointId;
@property int categoryId;
@property int cityId;
@property(retain,nonatomic)NSString* lon;
@property(retain,nonatomic)NSString* lat;
@property(retain,nonatomic)NSString* descriptionn;
@property (nonatomic, retain) NSData *icon;
-(NSMutableArray*)parseListInterestPointFromDict:(NSDictionary*)dict Icon:(NSData*)icon;
-(InterestPoint*)parseInterestPointFromDict:(NSDictionary*)dict Icon:(NSData*)icon;
@end
