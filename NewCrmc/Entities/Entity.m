//
//  Entity.m
//  CRMC
//
//  Created by MyMac on 6/22/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import "Entity.h"

@implementation Entity
-(NSMutableArray*)parseListEntityFromJson:(NSString*) strJson
{
    NSError* error;
    NSArray* arr=[NSJSONSerialization JSONObjectWithData:[strJson dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    NSMutableArray* arrStreet;
    if(arr.count>0)
    {
        arrStreet=[[NSMutableArray alloc]init];
        for (NSDictionary* dict in arr)
            [arrStreet addObject:[self parseEntityFromDict:dict]];
    }
    return  arrStreet;
}

-(Entity*)parseEntityFromDict:(NSDictionary*)dict
{
    Entity* entity=[[Entity alloc]init];
    entity.entityName=[dict objectForKey:@"entityName"];
    entity.phoneNumber=[dict objectForKey:@"phoneNumber"];
    entity.emailAddress=[dict objectForKey:@"emailAddress"];
    entity.information=[dict objectForKey:@"information"];
    entity.cityId=[[dict objectForKey:@"cityId"]integerValue];
    return entity;
}

@end
