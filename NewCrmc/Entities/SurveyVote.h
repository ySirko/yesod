//
//  SurveyVote.h
//  NewCrmc
//
//  Created by WebitMacMini Two on 7/23/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SurveyVote : NSObject
@property int surveyVoteId;
@property int selectedOptionId;
@property int age;
@property int gender;
-(NSDictionary*)parseSurveyVoteToDict;
@end
