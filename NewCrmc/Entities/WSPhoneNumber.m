//
//  WSPhoneNumber.m
//  מגדל העמק
//
//  Created by MyMac on 9/28/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "WSPhoneNumber.h"

@implementation WSPhoneNumber
- (id)initwith:(NSString*)AreaCode phoneNumber:(NSString*)PhoneNumber
{
  //  self = [super init];
    if (self) {
        _AreaCode=AreaCode;
        _PhoneNumber=PhoneNumber;
    }
    return self;
}

-(NSDictionary*)parseWSPhoneNumberToDict
{
    NSMutableDictionary* dict=[[NSMutableDictionary alloc]init];
    if (!_AreaCode) {
        _AreaCode = @"";
    }
    [dict setObject:_AreaCode forKey:@"AreaCode"];
    [dict setObject:_PhoneNumber forKey:@"PhoneNumber"];
    return dict;
}

@end
