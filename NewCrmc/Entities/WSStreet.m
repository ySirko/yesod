//
//  WSStreet.m
//  Global
//
//  Created by MyMac on 10/23/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "WSStreet.h"

@implementation WSStreet
-(NSMutableArray*)parseListWSStreetFromJson:(NSArray*) strJson
{

    NSMutableArray*arrWSStreet=[[NSMutableArray alloc]init];
//if(arrWSStreet.count>0)
//    {
        for (NSDictionary* dict in strJson)
            [arrWSStreet addObject:[self parseWSStreetFromDict:dict]];
  // }

   

    return  arrWSStreet;
}

-(WSStreet*)parseWSStreetFromDict:(NSDictionary*)dict
{
    WSStreet* wSStreet=[[WSStreet alloc]init];
    wSStreet.StreetID=(int)[[dict objectForKey:@"StreetID"] integerValue];
    wSStreet.StreetName=[dict objectForKey:@"StreetName"];
    return wSStreet;
}
@end
