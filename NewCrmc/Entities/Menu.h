//
//  Menu.h
//  חדרה
//
//  Created by MyMac on 11/25/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Menu : NSObject

@property NSInteger ID;
@property NSInteger cityID;
@property NSInteger menuID;
@property (retain,nonatomic) NSString *name;
@property (retain,nonatomic) UIColor *color;
@property (retain,nonatomic) UIColor *HighlightedColor;
@property (retain,nonatomic) UIImage *icon;
@property NSInteger active;
@property NSInteger tag;
@property (retain,nonatomic) NSString *link;
@property (retain,nonatomic) NSMutableArray *listMenu;
@property (retain,nonatomic) NSString *strColor;
@property (retain,nonatomic) NSString *phone;
@property (retain,nonatomic) NSString *email;
@property (retain,nonatomic) NSString *emailSubject;

- (NSMutableArray *)parseListMenuFromJson:(NSString *)strJson;

@end
