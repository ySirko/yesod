//
//  WSAddress.h
//  מגדל העמק
//
//  Created by MyMac on 9/28/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WSAddress : NSObject
@property(retain,nonatomic)NSString* StreetName;
@property(retain,nonatomic)NSString* HouseNumber;
-(NSDictionary*)parseWSAddressToDict;
- (id)initwith :(NSString*)StreetName house:(NSString*)HouseNumber sitename:(NSString*)SiteName;

@end
