//
//  Event.h
//  CRMC
//
//  Created by WebitMacMini Two on 6/15/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Event : NSObject
@property int eventId;

@property(retain,nonatomic)NSString* title;
@property(retain,nonatomic)NSString* notes;
@property(retain,nonatomic)NSString* eventDate;
@property(retain,nonatomic)NSString* address;
@property(retain,nonatomic)NSString* descriptionn;
@property int cityId;
@property(retain,nonatomic)NSString* price;
@property(retain,nonatomic)NSString* link;
-(NSMutableArray*)parseListEventFromJson:(NSString*) strJson;
-(Event*)parseEventFromDict:(NSDictionary*)dict;
-(NSMutableArray*)parseListEventFromDict:(NSDictionary*)dict;
-(Event*)parseEventFromDictFeeds:(NSMutableDictionary*)dict;
@end


