//
//  AppManager.m
//
//  Created by Benny Davidovitz on 1/6/15.
//  Copyright (c) 2015 webit. All rights reserved.
//

#import "AppManager.h"

@implementation AppManager
SYNTHESIZE_SINGLETON_FOR_CLASS (*AppManager);

+ (AppManager *) sharedInstance{
    static AppManager *sharedInstance = nil;
    @synchronized (self){
        if (sharedInstance == nil) {
            sharedInstance = [[self alloc] init];
            [AppManager setColors];

            [sharedInstance initialize];
        }
    }
    return sharedInstance;
}

//Here you can implement some basic stuff to do when shared instance being instantiating
- (void) initialize{
    
}

//When Project's classes being load, we will listen to certain point in the app's life-cycle to make sure that the AppManager is still alive

+ (void) load{
    [super load];
    
    //when app starts
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNotification:) name:UIApplicationDidFinishLaunchingNotification object:nil];
    //when app being active
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNotification:) name:UIApplicationDidBecomeActiveNotification object:nil];
   }

+ (void) handleNotification:(NSNotification *)note{
    [self sharedInstance];
}
+(NSString *)yesButWhichDeviceIsIt
{
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenHeight = screenSize.height;
    NSString*dName;
    
    int height=(int)(screenHeight);
    switch (height) {
        case 480:
            dName= @"IPHONE 4";
            break;
        case 568:
            dName= @"IPHONE 5";
            break;
        case 667:
            dName= @"IPHONE 6";
            break;
        default:
            break;
    }
    return dName;
}
+ (void) setColors
{
    
        [[AppManager sharedInstance] setLightBlue:[UIColor colorWithRed:27.0f/255.0f green:168.0f/255.0f blue:249.0f/255.0f alpha:1.0f]];
        [[AppManager sharedInstance] setPink:[UIColor colorWithRed:239.0f/255.0f green:19.0f/255.0f blue:121.0f/255.0f alpha:1.0f]];
    //   _purple=[UIColor colorWithRed:166.0f/255.0f green:27.0f/255.0f blue:184.0f/255.0f alpha:1.0f];
        [[AppManager sharedInstance] setPurple:[UIColor  colorWithRed:102.0f/255.0f green:45.0f/255.0f blue:145.0f/255.0f alpha:1.0f]];
        [[AppManager sharedInstance] setGreen:[UIColor  colorWithRed:23.0f/255.0f green:184.0f/255.0f blue:75.0f/255.0f alpha:1.0f]];
        [[AppManager sharedInstance] setOrange:[UIColor  colorWithRed:255.0f/255.0f green:142.0f/255.0f blue:12.0f/255.0f alpha:1.0f]];
        [[AppManager sharedInstance] setLightBlueHighlighted:[UIColor colorWithRed:0.0f/255.0f green:172.0f/255.0f blue:220.0f/255.0f alpha:1.0f] ];
        [[AppManager sharedInstance] setRedHighlighted:[UIColor  colorWithRed:186.0f/255.0f green:0.0f/255.0f blue:23.0f/255.0f alpha:1.0f] ];
        [[AppManager sharedInstance] setPinkHighlighted:[UIColor  colorWithRed:182.0f/255.0f green:35.0f/255.0f blue:103.0f/255.0f alpha:1.0f]];
        [[AppManager sharedInstance] setPurpleHighlighted:[UIColor  colorWithRed:150.0f/255.0f green:24.0f/255.0f blue:166.0f/255.0f alpha:1.0f]];
        [[AppManager sharedInstance] setGreenHighlighted:[UIColor colorWithRed:0.0f/255.0f green:166.0f/255.0f blue:81.0f/255.0f alpha:1.0f]];
        [[AppManager sharedInstance] setOrangeHighlighted:[UIColor colorWithRed:246.0f/255.0f green:139.0f/255.0f blue:31.0f/255.0f alpha:1.0f]];
        [[AppManager sharedInstance] setLightBlue2:[UIColor  colorWithRed:15.0f/255.0f green:136.0f/255.0f blue:184.0f/255.0f alpha:1.0f]];
        [[AppManager sharedInstance] setPink2:[UIColor  colorWithRed:239.0f/255.0f green:19.0f/255.0f blue:121.0f/255.0f alpha:1.0f] ];
    //    _purple2=[UIColor colorWithRed:118.0f/255.0f green:36.0f/255.0f blue:184.0f/255.0f alpha:1.0f]];
        [[AppManager sharedInstance] setPurple2:[UIColor colorWithRed:76.0f/255.0f green:23.0f/255.0f blue:109.0f/255.0f alpha:1.0f] ];
        [[AppManager sharedInstance] setGreen2:[UIColor colorWithRed:30.0f/255.0f green:130.0f/255.0f blue:51.0f/255.0f alpha:1.0f] ];
        [[AppManager sharedInstance] setOrange2:[UIColor colorWithRed:255.0f/255.0f green:107.0f/255.0f blue:14.0f/255.0f alpha:1.0f] ];
        [[AppManager sharedInstance] setRed2:[UIColor redColor]];
        [[AppManager sharedInstance] setLightBlueHighlighted2:[UIColor colorWithRed:11.0f/255.0f green:113.0f/255.0f blue:154.0f/255.0f alpha:1.0f] ];
        [[AppManager sharedInstance] setRedHighlighted2:[UIColor  colorWithRed:186.0f/255.0f green:0.0f/255.0f blue:23.0f/255.0f alpha:1.0f] ];
        [[AppManager sharedInstance] setPinkHighlighted2:[UIColor  colorWithRed:182.0f/255.0f green:35.0f/255.0f blue:103.0f/255.0f alpha:1.0f] ];
        [[AppManager sharedInstance] setPurpleHighlighted2:[UIColor colorWithRed:102.0f/255.0f green:45.0f/255.0f blue:145.0f/255.0f alpha:1.0f] ];
    
        [[AppManager sharedInstance] setGreenHighlighted2:[UIColor colorWithRed:21.0f/255.0f green:87.0f/255.0f blue:29.0f/255.0f alpha:1.0f] ];
        [[AppManager sharedInstance] setOrangeHighlighted2:[UIColor  colorWithRed:233.0f/255.0f green:98.0f/255.0f blue:13.0f/255.0f alpha:1.0f] ];
}
@end
