//
//  Event.m
//  CRMC
//
//  Created by WebitMacMini Two on 6/15/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import "Event.h"

@implementation Event

-(NSMutableArray*)parseListEventFromJson:(NSString*) strJson
{
    NSError* error;
    NSArray* arr=[NSJSONSerialization JSONObjectWithData:[strJson dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    NSMutableArray* arrEvents;
    if(arr.count>0)
    {
        arrEvents=[[NSMutableArray alloc]init];
        for (NSDictionary* dict in arr)
            [arrEvents addObject:[self parseEventFromDict:dict]];
    }
    return  arrEvents;
}

-(Event*)parseEventFromDict:(NSDictionary*)dict
{
    Event* event=[[Event alloc]init];
    event.eventId=(int)[[dict objectForKey:@"eventId"] integerValue];
    event.notes=[dict objectForKey:@"notes"];
     //event.eventDate=[dict objectForKey:@"eventDate"];
    event.eventDate=[dict objectForKey:@"eventDateToMobileClient"];

     event.title=[dict objectForKey:@"title"];
     event.address=[dict objectForKey:@"address"];
     event.descriptionn=[dict objectForKey:@"description"];
    event.cityId=(int)[[dict objectForKey:@"cityId"] integerValue];
    event.price=[dict objectForKey:@"price"];
    event.link=[dict objectForKey:@"link"];
    return event;
}

-(Event*)parseEventFromDictFeeds:(NSMutableDictionary*)dict
{
    Event* event=[[Event alloc]init];
 //   event.eventId=(int)[[dict objectForKey:@"eventId"] integerValue];
 //   event.notes=[dict objectForKey:@"notes"];
   event.eventDate=[dict objectForKey:@"pubDate"];
    event.title=[dict objectForKey:@"title"];
//    event.address=[dict objectForKey:@"address"];
    event.descriptionn=[dict objectForKey:@"description"];
    //event.cityId=(int)[[dict objectForKey:@"cityId"] integerValue];
   // event.price=[dict objectForKey:@"price"];
    event.link=[dict objectForKey:@"link"];
    return event;
}


-(NSMutableArray*)parseListEventFromDict:(NSDictionary*)dict
{
    NSMutableArray*arrInterestPoint=[[NSMutableArray alloc]init];
    for (NSDictionary* dic in dict)
        [arrInterestPoint addObject:[self parseEventFromDict:dic]];
    return arrInterestPoint;
}
@end
