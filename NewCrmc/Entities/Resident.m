//
//  Resident.m
//  CRMC
//
//  Created by WebitMacMini Two on 6/18/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import "Resident.h"
#import "InterestArea.h"
#import "EducationInstitution.h"
#import "AppDelegate.h"
@implementation Resident
- (id)init:(NSString*)firstName LastName:(NSString*)lastName Email:(NSString*)email MobileNumber:(NSString*)mobileNumber  PhoneNumber:(NSString*)phoneNumber HouseNumber:(NSString*)houseNumber StreetId:(int)streetId CityId:(int)cityId IsPushNotification:(BOOL)isPushNotification
{
    AppDelegate*appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _firstName=firstName;
    _lastName=lastName;
    _email=email;
    _mobileNumber=mobileNumber;
    _phoneNumber=phoneNumber;
    _houseNumber=houseNumber;
    _streetId=streetId;
    _interestAreasList=[[NSMutableArray alloc]init];
    _EducationInstitutionsList=[[NSMutableArray alloc]init];
    _cityId=cityId;
    _applianceType=@"ios";
    _isGetNotification=isPushNotification;
    if (appDelegate.dt)
        _applianceIdentity = appDelegate.dt;
    else
       _applianceIdentity=@"";
    return self;
}

- (id)init:(int)residentId FirstName: (NSString*)firstName LastName:(NSString*)lastName Email:(NSString*)email MobileNumber:(NSString*)mobileNumber  PhoneNumber:(NSString*)phoneNumber HouseNumber:(NSString*)houseNumber StreetId:(int)streetId CityId:(int)cityId IsPushNotification:(BOOL)isPushNotification
{
    AppDelegate*appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _residentId=residentId;
    _firstName=firstName;
    _lastName=lastName;
    _email=email;
    _mobileNumber=mobileNumber;
    _phoneNumber=phoneNumber;
    _houseNumber=houseNumber;
    _streetId=streetId;
    _cityId=cityId;
    _interestAreasList=[[NSMutableArray alloc]init];
    _EducationInstitutionsList=[[NSMutableArray alloc]init];
    _applianceType=@"ios";
    _isGetNotification=isPushNotification;
    if (appDelegate.dt)
        _applianceIdentity = appDelegate.dt;
    else
        _applianceIdentity=@"";
    return self;
}
-(NSDictionary*)parseResidentToDict
{
    NSMutableDictionary* dict=[[NSMutableDictionary alloc]init];
    [dict setObject:[NSNumber numberWithInt:_residentId] forKey:@"residentId"];
    [dict setObject:_firstName forKey:@"firstName"];
    [dict setObject:[NSNumber numberWithInt:_cityId] forKey:@"cityId"];
    [dict setObject:_lastName forKey:@"lastName"];
    [dict setObject:_email forKey:@"email"];
    [dict setObject:_mobileNumber forKey:@"mobileNumber"];
    [dict setObject:_phoneNumber forKey:@"phoneNumber"];
    [dict setObject:_houseNumber forKey:@"houseNumber"];
    [dict setObject:[NSNumber numberWithInt:_streetId] forKey:@"streetId"];
    [dict setObject:_applianceType forKey:@"applianceType"];
    [dict setObject:[NSNumber numberWithBool:_isGetNotification] forKey:@"isGetGlobalNotification"];
    [dict setObject:[NSNumber numberWithBool:_isGetNotification] forKey:@"isGetPersonalNotification"];
    [dict setObject:_applianceIdentity forKey:@"applianceIdentity"];
    NSDictionary* dictReport=[NSDictionary dictionaryWithObjectsAndKeys:dict,@"resident", nil];
    return dictReport;
}
-(NSDictionary*)parseResidentIdToDict
{
    NSMutableDictionary* dict=[[NSMutableDictionary alloc]init];
    [dict setObject:[NSNumber numberWithInt:_residentId] forKey:@"residentId"];
    [dict setObject:[NSNumber numberWithInt:_cityId] forKey:@"cityId"];
    NSDictionary* dictReport=[NSDictionary dictionaryWithObjectsAndKeys:dict,@"resident", nil];
    return dictReport;
}
-(NSDictionary*)parseResidentToDictSettingInterestAreas
{
    NSMutableDictionary* dict=[[NSMutableDictionary alloc]init];
    [dict setObject:[NSNumber numberWithInt:_residentId] forKey:@"residentId"];
    InterestArea*interestArea=[[InterestArea alloc]init];
    NSMutableArray*list=[[NSMutableArray alloc]init];
    for(int i=0;i<_interestAreasList.count;i++)
    {
        interestArea=_interestAreasList[i];
        [list addObject:[interestArea parseInterestAreaToDict]];
    }
    [dict setObject:list forKey:@"interestAreasList"];
    NSDictionary* dictReport=[NSDictionary dictionaryWithObjectsAndKeys:dict,@"resident", nil];
    return dictReport;
}
-(NSDictionary*)parseResidentToDictSettingEducationInstitutions
{
    NSMutableDictionary* dict=[[NSMutableDictionary alloc]init];
    [dict setObject:[NSNumber numberWithInt:_residentId] forKey:@"residentId"];
    EducationInstitution*educationInstitution=[[EducationInstitution alloc]init];
    NSMutableArray*list=[[NSMutableArray alloc]init];
    for(int i=0;i<_EducationInstitutionsList.count;i++)
    {
        educationInstitution=_EducationInstitutionsList[i];
        [list addObject:[educationInstitution parseEducationInstitutionToDict]];
    }
    [dict setObject:list forKey:@"EducationInstitutionsList"];
    NSDictionary* dictReport=[NSDictionary dictionaryWithObjectsAndKeys:dict,@"resident", nil];
    return dictReport;
}

@end