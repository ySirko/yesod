//
//  CityContent.m
//  חדרה
//
//  Created by Lior Ronen on 9/10/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "CityContent.h"
@implementation CityContent
-(CityContent*)parseCityContentFromJson:(NSString*) strJson
{
    NSError* error;
    NSDictionary* dict=[NSJSONSerialization JSONObjectWithData:[strJson dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    CityContent*cityContent=[[CityContent alloc]init];
    cityContent.aboutCity= [dict objectForKey:@"aboutCity"];
        cityContent.freedomOfInformation= [dict objectForKey:@"freedomOfInformation"];
    
    cityContent.mayorSpeech= [dict objectForKey:@"mayorSpeech"];
    cityContent.emergencyTime= [dict objectForKey:@"emergencyTime"];
    cityContent.cityHallWebsite= [dict objectForKey:@"cityHallWebsite"];
    cityContent.facebook= [dict objectForKey:@"facebook"];
    cityContent.callCenter= [dict objectForKey:@"callCenter"];
    cityContent.passwordToCrmcDB= [dict objectForKey:@"passwordToCrmcDB"];
    return cityContent;
}
@end
