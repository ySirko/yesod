//
//  AppManager.h
//
//  Created by Benny Davidovitz on 1/6/15.
//  Copyright (c) 2015 webit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppManager : NSObject

+ (AppManager *) sharedInstance;
+(NSString *)yesButWhichDeviceIsIt;
@property (strong,nonatomic)NSString* DeviceName;

@property(retain,nonatomic)UIColor*lightBlue;
@property(retain,nonatomic)UIColor*pink;
@property(retain,nonatomic)UIColor*purple;
@property(retain,nonatomic)UIColor*green;
@property(retain,nonatomic)UIColor*orange;
@property(retain,nonatomic)UIColor*lightBlueHighlighted;
@property(retain,nonatomic)UIColor*pinkHighlighted;
@property(retain,nonatomic)UIColor*purpleHighlighted;
@property(retain,nonatomic)UIColor*greenHighlighted;
@property(retain,nonatomic)UIColor*orangeHighlighted;
@property(retain,nonatomic)UIColor*redHighlighted;
@property(retain,nonatomic)UIColor*lightBlue2;
@property(retain,nonatomic)UIColor*pink2;
@property(retain,nonatomic)UIColor*purple2;
@property(retain,nonatomic)UIColor*green2;
@property(retain,nonatomic)UIColor*orange2;
@property(retain,nonatomic)UIColor*red2;
@property(retain,nonatomic)UIColor*lightBlueHighlighted2;
@property(retain,nonatomic)UIColor*pinkHighlighted2;
@property(retain,nonatomic)UIColor*purpleHighlighted2;
@property(retain,nonatomic)UIColor*greenHighlighted2;
@property(retain,nonatomic)UIColor*orangeHighlighted2;
@property(retain,nonatomic)UIColor*redHighlighted2;

+ (void) setColors;
@end
