//
//  City.h
//  CRMC
//
//  Created by WebitMacMini Two on 6/16/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface City : NSObject
@property int cityId;
@property(retain,nonatomic) NSString* cityName;
- (id)init:(int) cityId Name:(NSString*)cityName;
-(NSDictionary*)parseCityToDict;
-(NSMutableArray*)parseListCityFromJson:(NSString*) strJson;
-(City*)parseCityFromDict:(NSDictionary*)dict;
@end
