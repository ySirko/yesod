//
//  Trip.m
//  מגדל העמק
//
//  Created by MyMac on 12/9/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "Trip.h"
#import "LatLng.h"
@implementation Trip
-(NSMutableArray*)parseListTripFromJson:(NSString*) strJson
{
    NSError* error;
    NSArray* arr=[NSJSONSerialization JSONObjectWithData:[strJson dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    NSMutableArray* arrSurvey;
    if(arr.count>0)
    {
        arrSurvey=[[NSMutableArray alloc]init];
        for (NSDictionary* dict in arr)
            [arrSurvey addObject:[self parseTripFromDict:dict]];
    }
    return  arrSurvey;
}

-(Trip*)parseTripFromDict:(NSDictionary*)dict
{
    Trip* trip=[[Trip alloc]init];
    trip.nvTripName=[dict objectForKey:@"tripName"];
    trip.nvDescription=[dict objectForKey:@"description"];
    LatLng*latLng=[[LatLng alloc]init];
    trip.lLatLngList=[latLng parseListLatLngFromJson:[dict objectForKey:@"latLngList"]];
    return trip;
}
@end
