//
//  WSResult.m
//  Global
//
//  Created by MyMac on 10/23/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "WSResult.h"
#import "WSStreet.h"
@implementation WSResult
-(WSResult*)parseWSResultFromJson:(NSString*) strJson
{
     WSResult* wSResult=[[WSResult alloc]init];
    NSError* error;

    NSDictionary* dict=[NSJSONSerialization JSONObjectWithData:[strJson dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    NSString* res=[dict objectForKey:@"Status"];
  
    int number = [[dict objectForKey:@"Status"] intValue];
   // NSInteger b=(int) res;
      //  NSString* res=[dict objectForKey:@"ResultMessage"];
   // NSNull*n=[dict objectForKey:@"ResultMessag"];
    if(number!=1)
    {
        wSResult.ResultObjects=nil;
        return wSResult;

    }
    else
    {
    //    NSString* res=strJson;
//    NSDictionary* dict=[NSJSONSerialization JSONObjectWithData:[strJson dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
//   
    WSStreet*wSStreet=[[WSStreet alloc]init];
    wSResult.ResultObjects=[wSStreet parseListWSStreetFromJson:[dict objectForKey:@"ResultObjects"]];
    }
    return wSResult;
  
    }
@end
