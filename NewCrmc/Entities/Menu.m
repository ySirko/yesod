 //
//  Menu.m
//  חדרה
//
//  Created by MyMac on 11/25/14.
//  Copyright (c) 2014 webit. All rights reserved.
//
#import "Menu.h"

@implementation Menu

-(NSMutableArray*)parseListMenuFromJson:(NSString*)strJson
{
    NSError* error;
    NSArray* arr=[NSJSONSerialization JSONObjectWithData:[strJson dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    NSMutableArray* arrStreet;
    if(arr.count>0)
    {
        arrStreet=[[NSMutableArray alloc]init];
        for (NSDictionary* dict in arr)
            [arrStreet addObject:[self parseMenuFromDict:dict]];
    }
    return  arrStreet;
}

-(Menu*)parseMenuFromDict:(NSDictionary*)dict
{
    Menu* menu=[[Menu alloc]init];
    menu.ID=[[dict objectForKey:@"buttonId"] integerValue];
    menu.name = [[dict objectForKey:@"name"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSData* data=  [NSData dataFromBase64String:[dict objectForKey:@"icon"]];
    menu.icon=[UIImage imageWithData:data];
    menu.active=[[dict objectForKey:@"active"]integerValue];
    menu.tag=[[dict objectForKey:@"tag"]integerValue];
    menu.link=[dict objectForKey:@"link"];
    
    if(menu.active==3)
    {
        menu.listMenu=[[NSMutableArray alloc]init];
        for(NSDictionary*m in [dict objectForKey:@"listMenu"])
            [menu.listMenu addObject:[self parseMenuFromDict:m]];
    }
    
    menu.strColor=[dict objectForKey:@"strColor"];
    menu.phone=[dict objectForKey:@"phone"];
    menu.email=[dict objectForKey:@"email"];
    menu.emailSubject=[dict objectForKey:@"emailSubject"];
    menu.color=[self getUIColorObjectFromHexString:[dict objectForKey:@"color"] alpha:1];
    menu.HighlightedColor=[self getUIColorObjectFromHexString:[dict objectForKey:@"highlightedColor"] alpha:1];
    
    return menu;
}

- (UIColor *)getUIColorObjectFromHexString:(NSString *)hexStr alpha:(CGFloat)alpha
{
    unsigned int hexint = [self intFromHexString:hexStr];
    UIColor *color =
    [UIColor colorWithRed:((CGFloat) ((hexint & 0xFF0000) >> 16))/255
                    green:((CGFloat) ((hexint & 0xFF00) >> 8))/255
                     blue:((CGFloat) (hexint & 0xFF))/255
                    alpha:alpha];
    return color;
}

- (unsigned int)intFromHexString:(NSString *)hexStr
{
    unsigned int hexInt = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexStr];
    [scanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@"#"]];
    [scanner scanHexInt:&hexInt];
    return hexInt;
}
@end
