//
//  Resident.h
//  CRMC
//
//  Created by WebitMacMini Two on 6/18/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Resident : NSObject
@property int residentId;
@property(nonatomic,retain) NSString* firstName;
@property(nonatomic,retain)NSString* lastName;
@property(nonatomic,retain)NSString* email;
@property(nonatomic,retain) NSString* mobileNumber;
@property(nonatomic,retain)NSString* phoneNumber;
@property(nonatomic,retain)NSString* houseNumber;
@property int cityId;
@property int streetId;
@property(retain,nonatomic)NSMutableArray*interestAreasList;
@property(retain,nonatomic)NSMutableArray*EducationInstitutionsList;
@property(nonatomic,retain)NSString* applianceType;
@property(nonatomic,retain)NSString* applianceIdentity;
@property BOOL isGetNotification;
- (id)init:(NSString*)firstName LastName:(NSString*)lastName Email:(NSString*)email MobileNumber:(NSString*)mobileNumber  PhoneNumber:(NSString*)phoneNumber HouseNumber:(NSString*)houseNumber StreetId:(int)streetId CityId:(int)cityId IsPushNotification:(BOOL)isPushNotification;
- (id)init:(int)residentId FirstName: (NSString*)firstName LastName:(NSString*)lastName Email:(NSString*)email MobileNumber:(NSString*)mobileNumber  PhoneNumber:(NSString*)phoneNumber HouseNumber:(NSString*)houseNumber StreetId:(int)streetId CityId:(int)cityId IsPushNotification:(BOOL)isPushNotification;
-(NSDictionary*)parseResidentToDict;
-(NSDictionary*)parseResidentToDictSettingInterestAreas;
-(NSDictionary*)parseResidentToDictSettingEducationInstitutions;

-(NSDictionary*)parseResidentIdToDict;
@end



