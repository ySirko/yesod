//
//  InterestArea.m
//  NewCrmc
//
//  Created by WebitMacMini Two on 7/21/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "InterestArea.h"

@implementation InterestArea
- (id)init:(int)interestAreaId InterestAreaName:(NSString*)interestAreaName InterestAreaToResidentId:(int)interestAreaToResidentId
{
    self = [super init];
    if (self)
    {
        _interestAreaId=interestAreaId;
        _interestAreaName=interestAreaName;
        _interestAreaToResidentId=interestAreaToResidentId;
    }
    return self;
}

-(NSMutableArray*)parseListInterestAreaFromJson:(NSString*) strJson
{
    NSError* error;
    NSArray* arr=[NSJSONSerialization JSONObjectWithData:[strJson dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    NSMutableArray* arrInterestArea;
    if(arr.count>0)
    {
        arrInterestArea=[[NSMutableArray alloc]init];
        for (NSDictionary* dict in arr)
            [arrInterestArea addObject:[self parseInterestAreaFromDict:dict]];
    }
    return  arrInterestArea;
}

-(InterestArea*)parseInterestAreaFromDict:(NSDictionary*)dict
{
    InterestArea* interestArea=[[InterestArea alloc]init];
    interestArea.interestAreaId=(int)[[dict objectForKey:@"interestAreaId"] integerValue];
    interestArea.interestAreaName=[dict objectForKey:@"interestAreaName"];
    interestArea.interestAreaToResidentId=(int)[[dict objectForKey:@"interestAreaToResidentId"] integerValue];
    return interestArea;
}

-(NSDictionary*)parseInterestAreaToDict
{
    NSMutableDictionary* dict=[[NSMutableDictionary alloc]init];
    [dict setObject:[NSNumber numberWithInt:_interestAreaId] forKey:@"interestAreaId"];
    [dict setObject:_interestAreaName forKey:@"interestAreaName"];
    [dict setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"residentId"] forKey:@"interestAreaToResidentId"];
    return dict;
}

@end
