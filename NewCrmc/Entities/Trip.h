//
//  Trip.h
//  מגדל העמק
//
//  Created by MyMac on 12/9/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Trip : NSObject
@property int iTripId;
@property(retain,nonatomic)NSString*nvTripName;
@property(retain,nonatomic)NSString*nvDescription;
@property(retain,nonatomic)NSArray*lLatLngList;
-(NSMutableArray*)parseListTripFromJson:(NSString*) strJson;
@end
