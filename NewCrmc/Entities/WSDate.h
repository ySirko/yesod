//
//  WSDate.h
//  מגדל העמק
//
//  Created by Lior Ronen on 9/29/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WSDate : NSObject
@property int Day;
@property int Month;
@property int Year;
-(NSDictionary*)parseWSDateToDict;
@end
