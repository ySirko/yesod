//
//  focusStreet.h
//  מגדל העמק
//
//  Created by Lior Ronen on 9/29/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface focusStreet : NSObject
@property(retain,nonatomic) NSString*clientId;
@property(retain,nonatomic) NSString*password;
@property(retain,nonatomic) NSString*userName;
@property int streetId;
@property(retain,nonatomic) NSString*streetName;
-(NSDictionary*)parsefocusStreetToDict;
-(focusStreet*)parseEntityFromDict:(NSDictionary*)dict;
-(NSMutableArray*)parseListStreetsFromJson:(NSString*) strJson;
@end

