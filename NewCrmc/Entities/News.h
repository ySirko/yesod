//
//  News.h
//  CRMC
//
//  Created by WebitMacMini Two on 6/25/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface News : NSObject
@property int newId;
@property (retain,nonatomic)NSString* title;
@property (retain,nonatomic)NSString* content;
@property (retain,nonatomic)NSString* picture;
-(News*)parseNewsFromDict:(NSDictionary*)dict;
-(NSMutableArray*)parseListNewsFromJson:(NSString*) strJson;
-(News*)parseNewsFromDictFeeds:(NSMutableDictionary*)dict;
@end
