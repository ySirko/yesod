//
//  AppDelegate.h
//  NewCrmc
//
//  Created by WebitMacMini Two on 7/17/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "City.h"
#import "Connection.h"
#import "Generic.h"
#import "GAI.h"
#import "CityContent.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) NSMutableArray* listCities ;
@property (retain, nonatomic) City* city;
@property (retain, nonatomic) NSArray* menuList;
@property (retain, nonatomic) UIImage* back;
@property (retain, nonatomic) NSString*dt;
@property (retain, nonatomic) CityContent *cityContent;
@property (assign, nonatomic) BOOL openFromPush;
@property (retain, nonatomic) NSArray*menu;
@property (retain, nonatomic) UIImage* logoCity;
@property (retain, nonatomic) NSString*deviceName;

@end
